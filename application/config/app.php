<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//роли пользователей
$config['roles'] = array(
    1   => 'Админ',
    2   => 'Пользователь',
    3   => 'Водититель',
    4   => 'Склад',
    5   => 'Оператор закупок',
    6   => 'Специалист сопровождения торгов',
    7   => 'Ответственный менеджер торгов',
);

// типы контрагентов
$config['contractor_type'] = array(
    1   => 'Поставщик',
    2   => 'Покупатель',
    0   => 'Оба типа',
);

// статус курьера
$config['forwarder_status'] = array(
    0   => 'Не активен',
    1   => 'Активен',
    2   => 'Активен на дату'
);

//статус завки
$config['orders_status'] = array(
    0   => 'Новая',
    1   => 'В сборку',
    2   => 'Проблема',
    3   => 'В плане',
    4   => 'Готово',
);
$config['orders_status_color'] = array(
    0   => '',
    1   => 'box-default',
    2   => 'box-warning',
    3   => 'box-primary',
    4   => 'box-success',
);


//Тип операции завки
$config['orders_operation'] = array(
    1   => 'Доставка',
    2   => 'Забор товара'
);

// Статус склада
$config['stock_status'] = array(
    0   => 'Ожидает',
    1   => 'Собирается',
    2   => 'Готово',
    3   => 'Недостача'
);

$config['stock_status_color'] = array(
    0   => 'box-default',
    1   => 'box-warning',
    2   => 'box-success',
    3   => 'box-danger'
);
$config['stock_status_label'] = array(
    0   => 'label-default',
    1   => 'label-warning',
    2   => 'label-success',
    3   => 'label-danger'
);



$config['upload_dir'] = './upload/';

// статус заявки закупки
$config['bid_status'] = array(
	0 => 'Не указан',
	1 => 'Участвуем',
	2 => 'Не участвуем',
	3 => 'Подана',
	4 => 'Допущена',
	5 => 'Отклонена',
	6 => 'Проиграна',
	7 => 'Выиграна'
);

// статус закупки
$config['buy_status'] = array(
	1 => 'Подача заявок',
	2 => 'Работа комиссии',
	3 => 'Аукцион закончился'
);

$config['mon'] = array(
  1 => 'Январь',
  2 => 'Февраль',
  3 => 'Март',
  4 => 'Апрель',
  5 => 'Май',
  6 => 'Июнь',
  7 => 'Июль',
  8 => 'Август',
  9 => 'Сентябрь',
  10 => 'Октябрь',
  11 => 'Ноябрь',
  12 => 'Декабрь',
);

define('ORDER_UPLOAD', FCPATH.'upload/orders/');
