<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
    'auth/login' => array(
        array(
            'field' => 'password',
            'label' => 'Пароль',
            'rules' => 'trim|required|callback_user_check'
        ),
        array(
            'field' => 'login',
            'label' => 'Логин',
            'rules' => 'trim|required'
        )
    ),
    'user/profile' => array(
        array(
            'field' => 'login',
            'label' => 'Логин',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|valid_email'
        ),
        array(
            'field' => 'first_name',
            'label' => 'Имя',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Фамилия',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'password',
            'label' => 'Пароль',
            'rules' => 'trim'
        ),
    ),
    'user/add' => array(
        array(
            'field' => 'login',
            'label' => 'Логин',
            'rules' => 'trim|required|is_unique[users.login]'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|valid_email'
        ),
        array(
            'field' => 'first_name',
            'label' => 'Имя',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Фамилия',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'password',
            'label' => 'Пароль',
            'rules' => 'trim|required'
        ),
    ),
    'user/edit' => array(
        array(
            'field' => 'login',
            'label' => 'Логин',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|valid_email'
        ),
        array(
            'field' => 'first_name',
            'label' => 'Имя',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Фамилия',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'password',
            'label' => 'Пароль',
            'rules' => 'trim'
        ),
    ),

	'buys/add' => array(
		array(
			'field' => 'reg_number',
			'label' => 'Регистрационный номер',
			'rules' => 'trim|is_unique[buys.reg_number]'
		),array(
			'field' => 'customer',
			'label' => 'Заказчик',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'bid_date_end',
			'label' => 'Дата окончания рассмотрения заявки',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'auction_date',
			'label' => 'Дата проведения аукциона',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'max_contract_price',
			'label' => 'Начальная цена контракта',
			'rules' => 'trim|required'
		),
	),
	'buys/edit' => array(
		array(
			'field' => 'customer',
			'label' => 'Заказчик',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'bid_date_end',
			'label' => 'Дата окончания рассмотрения заявки',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'auction_date',
			'label' => 'Дата проведения аукциона',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'max_contract_price',
			'label' => 'Начальная цена контракта',
			'rules' => 'trim|required'
		),
	),

);
