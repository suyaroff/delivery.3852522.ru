<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Загрузка фото
 * @param $filename
 * @param $new_filename
 * @param $path
 * @param $w
 * @param $h
 * @param int $quality
 * @return bool|void
 */
function upload_photo($filename, $new_filename, $path = './', $w, $h, $quality = 85)
{
	$info = getimagesize($filename);
	$width = $info[0];
	$height = $info[1];
	$mime = $info['mime'];

	if ($mime == "image/jpeg")
		$srcImage = imagecreatefromjpeg($filename);
	elseif ($mime == "image/gif")
		$srcImage = imagecreatefromgif($filename);
	elseif ($mime == "image/png")
		$srcImage = imagecreatefrompng($filename);
	else
		return;

	$ratio = $width / $height;

	if ($w > $width AND $h > $height) {
		$offsetX = ($w / 2) - ($width / 2);
		$offsetY = ($h / 2) - ($height / 2);
		$newX = $width;
		$newY = $height;
	} else {

		if ($w / $h > $ratio) {
			$newX = $h * $ratio;
			$newY = $h;
		} else {
			$newX = $w;
			$newY = $w / $ratio;
		}
		$offsetX = ($w / 2) - ($newX / 2);
		$offsetY = ($h / 2) - ($newY / 2);

	}

	$destImage = ImageCreateTrueColor($w, $h);
	$bg_color = imagecolorallocate($destImage, 255, 255, 255);
	imagefilledrectangle($destImage, 0, 0, $w, $h, $bg_color);
	imagecopyresampled($destImage, $srcImage, $offsetX, $offsetY, 0, 0, $newX, $newY, $width, $height);
	$result = ImageJpeg($destImage, $path . $new_filename, $quality);
	return $result;
}

function my_date($mysql_date, $format = 'd-m-Y')
{
	$time = strtotime($mysql_date);
	if ($time > 100)
		return date($format, $time);
	else
		return '';
}

function mysql_date($date, $format = 'Y-m-d')
{
	return date($format, strtotime($date));
}

function set_buy_status($date_end, $date_auction)
{
	$date = date('Y-m-d H:i');
	if ($date > $date_auction) return 3;
	if ($date < $date_auction && $date > $date_end) return 2;
	if ($date < $date_end) return 1;
}

/**
 * @return mixed
 * Получение токена доступа
 */
function get_google_token()
{

	if (ENVIRONMENT == 'development ') {
		$service_email = 'jamal-buy@burnished-rider-200606.iam.gserviceaccount.com';
		$private_key = "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCw55u+s94lZf/X\nVPitVMS9bBjuzJuKBSZdLmaR88jCL4meG5hJmxy1wxKgRWZvWpufKaFKwptgkVi3\niEyjYeinaQUSVRKmpzuwuhtuJlqhd1tuJSF96im6Laab1jRF3V1W5itHmRiK+MCy\noNoUInenH2Wo2Xry09QmngM1ef6Wi+6dYYGr4QRLdOA3Y7EmndXpPPbDUwPs6wMX\nKR2APxKaqySmm0UJivN+YZwWmWF+obMUYzyZ2KXDO16ylr/4uuFON6UEmrELytKm\nocwxLbiB6pJnQiFXozcf1NTmdyCPcI6zrvTFwcmxq1hAXwsUhlhkDDNOEV7YiYre\nw4K/WI/3AgMBAAECggEADP7ux5syGiA8o8aHwEAGdf3vdPeOA50uPNEwUs+Mc4uH\nehh/3wRonwe2IhUHGe4XmaE2yqWyQW1cDZuTObtzGn1SFI8wYBLQBUMeycBCjzMU\nAerzY2sk5pr87tzVGT7SYRRmoItkLK1qhpTfgHQpwxrA3p8fc6BRKQcZqD+OjuOY\nflmyr7+epu5I+3DvuLJEPN/8vhfSZxGq0EiWiTk+LQXKr9/7jYrB5FeAVBCboWm+\n0orGJJ2luUlrBDfOqeBpreLzqaRKIt6hz1zeCM2q9qZgoJCMQFAau9wro60GK/EB\nCyQP4Lea5CyzyZPGC9sGZi9OcrQmC3XJ5bQqiyfyrQKBgQDXjX3qoZa5g9o0/DSV\nOXYo8+edgRaXaUOrBdX2X30CmRPueGYVmca7PfdANRkCwOhY0+f9JUcbLR282dby\nTvD24iLcw5AUrpV3wbfJibI829FDSVcH/yb2byolJY8WncmNr+cjkomfv283xN+I\n+8hYcK6lRbQE8Zcus8eP3/DmXQKBgQDSGZU2IsKMJyHdd6VNqZK3PV7VcHddtiMw\ngoIWXKs67da3HY5A4DyiAcHRp6cmMNl7oQm23EsEK1ZgwYx4rvgREBXeooJygjqc\nDsBof1pQzSLWvKf6buD1leY9cL1nBkz079XM9T9c7IpYkJPrgTI4x/cwtsrmfCh/\nV5Ds7UHCYwKBgQClkcV6iIxiC2l88YziYrTkWBU84n8b5ntXzq+/AzHe4P5yWoDv\nbqM8CCj3ZNHFT7ApKvwzcoLf2RPSj/GPRhvJR4JH2N+/QysERgSKBf6myKJA8QCs\n38xp1pGk3ui+Eo1TDB6bMGAOGNmxGMQrUFtfs44y3QueEIR0Fxve48EC8QKBgHeG\nnmCh/dQGHzoDa0N3iUQ94cnDpeTmTba3SJK4PaHU1SZv5hH6gAQaw80md+0+NTwk\nLYaeZP8GOKnq8qNlxzLN1+NEyLmkZPuLWOMaMKNvULahtV6/3bNZiBbEILyP1RAz\nsYpcq6ON3lbsSXrdkVjESlioZV4inFd11bk0cQjJAoGBAMUsnXhZzc5Hf9bc/JBO\nrOgocokN1Y8iVp3OouPBkoJ438KFSPn8q88OoLqlpN+pmCWRZm7Vm7JhkpBYobCy\nyEomu7Eb+oh8nUuDKqZv4ufyYyoRoBATHxixWCfV+PtxU6rCpTKclDYAzb27Dxi2\nZUf/0jr7HNREVJelk2J2Enbo\n-----END PRIVATE KEY-----\n";
	} else {
		$service_email = 'buys-2018@buys-200706.iam.gserviceaccount.com';
		$private_key = "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC2dyrmArHB7gCw\n0Y+QzUeVaouiMF9P3mPbQBCc+Grv3CPVNblXAERjk4gapV4nI4+uKMJevrlat2TD\n34pF7pzCiYNgMva+zywUpYz58M5Z287Z1twg5hR6qbpsHX6I/kBewSI6xmE1KLZw\nMtkpzqS/ZpKzdRmhuFh8a2Anz5wzvG4gWdzn33rh9H+c7oHGRAwSkiS328DsRaov\nt84EnH0GuuqWi+UlAM/3lGrJG+qHgSVaE78q1s6BxdwsXdiVkHIKH1jPc+eLSsKa\nNc1btX2oVNhVLy8ToBH6ro4A1hvcwPT1+uKde0YDumRcXpc3Op7rm6jl0yKeJIws\nQrtHUJ/HAgMBAAECggEACMbttNe/Vj1NIjRKbCQ7w8e2xMv/4bcoZYUM54gZmMUl\noh1t923nXETeAK4gu9A9mpqSUv1orCDzQ2rO23NRbhgwTiHVdbNAgxO9ls5LFs54\ngkkQGLMInCfq9EVs70kPrp8qttoRFf4J3JbNlHHAwgY8m05QN9PGyeFeZDvb3y8l\nzcOLCFt3borT898vt3suKSTDol1pUFKd+QGu+r+l1ycbZ+9EmMNHTJXsau35TZPg\nVCtkBYK5DHsr5y4mrWLi8J9PGvkk0ufLhxiMrkMa4+r1B+bY4phbI50sIsIg0vOL\nyqjq8zaoMhNggEP5VrtLWy062jJTOlIwZJnncwkYDQKBgQDyEux7KbUI06NiACoU\nWUA93fRmdooi0nUh/E4aojxnoM6DzmUC5/xEKvtqlS8RCKmDVr9emYY3IDaXINBp\nPog1rnJAlzYpmmynHVZpWz4K98R9Tqyou45Z9PB+c/f+lRHYLImhITdWwKj7Ykz/\nQJC5NABHGqLW7zLxLGv6DnaaTQKBgQDA9mCbxa1oBr5gwMVF3X4hQJmep+CleLVt\nEXTTtaQgYIcNaXPIXzchy+4QK+9t5Ocmk7xN+sfgkjlx7FMZtLKJn9Pu0penHkBz\n2R2css+f61QQklCZzCH7Q0+5Eng8S0SCuTmUoQrvkWJ8x4U4u8doipy4M8RMcWWX\nCY81bpDEYwKBgQDeRK/sOpRmIWVAI6R/gJ+y7qxstGULqjAw32ibneFWyVk1vvoS\nnA2OTeUBrS9FNa3n0WvvoYmR7jsRsuiJ6gfJOn7ffbgWpRskqpRgMo4aJfu9jtKM\nL4J6aKO1iCfqEtgKpIJOd6BHQKkoAM9wA8mes/eZUgS403Lz7PTL6iHOdQKBgHJs\nJtF4IOHEzfN1nAVX2ElDk1SfW/kDYSHx7gL0fQSt+KedVokE3R5vXRW919akm+Um\nSc073nZufx3s80IVAqHVYskFm+P/blzD7GlVJwU/eLPy71Sj1aq3OXkXWLD4OQID\nb/+Ql3mEDpDLh1YjE5YMS+520/o4a/NqOb+HdtTdAoGAYgxat1/kcO4GG8Ncui99\nTL5OJALPqnTRvs/+ytwQe+GiE08egg2ACLmy3at6EQgyIFmIikwB+mqi4Y1ZV+eZ\nemW0maj9iK/Ax8Mkix6lmN5o1FdF4m/HoAsJC4FTVKfuilQyXpLhpVm/MNRk4uvE\n/8IBRzNkIzmcRJtYdMlgGrU=\n-----END PRIVATE KEY-----\n";
	}
	$header = '{"alg":"RS256","typ":"JWT"}';
	$claim = '{"iss":"' . $service_email . '","scope":"https://www.googleapis.com/auth/calendar","aud":"https://www.googleapis.com/oauth2/v4/token","exp":' . (time() + 360) . ',"iat":' . time() . '}';
	$header64 = str_replace(array('+', '/', '='), array('-', '_', ''), base64_encode($header));
	$claim64 = str_replace(array('+', '/', '='), array('-', '_', ''), base64_encode($claim));
	openssl_sign($header64 . '.' . $claim64, $signature, $private_key, 'SHA256');
	$signature64 = str_replace(array('+', '/', '='), array('-', '_', ''), base64_encode($signature));
	$jwt = $header64 . '.' . $claim64 . '.' . $signature64;

	$ch = curl_init('https://www.googleapis.com/oauth2/v4/token');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer&assertion=$jwt");
	$resp = curl_exec($ch);
	curl_close($ch);
	return json_decode($resp)->access_token;
}

/**
 * @param $buy array Закупка
 * @param int $kind тип события 1 - заявка 2 - аукцион
 * @return mixed
 * Добавление события в календарь
 */
function insert_google_event($buy, $kind = 1)
{
	$token = get_google_token();
	if (ENVIRONMENT == 'development ') {
		$calendar_id = '983ujfd7jegc5qa51ph9n8dqlg@group.calendar.google.com';
	} else {
		$calendar_id = 'od5ql9rroabce7s285rekqkj9c@group.calendar.google.com';
	}

	$bid_date_end = str_replace(' ', 'T', $buy->bid_date_end);
	$auction_date = str_replace(' ', 'T', $buy->auction_date);
	$now = date('Y-m-d') . 'T' . date('H:i:s');
	if ($kind == 1) {
		$event = array(
			'summary' => 'Подача заявок: ЭА №' . $buy->reg_number . "\n" .
				'"' . $buy->buy_description . '"',
			'description' => 'Подача заявок: ' . $buy->customer . "\n" .
				"Электронный аукцион: №" . $buy->reg_number . "\n" .
				'"' . $buy->buy_description . "\"\n" .
				"Начальная цена контракта: " . $buy->max_contract_price . "\n".
				'Площадка: <a href="'.$buy->place_url.'">'.$buy->place.'</a>' ,

			'start' => array(
				'dateTime' => $now,
				'timeZone' => 'Europe/Moscow',
			),
			'end' => array(
				'dateTime' => $bid_date_end,
				'timeZone' => 'Europe/Moscow',
			),
		);
	}
	if ($kind == 2) {
		$event = array(
			'summary' => "Аукцион: " . $buy->reg_number . " \n \"" . $buy->buy_description . '"',
			'description' => 'Аукцион: ' . $buy->customer . "\n" .
				"Электронный аукцион: №" . $buy->reg_number . "\n" .
				'"' . $buy->buy_description . "\"\n" .
				"Начальная цена контракта: " . $buy->max_contract_price."\n",
				"От кого участвуем: " . $buy->partner_name ,

			'start' => array(
				'dateTime' => $auction_date,
				'timeZone' => 'Europe/Moscow',
			),
			'end' => array(
				'dateTime' => $auction_date,
				'timeZone' => 'Europe/Moscow',
			),
		);
	}


	$ch = curl_init('https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8', 'Authorization: Bearer ' . $token));
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($event));
	$resp = curl_exec($ch);
	curl_close($ch);
	return json_decode($resp);
}

function delete_google_event($event_bid_eventID, $event_auc_eventID)
{
	$token = get_google_token();
	if (ENVIRONMENT == 'development ') {
		$calendar_id = '983ujfd7jegc5qa51ph9n8dqlg@group.calendar.google.com';
	} else {
		$calendar_id = 'od5ql9rroabce7s285rekqkj9c@group.calendar.google.com';
	}
	if ($event_bid_eventID) {
		$ch = curl_init('https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events/' . $event_bid_eventID);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8', 'Authorization: Bearer ' . $token));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$resp = curl_exec($ch);
		curl_close($ch);
	}
	if ($event_auc_eventID) {
		$ch = curl_init('https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events/' . $event_auc_eventID);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8', 'Authorization: Bearer ' . $token));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$resp = curl_exec($ch);
		curl_close($ch);
	}
}
