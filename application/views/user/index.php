<div class="box">
    <div class="box-body">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="width:20px">#</th>
                <th>Фото</th>
                <th>Логин</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Email</th>
                <th>Роль</th>
                <th></th>
            </tr>
            <? foreach ( $users as $user) :?>
            <tr>
                <td><? echo $user->id; ?></td>
                <td><img src="/upload/<? echo $user->photo;?>" alt="" style="max-height: 40px;"></td>
                <td>
                    <? echo $user->login; ?>
                    <br><small><? echo $user->activity_date; ?></small>
                </td>
                <td><? echo $user->first_name; ?></td>
                <td><? echo $user->last_name; ?></td>
                <td><? echo $user->email; ?></td>
                <td><? echo $this->config->item($user->role, 'roles'); ?></td>
                <td><a class="btn btn-info btn-sm" href="/user/edit/<? echo $user->id; ?>/">редактировать</a></td>
            </tr>
            <? endforeach; ?>

            </tbody>
        </table>
    </div>
</div>