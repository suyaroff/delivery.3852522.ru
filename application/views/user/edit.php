<div class="box box-info">

    <!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <?php if (validation_errors()): ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php endif; ?>
        <?php if ($success): ?>
            <div class="alert alert-success">
                <?php echo $success; ?>
            </div>
        <?php endif; ?>

        <?php echo form_open('', array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal')); ?>

        <div class="form-group">
            <label for="selectRole" class="col-sm-2 control-label">Роль</label>
            <div class="col-sm-10">
                <select class="form-control" name="role">
                    <?php foreach ($this->config->item('roles') as $role_id => $role): ?>
                        <option value="<? echo $role_id ; ?>" <? if($role_id == $user->role):?>selected="selected"<? endif;?>><? echo $role ;?></option>
                    <? endforeach; ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="input0" class="col-sm-2 control-label">Логин</label>
            <div class="col-sm-10">
                <input type="text" name="login" value="<?php echo $user->login; ?>" id="input0"
                       class="form-control" autocomplete="off" >
            </div>
        </div>


        <div class="form-group">
            <label for="input1" class="col-sm-2 control-label">Имя</label>
            <div class="col-sm-10">
                <input type="text" name="first_name" value="<?php echo $user->first_name; ?>"
                       class="form-control"  autocomplete="off" >
            </div>
        </div>

        <div class="form-group">
            <label for="input2" class="col-sm-2 control-label">Фамилия</label>
            <div class="col-sm-10">
                <input type="text" name="last_name" value="<?php echo $user->last_name; ?>"
                       class="form-control"   autocomplete="off" >
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="col-sm-2 control-label">Пароль</label>
            <div class="col-sm-10">
                <input type="password" name="password" class="form-control"  autocomplete="off">
            </div>
        </div>

        <div class="form-group">
            <label for="input3" class="col-sm-2 control-label">Email</label>

            <div class="col-sm-10">
                <input type="email" name="email" class="form-control" value="<?php echo $user->email; ?>"
                       autocomplete="off">
            </div>
        </div>
        <div class="form-group">
            <label for="input4" class="col-sm-2 control-label">Телефон</label>

            <div class="col-sm-10">
                <input type="text" name="phone" class="form-control" value="<?php echo $user->phone; ?>"
                         autocomplete="off">
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">
                <a target="_blank" href="https://yandex.ru/map-constructor/location-tool/">Координаты</a>
            </label>
            <div class="col-sm-5">
                <input type="text" name="latitude" class="form-control" value="<?php echo $user->latitude; ?>" placeholder="Широта"  autocomplete="off">
            </div>
            <div class="col-sm-5">
                <input type="text" name="longitude" class="form-control" value="<?php echo $user->longitude; ?>" placeholder="Долгота"  autocomplete="off">
            </div>

        </div>



    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <a class="btn btn-danger" href="/user/delete/<?php echo $user->id; ?>">Удалить</a>
        <button type="submit" class="btn btn-info pull-right">Сохранить</button>
    </div>
    <!-- /.box-footer -->
    </form>
</div>
