<div class="box box-info">

    <!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <?php if (validation_errors()): ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php endif; ?>
        <?php if ($success): ?>
            <div class="alert alert-success">
                <?php echo $success; ?>
            </div>
        <?php endif; ?>

        <?php echo form_open('', array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal')); ?>

        <div class="form-group">
            <label for="selectRole" class="col-sm-2 control-label">Роль</label>
            <div class="col-sm-10">
                <select class="form-control" name="role">
                    <?php foreach ($this->config->item('roles') as $role_id => $role): ?>
                    <option value="<? echo $role_id ; ?>"><? echo $role ;?></option>
                    <? endforeach; ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="input0" class="col-sm-2 control-label">Логин</label>
            <div class="col-sm-10">
                <input type="text" name="login" value="<?php if(!$success) echo set_value('login'); ?>" id="input0"
                       class="form-control" placeholder="Логин" >
            </div>
        </div>


        <div class="form-group">
            <label for="input1" class="col-sm-2 control-label">Имя</label>
            <div class="col-sm-10">
                <input type="text" name="first_name" value="<?php if(!$success)  echo set_value('first_name'); ?>" id="input1"
                       class="form-control" placeholder="Имя" >
            </div>
        </div>

        <div class="form-group">
            <label for="input2" class="col-sm-2 control-label">Фамилия</label>
            <div class="col-sm-10">
                <input type="text" name="last_name" value="<?php if(!$success)  echo set_value('last_name'); ?>" id="input2"
                       class="form-control" placeholder="Фамилия" >
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="col-sm-2 control-label">Пароль</label>
            <div class="col-sm-10">
                <input type="password" name="password" class="form-control" id="inputPassword" placeholder="12345">
            </div>
        </div>

        <div class="form-group">
            <label for="input3" class="col-sm-2 control-label">Email</label>

            <div class="col-sm-10">
                <input type="email" name="email" class="form-control" value="<?php if(!$success)  echo set_value('email'); ?>"
                       id="input3" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label for="input4" class="col-sm-2 control-label">Телефон</label>

            <div class="col-sm-10">
                <input type="text" name="phone" class="form-control" value="<?php  if(!$success)  echo set_value('phone'); ?>"
                       id="input4" placeholder="Телефон">
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">
                <a target="_blank" href="https://yandex.ru/map-constructor/location-tool/">Координаты</a>
            </label>
            <div class="col-sm-5">
                <input type="text" name="latitude" class="form-control" value="<?php  if(!$success)  echo set_value('latitude'); ?>" placeholder="Широта">
            </div>
            <div class="col-sm-5">
                <input type="text" name="longitude" class="form-control" value="<?php  if(!$success)  echo set_value('longitude'); ?>" placeholder="Долгота">
            </div>

        </div>


    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <button type="submit" class="btn btn-info pull-right">Добавить</button>
    </div>
    <!-- /.box-footer -->
    </form>
</div>