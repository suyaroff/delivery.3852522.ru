<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        2017 - <? echo date('Y');?>
    </div>
    <!-- Default to the left -->
    <strong>&copy; <a href="/"><? echo $_SERVER['SERVER_NAME'];?></a> </strong>
</footer>