<nav class="navbar navbar-static-top">
	<div class="container-fluid">
		<? if (SYSTEM_NAME == 'delivery') { ?>
			<div class="navbar-header">
				<a href="/" class="navbar-brand">Delivery <? if(strstr($_SERVER['SERVER_NAME'], '.loc')) echo " - LOCAL ";?></a>
				<?php if ($this->session->user->role == 3): /* исполнитель */ ?>
					<a href="/plan/my" class="navbar-brand hidden-lg hidden-md hidden-sm">План доставок</a>
				<? endif; ?>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<ul class="nav navbar-nav">
					<?php if ($this->session->user->role == 1): /* админ */ ?>
						<li class="<? if ($this->router->class == 'stock'): ?>active<? endif; ?>"><a href="/stock">Склад</a></li>
						<li class="<? if ($this->router->class == 'plan'): ?>active<? endif; ?>"><a href="/plan">План доставок</a></li>

						<li class="dropdown <? if ($this->router->class == 'user'): ?>active<? endif; ?>">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Настройки <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="/user">Список пользователей</a></li>
								<li><a href="/user/add">Добавить пользователя</a></li>
							</ul>
						</li>

						<li class="dropdown <? if ($this->router->class == 'info'): ?>active<? endif; ?>">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Справочники <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="/info/contractor_list">Контрагенты</a></li>
								<li><a href="/info/contractor_add">Добавить контрагента</a></li>
								<li class="divider"></li>
								<li><a href="/info/forwarder_list">Экспедиторы</a></li>
								<li><a href="/info/forwarder_add">Добавить экспедитора</a></li>
							</ul>
						</li>
					<? endif; ?>
					<?php if ($this->session->user->role == 3): /* исполнитель */ ?>
						<li class="<? if ($this->router->class == 'plan'): ?>active<? endif; ?>"><a href="/plan/my">План доставок</a></li>
					<? endif; ?>
					<?php if ($this->session->user->role == 4): /* склад */ ?>
						<li class="<? if ($this->router->class == 'stock'): ?>active<? endif; ?>"><a href="/stock">Склад</a></li>
						<li class="<? if ($this->router->class == 'plan'): ?>active<? endif; ?>"><a href="/plan">План доставок</a></li>
					<? endif; ?>
					<li class="<? if ($this->router->class == 'order' && $this->router->method == 'history'): ?>active<? endif; ?>"><a href="/order/history">История</a></li>
					<li><a href="http://www.zakupki.3852522.ru">перейти на закупки</a></li>

				</ul>

			</div>
			<!-- /.navbar-collapse -->
		<? } ?>
		<? if (SYSTEM_NAME == 'buys') { ?>
			<div class="navbar-header">
				<a href="/buys" class="navbar-brand">Закупки</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>
			<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<ul class="nav navbar-nav">
					<?php if ($this->session->user->role != 5) { ?>
						<li><a href="/buys/add">Добавить</a></li>
					<? } ?>
					<li><a href="/buys/calendar">Календарь</a></li>
					<?php if ($this->session->user->role == 1) {  /* админ */ ?>
						<li><a href="/buys/deleted">Удаленные</a></li>
						<li><a href="/buys/statistic">Статистика</a></li>
						<li class="dropdown <? if ($this->router->class == 'user'): ?>active<? endif; ?>">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Настройки <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="/user">Список пользователей</a></li>
								<li><a href="/user/add">Добавить пользователя</a></li>
								<li><a href="/buys/partners">От кого участвуем</a></li>
							</ul>
						</li>
					<? } ?>
					<?php if ($this->session->user->role != 5) { ?>
						<li><a href="http://delivery.3852522.ru">перейти на delivery</a></li>
					<? } ?>
				</ul>
				<form class="navbar-form navbar-left" role="search" action="/buys/search" method="get">
					<div class="form-group">
						<input type="text" class="form-control" name="str" id="navbar-search-input" placeholder="Поиск">
					</div>
				</form>

			</div>
		<? } ?>
		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">

				<!-- User Account Menu -->
				<li class="dropdown user user-menu">
					<!-- Menu Toggle Button -->
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<!-- The user image in the navbar-->
						<img src="/upload/<? echo $this->session->user->photo; ?>" class="user-image" alt="User Image">

						<span class="hidden-xs"><? echo $this->session->user->first_name; ?> <? echo $this->session->user->last_name; ?> </span>
					</a>
					<ul class="dropdown-menu">
						<!-- The user image in the menu -->
						<li class="user-header">
							<img src="/upload/<? echo $this->session->user->photo; ?>" class="img-circle" alt="User Image">
							<p>
								<? echo $this->session->user->first_name; ?> <? echo $this->session->user->last_name; ?>
								- <? echo $this->config->item($this->session->user->role, 'roles'); ?>
								<small>Зарегистрирован <? echo my_date($this->session->user->registration_date); ?></small>
							</p>
						</li>

						<!-- Menu Footer-->
						<li class="user-footer">
							<?php if (!$this->session->user->role == 3) : /* исполнитель */ ?>
								<div class="pull-left">
									<a href="/user/profile" class="btn btn-default btn-flat">Настройки</a>
								</div>
							<? endif; ?>
							<div class="pull-right">
								<a href="/auth/logout" class="btn btn-default btn-flat">Выход</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<!-- /.navbar-custom-menu -->
	</div>
	<!-- /.container-fluid -->
</nav>
