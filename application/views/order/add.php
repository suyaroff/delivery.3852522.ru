<div class="box box-info">
	<!-- /.box-header -->
	<!-- form start -->
	<div class="box-body">
		<?php if (validation_errors()): ?>
			<div class="alert alert-danger">
				<?php echo validation_errors(); ?>
			</div>
		<?php endif; ?>
		<?php if ($success): ?>
			<div class="alert alert-success">
				<?php echo $success; ?>
			</div>
		<?php endif; ?>

		<?php echo form_open('', array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal')); ?>

		<div class="form-group">
			<label class="col-sm-2 control-label">Экспедитор</label>
			<div class="col-sm-10">
				<select class="form-control" name="forwarder_id">
					<?php foreach ($forwarders as $f): ?>
						<option value="<? echo $f['id']; ?>" <? echo set_select('forwarder_id', $f['id']); ?>
						><? echo $f['first_name'] . ' ' . $f['last_name']; ?></option>
					<? endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Тип операции</label>
			<div class="col-sm-10">
				<select class="form-control" name="operation">
					<?php foreach ($this->config->item('orders_operation') as $operation_id => $operation): ?>
						<option value="<? echo $operation_id; ?>" <? echo set_select('operation', $operation_id); ?> ><? echo $operation; ?></option>
					<? endforeach; ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Накладные</label>
			<div class="col-sm-10">
				<div id="invoice_box">
					<? if(isset($_POST['invoice'])){ foreach ($_POST['invoice'] as $invoice) { ?>
						<div class="input-group">
							<input type="text" class="form-control" name="invoice[]" value="<? echo $invoice;?>">
							<div class="input-group-btn">
								<button type="button" class="btn btn-danger" onclick="remove_invoice_field(this)">Удалить</button>
							</div>
						</div>
					<? } } else { ?>
					<div class="input-group">
						<input type="text" class="form-control" name="invoice[]">
						<div class="input-group-btn">
							<button type="button" class="btn btn-danger" onclick="remove_invoice_field(this)">Удалить</button>
						</div>
					</div>
					<? } ?>
				</div>

				<button type="button" class="btn btn-sm btn-success" onclick="add_invoice_field();">Добавить поле для накладной</button>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Контрагент</label>
			<div class="col-sm-5">
				<select class="form-control select2" name="contractor" onchange="contractor_contacts(this.value, '#contractor_contact')">
					<option value="0">Выберите</option>
					<?php foreach ($contractors as $c): ?>
						<option value="<? echo $c['id']; ?>" <? echo set_select('contractor', $c['id']); ?>><? echo $c['name']; ?></option>
					<? endforeach; ?>
				</select>

			</div>
			<div class="col-sm-5">
				<select class="form-control" name="contractor_contact" id="contractor_contact">
					<?php foreach ($contractor_contact as $cc): ?>
						<option value="<? echo $cc['id']; ?>" <? echo set_select('contractor_contact', $cc['id']); ?>><? echo $cc['contact']; ?></option>
					<? endforeach; ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Примечание</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="note" value="<?php if (!$success) echo set_value('note'); ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Назначить после</label>
			<div class="col-sm-10">
				<div class="input-group ">
					<input type="text"
						   class="form-control order-field"
						   name="next_order"
						   id="next_order"
						   value="">
					<span class="input-group-btn">
                          <button type="button" class="btn btn-info" title="найти"
								  onclick="order_check(this, $('#next_order') );"><i class="fa fa-search"></i></button>
                    </span>
				</div>


			</div>
		</div>

	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<button type="submit" class="btn btn-info pull-right">Добавить</button>
	</div>
	<!-- /.box-footer -->
	</form>
</div>
