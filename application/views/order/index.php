<div class="modal fade" id="order_form">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
</div>
<?php if ($this->session->user->role == 1) { /* админ */ ?>
	<p><a href="/order/add" class="btn btn-primary">Добавить</a></p>
<? } ?>

<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Фильтр</h3>
	</div>
	<form class="form-horizontal" action="" method="get">
		<div class="box-body">
			<div class="form-group">
				<label class="col-sm-2 control-label">Статус</label>
				<div class="col-sm-10">
					<select name="status" class="form-control">
						<option value="">Выберите статус</option>
						<? foreach ($this->config->item('orders_status') as $s_id => $s_name) { ?>
							<option
								value="<? echo $s_id; ?>" <? if (isset($_GET['status']) && !empty($_GET['status']) && $_GET['status'] == $s_id) echo 'selected'; ?> ><? echo $s_name; ?></option>
						<? } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Инициатор</label>
				<div class="col-sm-10">
					<select name="author_id" class="form-control select2">
						<option value="">Выберите из списка</option>
						<? foreach ($users as $u) { ?>
							<option
								value="<? echo $u['id']; ?>" <? if (isset($_GET['author_id']) && $_GET['author_id'] == $u['id']) echo 'selected'; ?> ><? echo $u['first_name'] . ' ' . $u['last_name']; ?></option>
						<? } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Контрагент</label>
				<div class="col-sm-10">
					<select name="contractor" class="form-control select2">
						<option value="">Выберите из списка</option>
						<? foreach ($contractors as $c) { ?>
							<option
								value="<? echo $c['id']; ?>" <? if (isset($_GET['contractor']) && $_GET['contractor'] == $c['id']) echo 'selected'; ?> ><? echo $c['name']; ?></option>
						<? } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Накладная</label>
				<div class="col-sm-10">
					<input type="text" name="invoice" value="<? if (isset($_GET['invoice']) && $_GET['invoice'] != '') echo $_GET['invoice']; ?>"
						   class="form-control" autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Номер счета</label>
				<div class="col-sm-10">
					<input type="text" name="invoice_contractor" value="<? if (isset($_GET['invoice_contractor']) && $_GET['invoice_contractor'] != '') echo $_GET['invoice_contractor']; ?>"
						   class="form-control" autocomplete="off">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Дата доставки</label>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_from" value="<? if (isset($_GET['date_from']) && $_GET['date_from'] != '') echo $_GET['date_from']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="От">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_to" value="<? if (isset($_GET['date_to']) && $_GET['date_to'] != '') echo $_GET['date_to']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="По">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Дата создания</label>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_created_from"
						   value="<? if (isset($_GET['date_created_from']) && $_GET['date_created_from'] != '') echo $_GET['date_created_from']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="От">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_created_to" value="<? if (isset($_GET['date_created_to']) && $_GET['date_created_to'] != '') echo $_GET['date_created_to']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="По">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Дата сборки</label>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_stock_done_from"
						   value="<? if (isset($_GET['date_stock_done_from']) && $_GET['date_stock_done_from'] != '') echo $_GET['date_stock_done_from']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="От">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_stock_done_to"
						   value="<? if (isset($_GET['date_stock_done_to']) && $_GET['date_stock_done_to'] != '') echo $_GET['date_stock_done_to']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="По">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"></label>
				<div class="col-sm-10">
					<button type="submit" name="action" value="filter" class="btn btn-info">Фильтровать</button>
					<a href="/" class="btn btn-default pull-right"> Сбросить фильтр</a>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<? foreach ($this->config->item('orders_status') as $id => $name) { ?>
			<li class="<? if ($id == 0) {
				echo 'active';
			} ?>">
				<a href="#tab_<? echo $id; ?>" data-toggle="tab">
					<? echo $name; ?> (<? if (isset($orders[$id])) {
						echo count($orders[$id]);
					} else {
						echo '0';
					} ?>)
				</a>
			</li>
		<? } ?>
	</ul>
	<div class="tab-content">
		<? foreach ($this->config->item('orders_status') as $id => $name) { ?>
			<div class="tab-pane <? if ($id == 0) {
				echo 'active';
			} ?>" id="tab_<? echo $id; ?>">
				<? if (isset($orders[$id])) {
					foreach ($orders[$id] as $order) { ?>
						<div id="boxOrder_<? echo $order['id']; ?>" class="box box-solid <? echo $this->config->item($order['status'], 'orders_status_color'); ?>">
							<div class="box-header">
								<h3 class="box-title">
									<a href="/order/view/<? echo $order['id']; ?>">Сборка № <? echo $order['id']; ?>
										, <? echo $this->config->item($order['operation'], 'orders_operation'); ?></a>
								</h3>
								<div class="box-tools pull-right">
									<a class="btn btn-box-tool"
									   title="Редактровать"
									   data-toggle="modal"
									   data-target="#order_form"
									   href="/order/edit/<? echo $order['id']; ?>"><i class="fa fa-wrench"></i></a>
									<? if ($order['status'] == 0) { ?>
										<a class="btn btn-box-tool"
										   title="Удалить"
										   data-toggle="modal"
										   data-target="#order_form"
										   href="/order/delete/<? echo $order['id']; ?>/"><i class="fa fa-trash"></i></a>
									<? } ?>
								</div>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="col-md-4">
										<b>Дата создания:</b> <? if ($order['date_created']) echo my_date($order['date_created'], "d-m-Y H:i:s"); else echo 'Не указана'; ?><br>
										<? if ($order['author_id']) { ?><b>Инициатор:</b> <? echo $order['author_first_name'] . ' ' . $order['author_last_name']; ?><br><? } ?>
										<b>Дата доставки:</b> <? if ($order['delivery_date']) echo my_date($order['delivery_date']); else echo 'Не указана'; ?><br>
										<b>Статус:</b> <? echo $this->config->item($order['status'], 'orders_status'); ?><br>
										<b>Статус склада:</b> <span
											class="label <? echo $this->config->item($order['stock_status'], 'stock_status_label'); ?>"><? echo $this->config->item($order['stock_status'], 'stock_status'); ?></span><br>
										<b>Экспедитор:</b>
										<? if ($order['forwarder_id']) { ?>
											<span data-html="true" data-toggle="tooltip"
												  title="Номер телефона:<? echo $order['forwarder_phone']; ?> <br>
													  Машина: <? echo $order['forwarder_auto_name']; ?><br>
													  Номер: <? echo $order['forwarder_auto_number']; ?>">
													<? echo $order['forwarder_first_name'] . ' ' . $order['forwarder_last_name']; ?>
											  </span>
										<? } else {
											echo 'Не назначен';
										} ?><br>
										<? if ($order['note_stock']) { ?><b>Примечания склада:</b> <? echo $order['note_stock']; ?><br><? } ?>
									</div>
									<div class="col-md-4">
										<b>Контрагент:</b> <? echo $order['contractor_name']; ?><br>
										<b>Контакты:</b> <? echo $order['contact_name']; ?><br>
										<b>Адрес:</b> <? echo $order['address_name']; ?><br>
										<? if ($order['contact_info'] || $order['contact_map']) { ?>
											<b>Примечания контакта:</b> <? echo $order['contact_info']; ?><? echo $order['contact_map']; ?><br>
										<? } ?>
										<b>Накладные:</b> <? if ($order['invoice']) echo $order['invoice']; else echo 'Не указано'; ?><br>
										<b>Номер счета:</b> <? if ($order['invoice_contractor']) echo $order['invoice_contractor']; else echo 'Не указано'; ?><br>
									</div>
									<div class="col-md-4">

										<b>Примечания:</b> <? if ($order['note']) echo $order['note']; else echo 'Не указано'; ?><br>
										<b>Назначить после:</b> <? if ($order['next_order']) echo $order['next_order']; else echo 'Не указано'; ?><br>
										<b>Вес :</b> <? echo $order['weight']; ?>
										<b>Мест :</b> <? echo $order['place']; ?>
										<? if (isset($order['files'])) { ?>
											<div>
												<? foreach ($order['files'] as $f) { ?>
													<a target="_blank" href="/upload/orders/<? echo $f['order_id'] . '/' . $f['file_name']; ?>"><? echo $f['file_name']; ?></a> ,
												<? } ?>
											</div>
										<? } ?>
									</div>
								</div>
							</div>
						</div>
					<? }
				} ?>
			</div>
		<? } ?>
	</div>
</div>
<?php if ($this->session->user->role == 1) { /* админ */ ?>
	<div class="box-footer">
		<a href="/order/add" class="btn btn-primary">Добавить</a>
	</div>
<? } ?>

