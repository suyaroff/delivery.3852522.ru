<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Редактирование сборки № <? echo $order->id;?></h4>
</div>
<div class="modal-body">
    <?php echo form_open('', array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'order_modal_edit_form')); ?>
    <div class="form-group">
        <label class="col-sm-3 control-label">Экспедитор</label>
        <div class="col-sm-9">
            <select class="form-control order-field" name="forwarder_id">
                <?php foreach ($forwarders as $f): ?>
                    <option value="<? echo $f['id'] ; ?>" <? if($order->forwarder_id == $f['id']) {?>selected="selected" <? } ?>  >
                    <? echo $f['first_name']. ' '.$f['last_name'] ; ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group ">
        <label class="col-sm-3 control-label">Тип операции</label>
        <div class="col-sm-9">
            <select class="form-control order-field" name="operation">
                <?php foreach ($this->config->item('orders_operation') as $operation_id => $operation): ?>
                    <option value="<? echo $operation_id ; ?>" <? if($order->operation == $operation_id) {?>selected="selected" <? } ?> ><? echo $operation ;?></option>
                <? endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Статус</label>
        <div class="col-sm-9">
            <select class="form-control order-field" name="status">
                <?php foreach ($this->config->item('orders_status') as $status_id => $status): ?>
                    <option value="<? echo $status_id ; ?>" <? if($order->status == $status_id) {?>selected="selected" <? } ?> ><? echo $status ;?></option>
                <? endforeach; ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Дата</label>
        <div class="col-sm-9">
            <input name="delivery_date" type="text" class="form-control datepicker-field order-field" value="<? echo my_date($order->delivery_date);?>"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Накладные</label>
        <div class="col-sm-9">
            <textarea name="invoice" class="form-control order-field"><? echo $order->invoice;?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Контрагент</label>
        <div class="col-sm-9">
            <select class="form-control select2 order-field" name="contractor" onchange="contractor_contacts(this.value, '#contractor_contact')">
                <?php foreach ($contractors as $c): ?>
                    <option value="<? echo $c['id'] ; ?>"  <? if($order->contractor == $c['id']) {?>selected="selected" <? } ?> ><? echo $c['name'] ; ?></option>
                <? endforeach; ?>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Контакт</label>
        <div class="col-sm-9">
            <select class="form-control order-field" name="contractor_contact" id="contractor_contact">
                <?php foreach ($contractor_contact as $cc): ?>
                    <option value="<? echo $cc['id'] ; ?>" <? if($order->contractor_contact == $cc['id']) {?>selected="selected" <? } ?> ><? echo $cc['contact'] ; ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Номер счета</label>
		<div class="col-sm-9">
			<input name="invoice_contractor" type="text" class="form-control order-field" value="<? echo $order->invoice_contractor; ?>"/>
		</div>
	</div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Примечание</label>
        <div class="col-sm-9">
            <textarea name="note" class="form-control order-field"><? echo $order->note;?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Назначить после</label>
        <div class="col-sm-9">
            <div class="input-group ">
                <input type="text"
                       class="form-control order-field"
                       name="next_order"
                       id="next_order"
                       value="<? echo $order->next_order;?>">
                <span class="input-group-btn">
                      <button type="button" class="btn btn-info" title="найти" onclick="order_check(this, $('#next_order') );"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Вес</label>
        <div class="col-sm-9">
            <input name="weight" type="text" class="form-control  order-field" value="<? echo $order->weight;?>"/>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-3 control-label">Фаил</label>
        <div class="col-sm-9">
            <input name="doc" type="file" class="form-control  order-field"/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <button type="button" class="btn btn-info button-save" data-dismiss="modal" onclick="order_save(<? echo $order->id;?>)">Сохранить</button>
        </div>
    </div>
</form>
</div>
<script>
    $('.datepicker-field').datepicker({
        autoclose: true,
        format: "dd-mm-yyyy",
        language: 'ru',
        todayHighlight: true
    });
</script>
