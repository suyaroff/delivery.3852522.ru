<div class="modal fade"  id="order_form" >
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
</div>
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Фильтр</h3>
	</div>
	<form class="form-horizontal" action="" method="get">
		<div class="box-body">
			<div class="form-group">
				<label class="col-sm-2 control-label">Статус</label>
				<div class="col-sm-10">
					<select name="status" class="form-control">
						<option value="">Выберите статус</option>
						<? foreach ($this->config->item('orders_status') as $s_id => $s_name) { ?>
							<option value="<? echo $s_id; ?>" <? if (isset($_GET['status']) && !empty($_GET['status']) && $_GET['status'] == $s_id) echo 'selected'; ?> ><? echo $s_name; ?></option>
						<? } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Статус склада</label>
				<div class="col-sm-10">
					<select name="stock_status" class="form-control">
						<option value="">Выберите статус склада</option>
						<? foreach ($this->config->item('stock_status') as $s_id => $s_name ) { ?>
							<option value="<? echo $s_id;?>" <? if(isset($_GET['stock_status']) && !empty($_GET['stock_status']) && $_GET['stock_status'] == $s_id) echo 'selected';?> ><? echo $s_name;?></option>
						<? } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Инициатор</label>
				<div class="col-sm-10">
					<select name="author_id" class="form-control select2">
						<option value="">Выберите из списка</option>
						<? foreach ($users as $u) { ?>
							<option value="<? echo $u['id']; ?>" <? if (isset($_GET['author_id']) && $_GET['author_id'] == $u['id']) echo 'selected'; ?> ><? echo $u['first_name'].' '.$u['last_name']; ?></option>
						<? } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Контрагент</label>
				<div class="col-sm-10">
					<select name="contractor" class="form-control select2">
						<option value="">Выберите из списка</option>
						<? foreach ($contractors as $c) { ?>
							<option value="<? echo $c['id']; ?>" <? if (isset($_GET['contractor']) && $_GET['contractor'] == $c['id']) echo 'selected'; ?> ><? echo $c['name']; ?></option>
						<? } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Накладная</label>
				<div class="col-sm-10">
					<input type="text" name="invoice" value="<? if (isset($_GET['invoice']) && $_GET['invoice'] != '') echo $_GET['invoice']; ?>"
						   class="form-control" autocomplete="off" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Номер счета</label>
				<div class="col-sm-10">
					<input type="text" name="invoice_contractor" value="<? if (isset($_GET['invoice_contractor']) && $_GET['invoice'] != '') echo $_GET['invoice_contractor']; ?>"
						   class="form-control" autocomplete="off" >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Дата доставки</label>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_from" value="<? if (isset($_GET['date_from']) && $_GET['date_from'] != '') echo $_GET['date_from']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="От">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_to" value="<? if (isset($_GET['date_to']) && $_GET['date_to'] != '') echo $_GET['date_to']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="По">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Дата создания</label>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_created_from" value="<? if (isset($_GET['date_created_from']) && $_GET['date_created_from'] != '') echo $_GET['date_created_from']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="От">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_created_to" value="<? if (isset($_GET['date_created_to']) && $_GET['date_created_to'] != '') echo $_GET['date_created_to']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="По">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Дата сборки</label>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_stock_done_from" value="<? if (isset($_GET['date_stock_done_from']) && $_GET['date_stock_done_from'] != '') echo $_GET['date_stock_done_from']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="От">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2">
					<input type="text" name="date_stock_done_to" value="<? if (isset($_GET['date_stock_done_to']) && $_GET['date_stock_done_to'] != '') echo $_GET['date_stock_done_to']; ?>"
						   class="form-control datepicker-field" autocomplete="off" placeholder="По">
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-2 control-label"></label>
				<div class="col-sm-10">
					<button type="submit" name="action" value="filter" class="btn btn-info">Фильтровать</button>
					<a href="/order/history" class="btn btn-default pull-right"> Сбросить фильтр</a>
				</div>
			</div>
		</div>
	</form>
</div>
<? if (isset($_GET['action'])) { ?>
	<? if (count($orders)) {  ?>
		<? foreach ($orders as $order) { ?>
			<div id="boxOrder_<? echo $order['id']; ?>" class="box box-solid <? echo $this->config->item($order['status'], 'orders_status_color'); ?>">
				<div class="box-header">
					<h3 class="box-title">
						<a href="/order/view/<? echo $order['id']; ?>">Сборка № <? echo $order['id']; ?>
							, <? echo $this->config->item($order['operation'], 'orders_operation'); ?></a>
					</h3>
					<div class="box-tools pull-right">
						<a class="btn btn-box-tool"
						   title="Редактровать"
						   data-toggle="modal"
						   data-target="#order_form"
						   href="/stock/edit/<? echo $order['id'];?>"><i class="fa fa-wrench"></i></a>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-3">

							<b>Статус:</b> <? echo $this->config->item($order['status'], 'orders_status'); ?><br>

							<div>
								<b>Статус склада:</b>
								<span class="label <? echo $this->config->item($order['stock_status'], 'stock_status_label'); ?>"><? echo $this->config->item($order['stock_status'], 'stock_status'); ?></span>
							</div>

							<b>Инициатор:</b> <? echo $order['author_first_name'] . ' ' . $order['author_last_name']; ?><br>
							<b>Экспедитор:</b>
							<? if ($order['forwarder_id']) { ?>
								<span data-html="true" data-toggle="tooltip"
									  title="Номер телефона:<? echo $order['forwarder_phone']; ?> <br>
										  Машина: <? echo $order['forwarder_auto_name']; ?><br>
										  Номер: <? echo $order['forwarder_auto_number']; ?>">
                        			<? echo $order['forwarder_first_name'] . ' ' . $order['forwarder_last_name']; ?>
                  				</span>
							<? } else {
								echo 'Не назначен';
							} ?><br>


						</div>
						<div class="col-sm-3">
							<b>Контрагент:</b> <? echo $order['contractor_name']; ?><br>
							<b>Контакты:</b> <? echo $order['contact_name']; ?><br>
							<b>Адрес:</b> <? echo $order['address_name']; ?><br>
							<? if ($order['contact_info'] || $order['contact_map']) { ?>
								<b>Примечания контакта:</b> <? echo $order['contact_info']; ?><? echo $order['contact_map']; ?><br>
							<? } ?>
							<b>Накладные:</b> <? if ($order['invoice']) echo $order['invoice']; else echo 'Не указано'; ?><br>
							<b>Номер счета:</b> <? if ($order['invoice_contractor']) echo $order['invoice_contractor']; else echo 'Не указано'; ?><br>
						</div>
						<div class="col-sm-3">
							<b>Примечания:</b> <? if ($order['note']) echo $order['note']; else echo 'Не указано'; ?><br>
							<b>Назначить после:</b> <? if ($order['next_order']) echo $order['next_order']; else echo 'Не указано'; ?><br>
							<b>Вес :</b> <? echo $order['weight']; ?>
							<b>Мест :</b> <? echo $order['place']; ?>
							<b>Объем :</b> <? echo $order['volume']; ?>м³
							<? if ($order['note_stock']) { ?><br><b>Примечания склада:</b> <? echo $order['note_stock']; ?><? } ?>
							<? if (isset($order['files'])) { ?>
								<div>
									<? foreach ($order['files'] as $f) { ?>
										<a target="_blank" href="/upload/orders/<? echo $f['order_id'] . '/' . $f['file_name']; ?>"><? echo $f['file_name']; ?></a> ,
									<? } ?>
								</div>
							<? } ?>

						</div>
						<div class="col-sm-3">
							<b>Дата создания:</b> <? if ($order['date_created']) echo my_date($order['date_created']); ?><br>
							<b>Дата по обновления:</b> <? if ($order['date_updated']) echo my_date($order['date_updated']);  ?><br>
							<b>Дата сборки:</b> <? if ($order['date_stock_done']) echo my_date($order['date_stock_done']); else echo 'Не указана'; ?><br>
							<b>Дата по плану:</b> <? if ($order['delivery_date']) echo my_date($order['delivery_date']); else echo 'Не указана'; ?><br>
						</div>
					</div>
				</div>
			</div>
		<? } ?>
	<? } else { ?>
		<div class="alert alert-danger">Сборки не найдены</div>
	<? }
} ?>
