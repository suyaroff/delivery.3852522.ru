<div class="box">
	<div class="box-header">
		<h3 class="box-title">Сборка № <? echo $order->id; ?></h3>
	</div>
	<div class="box-body">
		<table class="table table-borders table-hover">
			<tr>
				<td><b>Инициатор:</b></td>
				<td><? echo $order->author_first_name . ' ' . $order->author_last_name; ?></td>
			</tr>
			<tr>
				<td><b>Экспедитор:</b></td>
				<td><? echo $order->forwarder_first_name . ' ' . $order->forwarder_last_name; ?></td>
			</tr>

			<tr>
				<td><b>Тип операции:</b></td>
				<td><? echo $this->config->item($order->operation, 'orders_operation'); ?></td>
			</tr>
			<tr>
				<td><b>Статус:</b></td>
				<td><? echo $this->config->item($order->status, 'orders_status'); ?></td>
			</tr>
			<tr>
				<td><b>Дата по плану:</b></td>
				<td><? echo my_date($order->delivery_date); ?></td>
			</tr>
			<tr>
				<td><b>Накладные</b></td>
				<td><? echo $order->invoice; ?></td>
			</tr>
			<tr>
				<td><b>Контрагент</b></td>
				<td><? echo $order->contractor_name; ?>, <? echo $order->contact_name; ?>, <? echo $order->contact_info; ?>, <? echo $order->address_name; ?> </td>
			</tr>
			<tr>
				<td><b>Примечание</b></td>
				<td><? echo $order->note; ?></td>
			</tr>
			<tr>
				<td><b>Назначить после</b></td>
				<td><? echo $order->next_order; ?></td>
			</tr>
			<tr>
				<td><b>Вес</b></td>
				<td><? echo $order->weight; ?></td>
			</tr>
			<tr>
				<td><b>Мест</b></td>
				<td><? echo $order->place; ?></td>
			</tr>
			<tr>
				<td><b>Объем</b></td>
				<td><? echo $order->volume; ?></td>
			</tr>

			<tr>
				<td><b>Статус склада</b></td>
				<td><span class="label <? echo $this->config->item($order->stock_status, 'stock_status_label'); ?>"><? echo $this->config->item($order->stock_status, 'stock_status'); ?></span></td>
			</tr>
			<tr>
				<td><b>Примечание склада</b></td>
				<td><? echo $order->note_stock; ?></td>
			</tr>
			<tr>
				<td><b>Дата создания:</b></td>
				<td><? echo my_date($order->date_created, 'd-m-Y H:i:s'); ?></td>
			</tr>
			<tr>
				<td><b>Дата обновления:</b></td>
				<td><? echo my_date($order->date_updated, "d-m-Y H:i:s"); ?></td>
			</tr>
			<tr>
				<td><b>Дата сборки:</b></td>
				<td><? echo my_date($order->date_stock_done, "d-m-Y H:i:s"); ?></td>
			</tr>
			<tr>
				<td><b>Файлы:</b></td>
				<td>
					<? foreach ($order->files as $f) { ?>
						<a target="_blank" href="/upload/orders/<? echo $f['order_id'] . '/' . $f['file_name']; ?>"><? echo $f['file_name']; ?> <i class="fa fa-external-link"></i></a>
					<? } ?>
				</td>
			</tr>

		</table>
	</div>
</div>
<div class="box">
	<div class="box-header">
		<h3 class="box-title">Журнал</h3>
	</div>
	<div class="box-body table-responsive no-padding">
		<table class="table table-hover">
			<thead>
			<tr>
				<th style="width: 140px;">Время</th>
				<th>Пользователь</th>
				<th>Данные формы</th>
			</tr>
			</thead>
			<tbody>
			<? foreach ($logs as $l) { ?>
				<tr>
					<td><? echo my_date($l['log_time'], 'd-m-Y H:i:s');?></td>
					<td><? echo $l['user_first_name'].' '.$l['user_last_name'];?></td>
					<td><? echo $l['description'];?></td>
				</tr>
			<? } ?>
			</tbody>
		</table>
	</div>
</div>

