<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Удалить заявку № <? echo $id;?></h4>
</div>
<div class="modal-body" id="modalOrderDelete">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 margin-bottom">
                <button type="button" class="btn btn-block " data-dismiss="modal">Нет</button>
            </div>
            <div class="col-sm-6">
                <button type="button" class="btn btn-block" onclick="order_delete(<? echo $id;?>);">Да</button>
            </div>
        </div>
    </div>
    <p></p>
</div>