<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Вход</title>

    <base href="/">
    <link rel="icon" type="image/png" href="/favicon.png" sizes="16x16">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="assets/app/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
		.login-page{
			background: url("/assets/app/img/bg/delivery.jpg") no-repeat;
			background-size: auto ;
			display: flex;
			align-items: center;
			background-position:center bottom;
		}

		.login-logo{
			text-shadow: #ffffff -1px -1px 3px;
		}
		.login-box-body{
			background: rgba(0, 0, 0, 0.4);
		}
	</style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="/"><? echo $_SERVER['SERVER_NAME'];?></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php if( validation_errors()):?>
        <div class="alert alert-danger">
            <?php echo validation_errors(); ?>
        </div>
        <?php endif ;?>
        <?php echo form_open(); ?>
            <div class="form-group has-feedback">
                <input name="login" type="text" value="<?php echo set_value('login'); ?>" class="form-control" placeholder="Логин">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="password" type="password" class="form-control" placeholder="Пароль">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
                </div>

            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="assets/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
