<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><? if (isset($title)) echo $title; ?></title>
	<base href="/">
	<? if (SYSTEM_NAME == 'buys') { ?>
		<link rel="icon" type="image/png" href="/auction-icon.png" sizes="32x32">
	<? } else { ?>
		<link rel="icon" type="image/png" href="/favicon.png" sizes="16x16">
	<? } ?>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">

	<link rel="stylesheet" href="assets/datepicker/datepicker3.css">
	<link rel="stylesheet" href="assets/toastr/toastr.min.css">

	<? if (isset($css)) foreach ($css as $style) echo '<link rel="stylesheet" href="/assets/' . $style . '"> '; ?>


	<link rel="stylesheet" href="assets/app/css/AdminLTE.min.css">

	<link rel="stylesheet" href="assets/app/css/skins/skin-blue.min.css">
	<? if (SYSTEM_NAME == 'buys') { ?>
		<link rel="stylesheet" href="assets/app/css/skins/skin-green-light.min.css">
	<? } ?>
	<link rel="stylesheet" href="assets/app/css/app.css?v1.1.3">

	<!-- jQuery 2.2.3 -->
	<script src="assets/jQuery/jquery-2.2.3.min.js"></script>


	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>

<body class="hold-transition <? if (SYSTEM_NAME == 'buys') echo 'skin-green-light'; else echo 'skin-blue'; ?> layout-top-nav">
<div class="wrapper" id="root">
	<header class="main-header">
		<? include('navbar.php'); ?>
	</header>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<div class="container-fluid">
			<!-- Content Header (Page header) -->
			<section class="content-header"><h1><? if (isset($h1)) echo $h1; ?></h1></section>

			<!-- Main content -->
			<section class="content">
				<? if (isset($content)) echo $content; ?>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /.content-wrapper -->
	<!-- Main Footer -->
	<? include('main_footer.php') ?>
</div>
<!-- ./wrapper -->

<? if ($_SERVER['SERVER_NAME'] == 'delivery.3852522.loc') { ?>
	<script src="app/vue.js"></script>
<? } else { ?>
	<script src="app/vue.min.js"></script>
<? } ?>
<script src="app/vuex.min.js"></script>
<script src="app/axios.min.js"></script>
<script src="assets/toastr/toastr.min.js"></script>
<script src="app/app.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<!-- datepickper -->
<script src="assets/datepicker/bootstrap-datepicker.js"></script>
<script src="assets/datepicker/locales/bootstrap-datepicker.ru.js"></script>
<script src="assets/autosize.min.js"></script>


<link rel="stylesheet" href="/assets/select2/select2.min.css">
<link rel="stylesheet" href="/assets/app/css/alt/AdminLTE-select2.min.css">

<script src="/assets/select2/select2.full.min.js"></script>

<? if (isset($js)) foreach ($js as $script) echo '<script src="/assets/' . $script . '"></script>'; ?>

<script src="assets/app/js/app.min.js"></script>
<script src="assets/app/js/delivery.js?v=0.1.3"></script>
<script src="assets/app/js/buys.js?v=0.1.3"></script>

<? if (isset($javascript)) echo '<script>' . $javascript . '</script>'; ?>

</body>
</html>
