<?php echo form_open('', array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal')); ?>
<div class="box box-info">

    <!-- form start -->
    <div class="box-body">
        <?php if (validation_errors()): ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php endif; ?>
        <?php if ($success): ?>
            <div class="alert alert-success">
                <?php echo $success; ?>
            </div>
        <?php endif; ?>

        <?php echo form_open('', array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal')); ?>


        <div class="form-group">
            <label class="col-sm-2 control-label">Статус</label>
            <div class="col-sm-5">
                <select name="activity_status" class="form-control" onchange="if(this.value == 2) $('#inputActivityDate').show(); else $('#inputActivityDate').hide(); ">
                    <? foreach ($this->config->item('forwarder_status') as $fs_id => $fs) :?>
                    <option value="<? echo  $fs_id;?>" <? if($fs_id == $forwarder['activity_status']):?> selected="selected" <?endif;?> ><? echo  $fs ;?></option>
                    <?  endforeach; ?>
                </select>
            </div>
            <div class="col-sm-5">
                <input type="text"
                       name="activity_date"
                       value="<?php  if($forwarder['activity_date']) echo my_date($forwarder['activity_date']); else echo date('d-m-Y');?>"
                       id="inputActivityDate"
                       class="form-control"
                       placeholder="Дата активности"
                       data-date-format="dd-mm-yyyy"
                       data-date-language="ru"
                       data-date-autoclose="1"
                       data-provide="datepicker"
                       style="<? if($forwarder['activity_status'] != 2):?> display: none; <? endif;?>"
                >
            </div>
        </div>


        <div class="form-group">
            <label for="inputFirstName" class="col-sm-2 control-label">Имя</label>
            <div class="col-sm-10">
                <input type="text" name="first_name" value="<?php echo $forwarder['first_name']; ?>" id="inputFirstName"
                       class="form-control" placeholder="Имя" onkeyup="setLoginName();">
            </div>
        </div>




        <div class="form-group">
            <label for="inputLastName" class="col-sm-2 control-label">Фамилия</label>
            <div class="col-sm-10">
                <input type="text" name="last_name" value="<?php echo $forwarder['last_name']; ?>" id="inputLastName"
                       class="form-control" placeholder="Фамилия" onkeyup="setLoginName();" >
            </div>
        </div>

        <div class="form-group">
            <label for="input4" class="col-sm-2 control-label">Телефон</label>

            <div class="col-sm-10">
                <input type="text" name="phone" class="form-control" value="<?php echo $forwarder['phone']; ?>"
                       id="input4" placeholder="Телефон">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="col-sm-2 control-label">Email</label>

            <div class="col-sm-10">
                <input type="text" name="email" class="form-control" value="<?php echo $forwarder['email']; ?>"
                       id="inputEmail" placeholder="Email">
            </div>
        </div>

        <div class="form-group">
            <label for="input5" class="col-sm-2 control-label">Автомобиль</label>
            <div class="col-sm-10">
                <input type="text" name="auto_name" class="form-control" value="<?php echo $forwarder['auto_name']; ?>"
                       id="input5" placeholder="Пежо бокстер">
            </div>
        </div>
        <div class="form-group">
            <label for="input6" class="col-sm-2 control-label">Госномер</label>
            <div class="col-sm-10">
                <input type="text" name="auto_number" class="form-control" value="<?php echo $forwarder['auto_number']; ?>"
                       id="input6" placeholder="А912 ОК">
            </div>
        </div>
        <div class="form-group">
            <label for="input7" class="col-sm-2 control-label">Примечание</label>
            <div class="col-sm-10">
                <textarea name="note" class="form-control" id="input7" ><?php echo $forwarder['note']; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">
                <a target="_blank" href="https://yandex.ru/map-constructor/location-tool/">Координаты</a>
            </label>
            <div class="col-sm-5">
                <input type="text" name="latitude" class="form-control" value="<?php echo $forwarder['latitude']; ?>" placeholder="Широта">
            </div>
            <div class="col-sm-5">
                <input type="text" name="longitude" class="form-control" value="<?php echo $forwarder['longitude']; ?>" placeholder="Долгота">
            </div>

        </div>

    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <a class="btn btn-danger" href="/info/forwarder_delete/<?php echo $forwarder['id']; ?>">Удалить</a>
        <button type="submit" class="btn btn-info pull-right">Сохранить</button>
    </div>
    <!-- /.box-footer -->
</div>

</form>
