<?php if (validation_errors()): ?>
    <div class="alert alert-danger">
        <?php echo validation_errors(); ?>
    </div>
<?php endif; ?>
<?php if ($success): ?>
    <div class="alert alert-success">
        <?php echo $success; ?>
    </div>
<?php endif; ?>
<?php echo form_open('', array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal')); ?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Контрагент</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label  class="col-sm-2 control-label">Тип</label>
            <div class="col-sm-10">
                <select  class="form-control" name="type">
                    <?php foreach ($this->config->item('contractor_type') as $id => $name): ?>
                        <option value="<? echo $id; ?>"><? echo $name; ?></option>
                    <? endforeach; ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label  class="col-sm-2 control-label">Название</label>
            <div class="col-sm-10">
                <input type="text" name="name" value="<?php if (!$success) echo set_value('name'); ?>"
                       class="form-control" placeholder="ООО САД">
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Адрес</label>
            <div class="col-sm-6">
                <input type="text" name="address" value="<?php if (!$success) echo set_value('address'); ?>"
                       class="form-control" placeholder="Ул. Ленина 38">
            </div>
            <div class="col-sm-2">
                <input type="text" name="latitude" placeholder="Широта"
                       class="form-control" value="<?php if (!$success) echo set_value('latitude'); ?>">
            </div>
            <div class="col-sm-2">
                <input type="text" name="longitude" placeholder="Долгота"
                       class="form-control" value="<?php if (!$success) echo set_value('longitude'); ?>">
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Контактное лицо</label>
            <div class="col-sm-10">
                <input type="text" name="contact" value="<?php if (!$success) echo set_value('contact'); ?>"
                       class="form-control" placeholder="Татьяна иванова тел">
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Примечание</label>
            <div class="col-sm-10">
                <input type="text" name="contact_info" value="<?php if (!$success) echo set_value('contact_info'); ?>"
                       class="form-control" placeholder="Сидит в кабинете 11">
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">На карте</label>
            <div class="col-sm-10">
                <input type="text" name="contact_map" value="<?php if (!$success) echo set_value('contact_map'); ?>"
                       class="form-control" placeholder="ссылка на карту">
            </div>
        </div>


    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-info">Сохранить</button>
    </div>
</div>






</form>