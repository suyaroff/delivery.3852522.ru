<?php if (validation_errors()): ?>
    <div class="alert alert-danger">
        <?php echo validation_errors(); ?>
    </div>
<?php endif; ?>
<contractor-edit id="<? echo $id;?>"></contractor-edit>
