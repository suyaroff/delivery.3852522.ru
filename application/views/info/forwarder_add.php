<?php echo form_open('', array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal')); ?>
<div class="box box-info">

    <!-- form start -->
    <div class="box-body">
        <?php if (validation_errors()): ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
        <?php endif; ?>
        <?php if ($success): ?>
            <div class="alert alert-success">
                <?php echo $success; ?>
            </div>
        <?php endif; ?>

        <?php echo form_open('', array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal')); ?>


        <div class="form-group">
            <label for="inputLogin" class="col-sm-2 control-label">Логин</label>
            <div class="col-sm-10">
                <input type="text" name="login" value="<?php if(!$success)  echo set_value('login'); ?>" id="inputLogin"
                       class="form-control" placeholder="Логин" >
            </div>
        </div>
        <div class="form-group">
            <label for="inputFirstName" class="col-sm-2 control-label">Имя</label>
            <div class="col-sm-10">
                <input type="text" name="first_name" value="<?php if(!$success)  echo set_value('first_name'); ?>" id="inputFirstName"
                       class="form-control" placeholder="Имя" onkeyup="setLoginName();">
            </div>
        </div>


        <div class="form-group">
            <label for="inputLastName" class="col-sm-2 control-label">Фамилия</label>
            <div class="col-sm-10">
                <input type="text" name="last_name" value="<?php if(!$success)  echo set_value('last_name'); ?>" id="inputLastName"
                       class="form-control" placeholder="Фамилия" onkeyup="setLoginName();" >
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="col-sm-2 control-label">Пароль</label>
            <div class="col-sm-10">
                <input type="text" name="password" value="<?php if(!$success)  echo set_value('password'); ?>" id="inputPassword"
                       class="form-control" placeholder="12345"  >
            </div>
        </div>

        <div class="form-group">
            <label for="input4" class="col-sm-2 control-label">Телефон</label>

            <div class="col-sm-10">
                <input type="text" name="phone" class="form-control" value="<?php  if(!$success)  echo set_value('phone'); ?>"
                       id="input4" placeholder="Телефон">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="col-sm-2 control-label">Email</label>

            <div class="col-sm-10">
                <input type="text" name="email" class="form-control" value="<?php  if(!$success)  echo set_value('email'); ?>"
                       id="inputEmail" placeholder="Email">
            </div>
        </div>

        <div class="form-group">
            <label for="input5" class="col-sm-2 control-label">Автомобиль</label>
            <div class="col-sm-10">
                <input type="text" name="auto_name" class="form-control" value="<?php  if(!$success)  echo set_value('auto_name'); ?>"
                       id="input5" placeholder="Пежо бокстер">
            </div>
        </div>
        <div class="form-group">
            <label for="input6" class="col-sm-2 control-label">Госномер</label>
            <div class="col-sm-10">
                <input type="text" name="auto_number" class="form-control" value="<?php  if(!$success)  echo set_value('auto_number'); ?>"
                       id="input6" placeholder="А912 ОК">
            </div>
        </div>
        <div class="form-group">
            <label for="input7" class="col-sm-2 control-label">Примечание</label>
            <div class="col-sm-10">
                <textarea name="note" class="form-control" id="input7" ><?php  if(!$success)  echo set_value('note'); ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">
                <a target="_blank" href="https://yandex.ru/map-constructor/location-tool/">Координаты</a>
            </label>
            <div class="col-sm-5">
                <input type="text" name="latitude" class="form-control" value="<?php  if(!$success)  echo set_value('latitude'); ?>" placeholder="Широта">
            </div>
            <div class="col-sm-5">
                <input type="text" name="longitude" class="form-control" value="<?php  if(!$success)  echo set_value('longitude'); ?>" placeholder="Долгота">
            </div>

        </div>

    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <button type="submit" class="btn btn-info pull-right">Добавить</button>
    </div>
    <!-- /.box-footer -->
</div>

</form>

<script>
    function setLoginName() {
        var login = document.getElementById('inputFirstName').value+
                    document.getElementById('inputLastName').value;
        document.getElementById('inputLogin').value = login;
    }
</script>