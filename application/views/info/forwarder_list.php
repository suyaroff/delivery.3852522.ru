<div class="box">
    <div class="box-body">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <th>Статус</th>
                <th>№</th>
                <th>ФИО</th>
                <th>Автомобиль</th>
                <th>Госномер</th>
                <th>Контакты</th>
                <th>Примечание</th>
                <th>Фото</th>
                <th></th>
            </tr>
            <? foreach ( $users as $user) :?>
                <tr>
                    <td>
                        <? echo $this->config->item($user->activity_status, 'forwarder_status'); ?>
                        <? if($user->activity_status ==2) :?><br><? echo my_date($user->activity_date) ?><? endif?>
                    </td>
                    <td><? echo $user->id; ?></td>
                    <td><? echo $user->first_name; ?> <? echo $user->last_name; ?> </td>
                    <td><? echo $user->auto_name; ?></td>
                    <td><? echo $user->auto_number; ?></td>
                    <td>
                        <? if( $user->phone ): ?>Тел: <? echo $user->phone; ?><br><? endif;?>
                        <? if( $user->email ): ?>Email: <? echo $user->email; ?><br><? endif;?>
                    </td>
                    <td><? echo $user->note; ?></td>
                    <td><img src="/upload/<? echo $user->photo;?>" alt="" style="max-height: 40px;"></td>
                    <td><a class="btn btn-info btn-sm" href="/info/forwarder_edit/<? echo $user->id; ?>/">редактировать</a></td>
                </tr>
            <? endforeach; ?>

            </tbody>
        </table>
    </div>
</div>