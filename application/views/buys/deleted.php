<div class="box box-primary">
	<div class="box-body table-responsive">
	<? if($buys) { ?>
		<table class="table table-bordered table-hover">
			<thead>
			<tr>
				<th>ID</th>
				<th>Номер</th>
				<th>Дата удаления</th>
				<th>Информация</th>
			</tr>
			</thead>
			<tbody>
			<? foreach ($buys as $b) { ?>
				<td><a href="/buys/edit/<? echo $b->id;?>"><? echo $b->id;?></a></td>
				<td><? echo $b->reg_number;?></td>
				<td><? echo $b->trash_date;?></td>
				<td><? echo $b->buy_description;?></td>
			<? } ?>
			</tbody>
		</table>
		<? } else { ?>
		Удаленных заявок нет
		<? } ?>
	</div>
</div>

