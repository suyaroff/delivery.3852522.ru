<form class="from margin-bottom" method="get">
    <div class="input-group">
        <input type="text" name="number" class="form-control" placeholder="номер"
        value="<? echo $this->input->get('number');?>">
        <span class="input-group-btn">
            <button type="submit" class="btn btn-primary flat">Найти</button>
        </span>
    </div>
</form>
<? if(isset($not_found)) { ?> <div class="alert alert-warning"><? echo $not_found;?></div> <? } ?>
<? if(isset($parce_error)) { ?> <div class="alert alert-warning"><? echo $parce_error;?></div> <? } ?>
<? if(isset($parameters)) { foreach ($parameters as $name => $val) { ?>
    <div class=""><b><? echo $name;?>:</b> <? echo  $val;?> </div>
<? }} ?>
