<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab_1" data-toggle="tab">Новые (<? echo count($buys['tab1']); ?>)</a></li>
		<li><a href="#tab_2" data-toggle="tab">Поданные, допущенные (<? echo count($buys['tab2']); ?>)</a></li>
		<li><a href="#tab_3" data-toggle="tab">Выигранные (<? echo count($buys['tab3']); ?>)</a></li>
		<li><a href="#tab_5" data-toggle="tab">Проиграные (<? echo count($buys['tab5']); ?>)</a></li>
		<li><a href="#tab_4" data-toggle="tab">Просроченные (<? echo count($buys['tab4']); ?>)</a></li>

	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1">
			<? foreach ($buys['tab1'] as $b) { ?>
				<div class="box box-solid box-danger" id="boxBuy_<? echo $b->id; ?>">
					<div class="box-header">
						<h3 class="box-title">
							Закупка № <? echo $b->id; ?>
							<? if ($b->bid_date_change || $b->auction_date_change) { ?> <i class="fa fa-calendar"></i> <? } ?>
						</h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" title="Редактировать" href="/buys/edit/<? echo $b->id; ?>">
								<i class="fa fa-wrench"></i>
								<span class="hidden-xs">Редактировать</span></a>
							<button class="btn btn-box-tool" title="Дополнительная информация" onclick="$('#boxBuyInfo_<? echo $b->id; ?>').toggle();">
								<i class="fa  fa-info-circle"></i>
								<span class="hidden-xs">Параметры</span>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-sm-4">
								<b>Регистрационный номер:</b> <? echo $b->reg_number; ?>
								<? if ($b->buy_url) { ?><a href="<? echo $b->buy_url; ?>" target="_blank"><i class="fa fa-external-link"></i></a><? } ?>
								<br>
								<b>Площадка:</b> <a href="<? echo $b->place_url; ?>" target="_blank"><? echo $b->place; ?></a><br>
								<b>Подача до:</b> <?php echo $b->bid_date_end; ?> <? if ($b->bid_date_change) { ?>
									<small class="text-info">(новая дата/время)</small> <? } ?><br>
								<b>Аукцион:</b> <?php echo $b->auction_date; ?> <? if ($b->auction_date_change) { ?>
									<small class="text-info">(новая дата/время)</small> <? } ?><br>
								<b>Цена закупки:</b> <? echo $b->buy_price; ?><br>

							</div>
							<div class="col-sm-4">
								<b>Заказчик:</b>
								<small><? echo $b->customer; ?></small>
								<br>
								<b>Объект закупки:</b> <?php echo $b->buy_description; ?><br>
								<b>Максимальная цена контракта:</b> <? echo $b->max_contract_price; ?><br>
							</div>
							<div class="col-sm-4 <? if ($b->buy_status == 3 && $b->bid_status < 6) { ?>text-danger<? } ?>">
								<b>Дата проверки :</b> <?php echo $b->check_datetime; ?><br>
								<b>Статус заявки:</b> <? echo $this->config->item($b->bid_status, 'bid_status'); ?><br>
								<b>Статус закупки:</b> <? echo $this->config->item($b->buy_status, 'buy_status'); ?><br>
								<b>Ответственный менеджер:</b> <? echo $b->manager_first_name . ' ' . $b->manager_last_name; ?><br>
								<? if ($b->winner) { ?>
									<b>Победитель:</b> <? echo $b->winner_name; ?>. Предложение: <? echo $b->winner_offer; ?>р.
								<? } ?>

							</div>
						</div>
						<div class="row" id="boxBuyInfo_<? echo $b->id; ?>" style="display: none;">
							<hr class="no-margin">
							<div class="col-sm-4">

								<b>Обеспечение заявки:</b> <? echo $b->bid_price; ?><br>
							</div>
							<div class="col-sm-4">
								<b>Кто рассчитал:</b> <? echo $b->calc_user_first_name . ' ' . $b->calc_user_last_name; ?><br>

								<? if ($b->description_1) { ?><b>Примечание 1:</b> <? echo $b->description_1; ?><? } ?>
							</div>
							<div class="col-sm-4">

								<b>Сумма снижения:</b> <? echo $b->down_sum; ?><br>
								<? if ($b->description_2) { ?><b>Примечание 2:</b> <? echo $b->description_2; ?><? } ?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>
		</div>
		<div class="tab-pane" id="tab_2">
			<? foreach ($buys['tab2'] as $b) { ?>
				<div class="box box-solid box-warning" id="boxBuy_<? echo $b->id; ?>">
					<div class="box-header">
						<h3 class="box-title">
							Закупка № <? echo $b->id; ?>
							<? if ($b->bid_date_change || $b->auction_date_change) { ?> <i class="fa fa-calendar"></i> <? } ?>
						</h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" title="Редактировать" href="/buys/edit/<? echo $b->id; ?>">
								<i class="fa fa-wrench"></i>
								<span class="hidden-xs">Редактировать</span></a>
							<button class="btn btn-box-tool" title="Дополнительная информация" onclick="$('#boxBuyInfo_<? echo $b->id; ?>').toggle();">
								<i class="fa  fa-info-circle"></i>
								<span class="hidden-xs">Параметры</span>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-sm-4">
								<b>Регистрационный номер:</b> <? echo $b->reg_number; ?>
								<? if ($b->buy_url) { ?><a href="<? echo $b->buy_url; ?>" target="_blank"><i class="fa fa-external-link"></i></a><? } ?>
								<br>
								<b>Площадка:</b> <a href="<? echo $b->place_url; ?>" target="_blank"><? echo $b->place; ?></a><br>
								<b>Подача до:</b> <?php echo $b->bid_date_end; ?> <? if ($b->bid_date_change) { ?>
									<small class="text-info">(новая дата/время)</small> <? } ?><br>
								<b>Аукцион:</b> <?php echo $b->auction_date; ?> <? if ($b->auction_date_change) { ?>
									<small class="text-info">(новая дата/время)</small> <? } ?><br>
								<b>Цена закупки:</b> <? echo $b->buy_price; ?><br>
							</div>
							<div class="col-sm-4">
								<b>Заказчик:</b>
								<small><? echo $b->customer; ?></small>
								<br>
								<b>Объект закупки:</b> <?php echo $b->buy_description; ?><br>
								<b>Максимальная цена контракта:</b> <? echo $b->max_contract_price; ?><br>
							</div>
							<div class="col-sm-4 <? if ($b->buy_status == 3 && $b->bid_status < 6) { ?>text-danger<? } ?>">
								<b>Дата проверки :</b> <?php echo $b->check_datetime; ?><br>
								<b>Статус заявки:</b> <? echo $this->config->item($b->bid_status, 'bid_status'); ?><br>
								<b>Статус закупки:</b> <? echo $this->config->item($b->buy_status, 'buy_status'); ?><br>
								<b>Ответственный менеджер:</b> <? echo $b->manager_first_name . ' ' . $b->manager_last_name; ?><br>
								<? if ($b->winner) { ?>
									<b>Победитель:</b> <? echo $b->winner_name; ?>. Предложение: <? echo $b->winner_offer; ?>р.
								<? } ?>

							</div>
						</div>
						<div class="row" id="boxBuyInfo_<? echo $b->id; ?>" style="display: none;">
							<hr class="no-margin">
							<div class="col-sm-4">
								<b>Обеспечение заявки:</b> <? echo $b->bid_price; ?><br>
							</div>
							<div class="col-sm-4">
								<b>Кто рассчитал:</b> <? echo $b->calc_user_first_name . ' ' . $b->calc_user_last_name; ?><br>
								<? if ($b->description_1) { ?><b>Примечание 1:</b> <? echo $b->description_1; ?><? } ?>
							</div>
							<div class="col-sm-4">
								<b>Сумма снижения:</b> <? echo $b->down_sum; ?><br>
								<? if ($b->description_2) { ?><b>Примечание 2:</b> <? echo $b->description_2; ?><? } ?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>
		</div>
		<div class="tab-pane" id="tab_3">
			<? foreach ($buys['tab3'] as $b) { ?>
				<div class="box box-solid box-success" id="boxBuy_<? echo $b->id; ?>">
					<div class="box-header">
						<h3 class="box-title">
							Закупка № <? echo $b->id; ?>
							<? if ($b->bid_date_change || $b->auction_date_change) { ?> <i class="fa fa-calendar"></i> <? } ?>
						</h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" title="Редактировать" href="/buys/edit/<? echo $b->id; ?>">
								<i class="fa fa-wrench"></i>
								<span class="hidden-xs">Редактировать</span></a>
							<button class="btn btn-box-tool" title="Дополнительная информация" onclick="$('#boxBuyInfo_<? echo $b->id; ?>').toggle();">
								<i class="fa  fa-info-circle"></i>
								<span class="hidden-xs">Параметры</span>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-sm-4">
								<b>Регистрационный номер:</b> <? echo $b->reg_number; ?>
								<? if ($b->buy_url) { ?><a href="<? echo $b->buy_url; ?>" target="_blank"><i class="fa fa-external-link"></i></a><? } ?>
								<br>
								<b>Площадка:</b> <a href="<? echo $b->place_url; ?>" target="_blank"><? echo $b->place; ?></a><br>
								<b>Подача до:</b> <?php echo $b->bid_date_end; ?> <? if ($b->bid_date_change) { ?>
									<small class="text-info">(новая дата/время)</small> <? } ?><br>
								<b>Аукцион:</b> <?php echo $b->auction_date; ?> <? if ($b->auction_date_change) { ?>
									<small class="text-info">(новая дата/время)</small> <? } ?><br>
								<b>Цена закупки:</b> <? echo $b->buy_price; ?><br>
							</div>
							<div class="col-sm-4">
								<b>Заказчик:</b><small><? echo $b->customer; ?></small><br>
								<b>Объект закупки:</b> <?php echo $b->buy_description; ?><br>
								<b>Максимальная цена контракта:</b> <? echo $b->max_contract_price; ?><br>
							</div>
							<div class="col-sm-4 <? if ($b->buy_status == 3 && $b->bid_status < 6) { ?>text-danger<? } ?>">
								<b>Дата проверки :</b> <?php echo $b->check_datetime; ?><br>
								<b>Статус заявки:</b> <? echo $this->config->item($b->bid_status, 'bid_status'); ?><br>
								<b>Статус закупки:</b> <? echo $this->config->item($b->buy_status, 'buy_status'); ?><br>
								<b>Ответственный менеджер:</b> <? echo $b->manager_first_name . ' ' . $b->manager_last_name; ?><br>
								<? if ($b->winner) { ?>
									<b>Победитель:</b> <? echo $b->winner_name; ?>. Предложение: <? echo $b->winner_offer; ?>р.
								<? } ?>

							</div>
						</div>
						<div class="row" id="boxBuyInfo_<? echo $b->id; ?>" style="display: none;">
							<hr class="no-margin">
							<div class="col-sm-4">
								<b>Обеспечение заявки:</b> <? echo $b->bid_price; ?><br>
							</div>
							<div class="col-sm-4">
								<b>Кто рассчитал:</b> <? echo $b->calc_user_first_name . ' ' . $b->calc_user_last_name; ?><br>
								<? if ($b->description_1) { ?><b>Примечание 1:</b> <? echo $b->description_1; ?><? } ?>
							</div>
							<div class="col-sm-4">
								<b>Сумма снижения:</b> <? echo $b->down_sum; ?><br>
								<? if ($b->description_2) { ?><b>Примечание 2:</b> <? echo $b->description_2; ?><? } ?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>
		</div>
		<div class="tab-pane" id="tab_5">
			<? foreach ($buys['tab5'] as $b) { ?>
				<div class="box box-solid box-default" id="boxBuy_<? echo $b->id; ?>">
					<div class="box-header">
						<h3 class="box-title">
							Закупка № <? echo $b->id; ?>
							<? if ($b->bid_date_change || $b->auction_date_change) { ?> <i class="fa fa-calendar"></i> <? } ?>
						</h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" title="Редактировать" href="/buys/edit/<? echo $b->id; ?>">
								<i class="fa fa-wrench"></i>
								<span class="hidden-xs">Редактировать</span></a>
							<button class="btn btn-box-tool" title="Дополнительная информация" onclick="$('#boxBuyInfo_<? echo $b->id; ?>').toggle();">
								<i class="fa  fa-info-circle"></i>
								<span class="hidden-xs">Параметры</span>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-sm-4">
								<b>Регистрационный номер:</b> <? echo $b->reg_number; ?>
								<? if ($b->buy_url) { ?><a href="<? echo $b->buy_url; ?>" target="_blank"><i class="fa fa-external-link"></i></a><? } ?>
								<br>
								<b>Площадка:</b> <a href="<? echo $b->place_url; ?>" target="_blank"><? echo $b->place; ?></a><br>
								<b>Подача до:</b> <?php echo $b->bid_date_end; ?> <? if ($b->bid_date_change) { ?>
									<small class="text-info">(новая дата/время)</small> <? } ?><br>
								<b>Аукцион:</b> <?php echo $b->auction_date; ?> <? if ($b->auction_date_change) { ?>
									<small class="text-info">(новая дата/время)</small> <? } ?><br>
								<b>Цена закупки:</b> <? echo $b->buy_price; ?><br>
							</div>
							<div class="col-sm-4">
								<b>Заказчик:</b><small><? echo $b->customer; ?></small><br>
								<b>Объект закупки:</b> <?php echo $b->buy_description; ?><br>
								<b>Максимальная цена контракта:</b> <? echo $b->max_contract_price; ?><br>
							</div>
							<div class="col-sm-4 <? if ($b->buy_status == 3 && $b->bid_status < 6) { ?>text-danger<? } ?>">
								<b>Дата проверки :</b> <?php echo $b->check_datetime; ?><br>
								<b>Статус заявки:</b> <? echo $this->config->item($b->bid_status, 'bid_status'); ?><br>
								<b>Статус закупки:</b> <? echo $this->config->item($b->buy_status, 'buy_status'); ?><br>
								<b>Ответственный менеджер:</b> <? echo $b->manager_first_name . ' ' . $b->manager_last_name; ?><br>
								<? if ($b->winner) { ?>
									<b>Победитель:</b> <? echo $b->winner_name; ?>. Предложение: <? echo $b->winner_offer; ?>р.
								<? } ?>

							</div>
						</div>
						<div class="row" id="boxBuyInfo_<? echo $b->id; ?>" style="display: none;">
							<hr class="no-margin">
							<div class="col-sm-4">
								<b>Обеспечение заявки:</b> <? echo $b->bid_price; ?><br>
							</div>
							<div class="col-sm-4">
								<b>Кто рассчитал:</b> <? echo $b->calc_user_first_name . ' ' . $b->calc_user_last_name; ?><br>
								<? if ($b->description_1) { ?><b>Примечание 1:</b> <? echo $b->description_1; ?><? } ?>
							</div>
							<div class="col-sm-4">
								<b>Сумма снижения:</b> <? echo $b->down_sum; ?><br>
								<? if ($b->description_2) { ?><b>Примечание 2:</b> <? echo $b->description_2; ?><? } ?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>
		</div>
		<div class="tab-pane" id="tab_4">
			<? foreach ($buys['tab4'] as $b) { ?>
				<div class="box box-solid box-default" id="boxBuy_<? echo $b->id; ?>">
					<div class="box-header">
						<h3 class="box-title">
							Закупка № <? echo $b->id; ?>
							<? if ($b->bid_date_change || $b->auction_date_change) { ?> <i class="fa fa-calendar"></i> <? } ?>
						</h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" title="Редактировать" href="/buys/edit/<? echo $b->id; ?>">
								<i class="fa fa-wrench"></i>
								<span class="hidden-xs">Редактировать</span></a>
							<button class="btn btn-box-tool" title="Дополнительная информация" onclick="$('#boxBuyInfo_<? echo $b->id; ?>').toggle();">
								<i class="fa  fa-info-circle"></i>
								<span class="hidden-xs">Параметры</span>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-sm-4">
								<b>Регистрационный номер:</b> <? echo $b->reg_number; ?>
								<? if ($b->buy_url) { ?><a href="<? echo $b->buy_url; ?>" target="_blank"><i class="fa fa-external-link"></i></a><? } ?>
								<br>
								<b>Площадка:</b> <a href="<? echo $b->place_url; ?>" target="_blank"><? echo $b->place; ?></a><br>
								<b>Подача до:</b> <?php echo $b->bid_date_end; ?> <? if ($b->bid_date_change) { ?>
									<small class="text-info">(новая дата/время)</small> <? } ?><br>
								<b>Аукцион:</b> <?php echo $b->auction_date; ?> <? if ($b->auction_date_change) { ?>
									<small class="text-info">(новая дата/время)</small> <? } ?><br>
								<b>Цена закупки:</b> <? echo $b->buy_price; ?><br>
							</div>
							<div class="col-sm-4">
								<b>Заказчик:</b><small><? echo $b->customer; ?></small><br>
								<b>Объект закупки:</b> <?php echo $b->buy_description; ?><br>
								<b>Максимальная цена контракта:</b> <? echo $b->max_contract_price; ?><br>
							</div>
							<div class="col-sm-4 <? if ($b->buy_status == 3 && $b->bid_status < 6) { ?>text-danger<? } ?>">
								<b>Дата проверки :</b> <?php echo $b->check_datetime; ?><br>
								<b>Статус заявки:</b> <? echo $this->config->item($b->bid_status, 'bid_status'); ?><br>
								<b>Статус закупки:</b> <? echo $this->config->item($b->buy_status, 'buy_status'); ?><br>
								<b>Ответственный менеджер:</b> <? echo $b->manager_first_name . ' ' . $b->manager_last_name; ?><br>
								<? if ($b->winner) { ?>
									<b>Победитель:</b> <? echo $b->winner_name; ?>. Предложение: <? echo $b->winner_offer; ?>р.
								<? } ?>


							</div>
						</div>
						<div class="row" id="boxBuyInfo_<? echo $b->id; ?>" style="display: none;">
							<hr class="no-margin">
							<div class="col-sm-4">
								<b>Обеспечение заявки:</b> <? echo $b->bid_price; ?><br>
							</div>
							<div class="col-sm-4">
								<b>Кто рассчитал:</b> <? echo $b->calc_user_first_name . ' ' . $b->calc_user_last_name; ?><br>
								<? if ($b->description_1) { ?><b>Примечание 1:</b> <? echo $b->description_1; ?><? } ?>
							</div>
							<div class="col-sm-4">
								<b>Сумма снижения:</b> <? echo $b->down_sum; ?><br>
								<? if ($b->description_2) { ?><b>Примечание 2:</b> <? echo $b->description_2; ?><? } ?>
							</div>
						</div>
					</div>
				</div>
			<? } ?>
		</div>
	</div>
</div>


