<? if($this->input->get('edit')) { ?>
<div class="box box-primary">
	<div class="box-body">
		<?php echo form_open('/buys/partners?edit='.$partner->id, array('enctype' => 'multipart/form-data', 'role' => 'form', 'method' => 'post')); ?>
		<div class="form-group form-group-sm">
			<label>Имя</label>
			<input type="text" class="form-control" name="name" value="<?php echo $partner->name; ?>" required>
		</div>
		<div class="form-group form-group-sm">
			<label>Телефон</label>
			<input type="text" class="form-control" name="phone" value="<?php echo $partner->phone; ?>">
		</div>
		<input type="hidden" name="save" value="<?php echo $partner->id; ?>">
		<button type="submit" class="btn btn-success">Сохранить</button>
		</form>
	</div>
</div>
<? } else { ?>
<div class="row">
	<div class="col-sm-9">
		<div class="box box-primary">
			<div class="box-body">
				<? if($partners) { ?>
				<table class="table">
					<thead>
					<tr>
						<th>ID</th>
						<th>Имя</th>
						<th>Телефон</th>
						<th style="width:50px; "></th>
					</tr>
					</thead>
					<tbody>
					<? foreach ($partners as $p) { ?>
						<tr>
							<td>
								<a title="Редактировать" href="/buys/partners?edit=<? echo $p->id;?>" class="btn btn-primary btn-xs"><? echo $p->id; ?></a>
							</td>
							<td><? echo $p->name; ?></td>
							<td><? echo $p->phone; ?></td>
							<td>
								<a title="Удалить" href="/buys/partners?delete=<? echo $p->id;?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
					<? } ?>
					</tbody>
				</table>
				<? } ?>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Добавить</h3>
			</div>
			<div class="box-body">
				<?php echo form_open('', array('enctype' => 'multipart/form-data', 'role' => 'form', 'method' => 'post')); ?>
				<div class="form-group form-group-sm">
					<label>Имя</label>
					<input type="text" class="form-control" name="name" value="<?php echo set_value('name'); ?>" required>
				</div>
				<div class="form-group form-group-sm">
					<label>Телефон</label>
					<input type="text" class="form-control" name="phone" value="<?php echo set_value('phone'); ?>">
				</div>
				<input type="hidden" name="add" value="1">
				<button type="submit" class="btn btn-success">Добавить</button>
				</form>
			</div>
		</div>
	</div>
</div>
<? } ?>



