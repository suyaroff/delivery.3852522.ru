
<div class="box box-primary">
	<div class="box-body table-responsive">
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php endif; ?>
		<?php if (validation_errors()): ?>
			<div class="alert alert-danger">
				<?php echo validation_errors(); ?>
			</div>
		<?php endif; ?>
		<?php echo form_open('', array('enctype' => 'multipart/form-data', 'role' => 'form', 'method' => 'post')); ?>
		<table class="table">
			<tr>
				<td><b>Регистрационный номер:</b></td>
				<td><?php echo $buy->reg_number; ?> <a target="_blank" href="<?php echo $buy->buy_url; ?>"><i class="fa fa-external-link"></i></a></td>
			</tr>
			<tr>
				<td><b>Наименование объекта закупки:</b></td>
				<td><? echo $buy->buy_description; ?></td>
			</tr>
			<tr>
				<td><b>Площадка:</b></td>
				<td><a target="_blank" href="<?php echo $buy->place_url; ?>"><?php echo $buy->place; ?></a></td>
			</tr>
			<tr>
				<td><b>Заказчик:</b></td>
				<td><? echo $buy->customer; ?></td>
			</tr>
			<tr>
				<td><b>Дата и время окончания рассмотрения заявки:</b></td>
				<td><? echo $buy->bid_date_end; ?></td>
			</tr>
			<tr>
				<td><b>Дата и время проведения аукциона:</b></td>
				<td><? echo $buy->auction_date; ?></td>
			</tr>
			<tr>
				<td><b>Начальная цена контракта:</b></td>
				<td><? echo $buy->max_contract_price; ?></td>
			</tr>
			<tr>
				<td><b>Обеспечение заявки:</b></td>
				<td><? echo $buy->bid_price; ?></td>
			</tr>
			<tr>
				<td><b>Статус процедуры:</b></td>
				<td><? echo $this->config->item($buy->buy_status, 'buy_status'); ?></td>
			</tr>
			<tr>
				<td><b>Статус заявки:</b></td>
				<td><? echo $this->config->item($buy->bid_status, 'bid_status'); ?></td>
			</tr>
			<tr>
				<td><b>Цена закупки:</b></td>
				<td>
					<input type="text" name="buy_price" class="form-control" value="<? echo $buy->buy_price; ?>" required>
				</td>
			</tr>
			<tr>
				<td><b>Сумма снижения:</b></td>
				<td><? echo $buy->down_sum; ?></td>
			</tr>
			<tr>
				<td><b>Кто рассчитал:</b></td>
				<td><? echo $buy->calc_user_first_name.' '.$buy->calc_user_last_name; ?></td>
			</tr>
			<tr>
				<td><b>Примечание 1:</b></td>
				<td>
					<textarea name="description_1" class="form-control"><? echo $buy->description_1; ?></textarea>
				</td>
			</tr>
			<tr>
				<td><b>Ответственный менеджер:</b></td>
				<td><? echo $buy->manager_first_name.' '.$buy->manager_last_name; ?></td>
			</tr>
			<tr>
				<td><b>Примечание 2:</b></td>
				<td><? echo $buy->description_2; ?></td>
			</tr>
			<tr>
				<td colspan="2"><b>Информация об объекте закупки:</b></td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="table-borders"><? echo $buy->items; ?></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button type="submit" class="btn btn-primary">Сохранить</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
