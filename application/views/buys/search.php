<div class="box box-solid box-primary">
	<div class="box-body">
		<form action="/buys/search" class="margin-bottom">
			<div class="input-group input-group">
				<input type="text" class="form-control" name="str" value="<? if (isset($_GET['str'])) echo $_GET['str']; ?>">
				<span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat">найти</button>
				</span>
			</div>
		</form>
		<? if ($str) { ?>
			<? if (count($finds)) { ?>
				<table class="table table-hover table-bordered">
					<thead>
					<tr>
						<th>ID</th>
						<th>Регистрационный номер</th>
						<th>Площадка</th>
						<th>Заказчик</th>
						<th>Объект закупки</th>
						<th style="width: 90px;">Подача до</th>
						<th style="width: 90px;">Аукцион</th>
						<th style="width: 110px;">Статус заявки</th>
						<th style="width: 110px;">Статус закупки</th>
					</tr>
					</thead>
					<tbody>
					<? foreach ($finds as $f) { ?>
						<tr>
							<td><a href="/buys/edit/<? echo $f['id']; ?>" target="_blank"><? echo $f['id']; ?></a></td>
							<td><? echo $f['reg_number']; ?> <a href="<? echo $f['buy_url']; ?>" target="_blank"><i class="fa fa-external-link"></i></a></td>
							<td><? echo $f['place']; ?> <a href="<? echo $f['place_url']; ?>" target="_blank"><i class="fa fa-external-link"></i></a></td>

							<td><? echo $f['customer']; ?></td>
							<td><? echo $f['buy_description']; ?></td>
							<td><? echo $f['bid_date_end']; ?></td>
							<td><? echo $f['auction_date']; ?></td>
							<td><? echo $this->config->item($f['bid_status'], 'bid_status'); ?></td>
							<td><? echo $this->config->item($f['buy_status'], 'buy_status'); ?></td>
						</tr>
					<? } ?>
					</tbody>
				</table>
			<? } else { ?>
				<div class="alert alert-info">Ничего не найдено</div>
			<? } ?>
		<? } ?>
	</div>
</div>
