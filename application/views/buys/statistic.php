<div style="width:75%;">
  <canvas id="canvas"></canvas>
</div>
<script>
  var bid_s = ['<? echo implode("', '", $this->config->item('bid_status'));?>'];
  var bid_c = [
    'rgba(193,203,191,0.5)',
    'rgba(112,235,87,0.5)',
    'rgba(231,235,65,0.5)',
    'rgba(255,171,41,0.5)',
    'rgba(0,255,25,0.5)',
    'rgba(255,13,154,0.5)',
    'rgba(255,18,3,0.5)',
    'rgba(32,0,255,0.5)'
  ];

  var labs = ['<? echo implode("', '", array_keys($statistic[0]));?>'];

  var config = {
    type: 'line',
    data: {
      labels:labs,
      datasets: [
        <? foreach ($statistic as $status =>  $data) { ?>
        {
          label: '<? echo $this->config->item($status, 'bid_status');?>' ,
          backgroundColor:bid_c[<? echo $status;?>],
          borderColor:bid_c[<? echo $status;?>],
          data: [
            <? foreach ($data as $v) echo $v.',' ?>
          ],
          fill: false,
        },
        <? } ?>

      ]
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'Статистика по закупкам '
      },
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Месяц'
          }
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Количество'
          }
        }]
      }
    }
  };

  window.onload = function () {
    var ctx = document.getElementById('canvas').getContext('2d');
    window.myLine = new Chart(ctx, config);
  };
</script>