<div class="box box-primary">
	<div class="box-header">Информация о закупке</div>
	<div class="box-body">
		<table class="table">
			<tr>
				<td><b>Регистрационный номер:</b></td>
				<td><?php echo $buy->reg_number; ?> <a target="_blank" href="<?php echo $buy->buy_url; ?>"><i class="fa fa-external-link"></i></a></td>
			</tr>
			<tr>
				<td><b>Наименование объекта закупки:</b></td>
				<td><? echo $buy->buy_description; ?></td>
			</tr>
			<tr>
				<td><b>Заказчик:</b></td>
				<td><? echo $buy->customer; ?></td>
			</tr>
			<tr>
				<td><b>Статус процедуры:</b></td>
				<td><? echo $this->config->item($buy->buy_status, 'buy_status'); ?></td>
			</tr>
			<tr>
				<td><b>Статус заявки:</b></td>
				<td><? echo $this->config->item($buy->bid_status, 'bid_status'); ?></td>
			</tr>
				<td colspan="2">
					<a class="btn btn-danger btn-block" href="/buys/delete/<? echo $buy->id;?>/yes">Удалить</a>
				</td>
			</tr>
		</table>
	</div>
</div>
