<div class="box box-primary">
	<div class="box-body">
		<?php if (validation_errors()): ?>
			<div class="alert alert-danger">
				<?php echo validation_errors(); ?>
			</div>
		<?php endif; ?>
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php endif; ?>
		<?php if (isset($warning) && $warning) : ?>
			<div class="alert alert-warning">
				<?php echo $warning; ?>
			</div>
		<?php endif; ?>

		<?php echo form_open('', array('enctype' => 'multipart/form-data', 'role' => 'form', 'id' => 'buyAddForm', 'method' => 'post')); ?>
		<div class="form-group form-group-sm" id="regNumberBox">
			<label>Регистрационный номер</label>
			<input type="text" class="form-control"
				   name="reg_number"
				   id="reg_number"
				   value="<?php  echo $buy->reg_number; ?>"
				   placeholder="0162100007717000161">
		</div>
		<div class="form-group form-group-sm">
			<label>Наименование объекта закупки</label>
			<textarea class="form-control" rows="1"
					  name="buy_description"
					  placeholder="Поставка хозяйственных материалов  ( в том числе бытовая химия)"
			><?php  echo $buy->buy_description; ?></textarea>
		</div>
		<div class="form-group form-group-sm">
			<label>Ссылка на процедуру</label>
			<div class="input-group">
				<input type="text" class="form-control"
					   name="buy_url"
					   value="<?php  echo $buy->buy_url; ?>"
					   placeholder="http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=0162100007717000161">
				<span class="input-group-addon" id="buy_url_external">
					<a target="_blank" href="<?php  echo $buy->buy_url; ?>"><i class="fa fa-external-link"></i></a>
				</span>
			</div>
		</div>
		<div class="form-group form-group-sm">
			<label>Площадка</label>
			<div class="row">
				<div class="col-sm-6">
					<textarea class="form-control"
							  placeholder="ЗАО «Сбербанк-АСТ»"
							  name="place"
							  rows="1"
					><?php  echo $buy->place; ?></textarea>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control"
						   placeholder="http://www.sberbank-ast.ru"
						   name="place_url"
						   value="<?php  echo $buy->place_url; ?>">
				</div>
			</div>
		</div>
		<div class="form-group form-group-sm">
			<label>От кого участвуем</label>
			<select name="partner_id" class="form-control">
				<option value="0">Не указан</option>
				<?php foreach ($partners as $p): ?>
					<option value="<? echo $p->id ; ?>" <? if($p->id == $buy->partner_id) echo 'selected'; ?> ><? echo $p->name ;?></option>
				<? endforeach; ?>
			</select>
		</div>
		<div class="form-group form-group-sm">
			<label>Заказчик</label>
			<textarea class="form-control"
					  name="customer"
					  rows="1"
					  placeholder="УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ СЛУЖБЫ СУДЕБНЫХ ПРИСТАВОВ ПО СВЕРДЛОВСКОЙ ОБЛАСТИ"
			><?php  echo $buy->customer; ?></textarea>
		</div>
		<div class="form-group form-group-sm">
			<div class="row">
				<div class="col-sm-6">
					<label>Дата и время окончания рассмотрения заявки</label>
					<input type="text"
						   class="form-control datetime-picker"
						   placeholder="2017-08-16 10:00"
						   name="bid_date_end"
						   value="<?php  echo $buy->bid_date_end ; ?>" >
				</div>

				<div class="col-sm-6">
					<label>Дата и время проведения аукциона</label>
					<input type="text "
						   class="form-control datetime-picker"
						   placeholder="2018-08-16 12:30"
						   name="auction_date"
						   value="<?php  echo $buy->auction_date ; ?>" >

				</div>
			</div>
		</div>
		<div class="form-group form-group-sm">
			<div class="row">
				<div class="col-sm-6">
					<label>Начальная цена контракта</label>
					<input type="text" class="form-control"
						   name="max_contract_price"
						   value="<?php  echo $buy->max_contract_price; ?>"
						   placeholder="156863.15">
				</div>
				<div class="col-sm-6">
					<label>Обеспечение заявки</label>
					<input type="text" class="form-control"
						   name="bid_price"
						   value="<?php  echo $buy->bid_price; ?>"
						   placeholder="1568.63">
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<div class="row">
				<div class="col-sm-6">
					<label>Статус процедуры</label>
					<select name="buy_status" class="form-control">
						<option value="0">Не указан</option>
						<?php foreach ($this->config->item('buy_status') as $status_id => $status): ?>
							<option value="<? echo $status_id ; ?>" <? if($status_id == $buy->buy_status) echo 'selected'; ?> ><? echo $status ;?></option>
						<? endforeach; ?>
					</select>
					<? if($buy->winner) { ?>
						<small><b>Победитель:</b> <? echo $buy->winner_name;?>.
							Предложение: <? echo $buy->winner_offer;?>р. </small>
					<? } ?>
				</div>
				<div class="col-sm-6">
					<label>Статус заявки</label>
					<select name="bid_status" class="form-control">
						<?php foreach ($this->config->item('bid_status') as $status_id => $status): ?>
							<? if($buy->buy_status == 3 && $status_id > 5 ) { ?>
							<option value="<? echo $status_id ; ?>" <? if($status_id == $buy->bid_status) echo 'selected'; ?> ><? echo $status ;?></option>
							<? } elseif($buy->buy_status != 3 && $status_id < 6 ) { ?>
							<option value="<? echo $status_id ; ?>" <? if($status_id == $buy->bid_status) echo 'selected'; ?> ><? echo $status ;?></option>
							<? } ?>
						<? endforeach; ?>
					</select>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<div class="row">
				<div class="col-sm-6">
					<label>Цена закупки</label>
					<input type="text" class="form-control"
						   name="buy_price"
						   value="<?php  echo $buy->buy_price; ?>"
						   placeholder="100000.90">
				</div>
				<div class="col-sm-6">
					<label>Сумма снижения</label>
					<input type="text" class="form-control"
						   name="down_sum"
						   value="<?php  echo $buy->down_sum; ?>"
						   placeholder="100000.50">
				</div>
			</div>
		</div>
		<div class="form-group form-group-sm">
			<div class="row">
				<div class="col-sm-6">
					<label>Кто рассчитал</label>
					<? /*
					<select name="calculate_user" class="form-control">
						<option value="0">Не указан</option>
						<?php foreach ($users as $u): ?>
							<option value="<? echo $u->id ; ?>" <? if($u->id == $buy->calculate_user) echo 'selected'; ?> >
								<? echo $u->first_name.' '.$u->last_name ;?> (<? echo $this->config->item($u->role, 'roles');?>)
							</option>
						<? endforeach; ?>

					</select>
 					*/ ?>
					<div><? echo $buy->calc_user_first_name.' '.$buy->calc_user_last_name;?></div>
				</div>
				<div class="col-sm-6">
					<label>Ответственный менеджер</label>
					<select name="manager" class="form-control">
						<option value="0">Не указан</option>
						<?php foreach ($users as $u): ?>
							<option value="<? echo $u->id ; ?>" <? if($u->id == $buy->manager) echo 'selected'; ?> >
								<? echo $u->first_name.' '.$u->last_name ;?> (<? echo $this->config->item($u->role, 'roles');?>)
							</option>
						<? endforeach; ?>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group form-group-sm">
			<div class="row">
				<div class="col-sm-6">
					<label>Примечание 1</label>
					<textarea name="description_1" class="form-control"><?php  echo $buy->description_1; ?></textarea>
				</div>
				<div class="col-sm-6">
					<label>Примечание 2</label>
					<textarea name="description_2" class="form-control"><?php  echo $buy->description_2; ?></textarea>
				</div>
			</div>
		</div>

	</div>
	<div class="box-footer">
		<a class="btn btn-danger" href="/buys/delete/<? echo $buy->id;?>">Удалить</a>
		<input type="hidden" name="save_buy" value="<? echo $buy->id;?>">
		<button type="submit" class="btn btn-info pull-right">Сохранить</button>
	</div>
	</form>
</div>



<? if($participants) { ?>
	<?php echo form_open('', array('role' => 'form', 'method' => 'post')); ?>
	<div class="box box-success" id="buy_participant">
		<div class="box-header">
			<h3 class="box-title">Список участников</h3>
		</div>
		<div class="box-body">

			<table class="table">
				<thead>
				<tr>
					<th>Наименование</th>
					<th style="width: 80px;">Предложение</th>
					<th style="width: 50px;">Победитель</th>
					<th style="width: 30px;"></th>
				</tr>
				</thead>
				<tbody>
				<? foreach ($participants as $p) { ?>
				<tr>
					<td>
						<textarea name="name[<? echo $p->id;?>]" class="form-control input-sm" rows="1"><? echo $p->name;?></textarea>
					</td>
					<td>
						<input type="text" name="offer[<? echo $p->id;?>]" class="form-control input-sm" value="<? echo $p->offer;?>"/>
					</td>
					<td>
						<input type="radio" name="winner" value="<? echo $p->id;?>" <? if ($buy->winner == $p->id) echo 'checked';?>
					</td>
					<td>
						<a class="btn btn-xs btn-danger" href="/buys/edit/<? echo $buy->id;?>?delete_bp=<? echo $p->id; ?>" title="Удалить"><i class="fa fa-remove"></i></a>
					</td>

				</tr>
				<? } ?>
				</tbody>
			</table>

		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-success" onclick="add_buy_participant_row();">Добавить</button>
			<input type="hidden" name="save_participants" value="1"/>
			<button type="submit" class="btn btn-info pull-right">Сохранить список</button>
		</div>
	</div>
	</form>
<? } ?>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Лог изменений</h3>
	</div>
	<div class="box-body table-responsive no-padding">
		<table class="table table-hover">
			<thead>
			<tr>
				<th style="width: 140px;">Время</th>
				<th>Пользователь</th>
				<th>Данные формы</th>
			</tr>
			</thead>
			<tbody>
			<? foreach ($buy_log as $bl) {?>
			<tr>
				<td><? echo $bl->log_time;?></td>
				<td><? echo $bl->first_name.'&nbsp;'.$bl->last_name;?></td>
				<td><? echo $bl->dif;?></td>
			</tr>
			<? } ?>
			</tbody>
		</table>
	</div>
</div>
