<? foreach ($buys as $b){ ?>
	<div class="box box-solid box-primary" id="boxBuy_<? echo $b->id;?>">
		<div class="box-header">
			<h3 class="box-title">
				Закупка № <? echo $b->id;?>

			</h3>
			<div class="box-tools pull-right">
				<button class="btn btn-box-tool"
						title="Дополнительная информация"
						onclick="$('#boxBuyInfo_<? echo $b->id;?>').toggle();"
				><i class="fa  fa-info-circle"></i> Параметры</button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-sm-4">
					<b>Регистрационный номер:</b> <? echo $b->reg_number;?>
					<a href="<? echo $b->buy_url;?>" target="_blank"><i class="fa fa-external-link"></i></a><br>
					<b>Площадка:</b> <a href="<? echo $b->place_url;?>" target="_blank"><? echo $b->place;?></a><br>
					<b>Подача до:</b> <?php echo $b->bid_date_end;?>
					<? $bidtime = ceil((strtotime($b->bid_date_end)-time())/3600);
					if($bidtime > 0 && $bidtime < 25){
						echo '<span class="text-danger">Осталось: '.$bidtime.' ч.</span>';
					}

					?>
					<br>
					<b>Аукцион:</b> <?php echo $b->auction_date;?>
				</div>
				<div class="col-sm-4">
					<b>Заказчик:</b> <small><? echo $b->customer;?></small><br>
					<b>Объект закупки:</b> <?php echo $b->buy_description;?>
				</div>
				<div class="col-sm-4">
					<b>Статус закупки:</b> <? echo $this->config->item( $b->buy_status, 'buy_status');?><br>
					<b>Статус заявки:</b> <? echo $this->config->item( $b->bid_status, 'bid_status');?><br>
				</div>
			</div>
			<div class="row" id="boxBuyInfo_<? echo $b->id;?>" style="display: none;">
				<hr class="">
				<div class="col-sm-4">
					<b>Максимальная цена контракта:</b> <? echo $b->max_contract_price;?><br>
					<b>Обеспечение заявки:</b> <? echo $b->bid_price;?><br>
				</div>
				<div class="col-sm-4">
					<b>Кто рассчитал:</b><? if($b->calculate_user) { echo $b->calc_user_first_name.' '.$b->calc_user_last_name; } else {?>
					<a href="/buys/calculate/<? echo $b->id;?>" class="btn btn-success btn-xs">Я сделаю это</a>
					<? } ?><br>
					<? if($b->buy_price > 0) { ?><b>Цена закупки:</b> <? echo $b->buy_price;?><br><? } ?>
					<? if($b->description_1) { ?><b>Примечание 1:</b> <? echo $b->description_1;?><? } ?>
				</div>
				<div class="col-sm-4">
					<b>Ответственный менеджер:</b> <? echo $b->manager_first_name.' '.$b->manager_last_name;?><br>
					<? if($b->down_sum > 0) { ?><b>Сумма снижения:</b> <? echo $b->down_sum;?><br> <? } ?>
					<? if($b->description_2) { ?><b>Примечание 2:</b> <? echo $b->description_2;?><? } ?>
				</div>
			</div>
		</div>
	</div>

<? } ?>

