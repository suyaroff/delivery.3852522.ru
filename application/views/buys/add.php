<div class="box box-info">
	<div class="box-body">
		<?php if (validation_errors()): ?>
			<div class="alert alert-danger">
				<?php echo validation_errors(); ?>
			</div>
		<?php endif; ?>
		<?php if ($this->session->flashdata('success')) : ?>
			<div class="alert alert-success">
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php endif; ?>
		<?php echo form_open('', array('enctype' => 'multipart/form-data', 'role' => 'form', 'id' => 'buyAddForm')); ?>
		<div class="form-group  form-group-sm" id="regNumberBox">
			<label>
				Регистрационный номер
				<a href="http://zakupki.gov.ru/epz/order/quicksearch/search.html" target="_blank">
					<i class="fa fa-external-link"></i> Гос. закупки</a>
			</label>
			<div class="input-group">
				<input type="text" class="form-control" name="reg_number" id="reg_number" value="<?php  echo set_value('reg_number'); ?>"
				   placeholder="">
				<span class="input-group-btn  ">
					<button type="button" class="btn btn-info btn-flat btn-sm" id="checkBuyButton" onclick="check_buy()" >Проверить на zakupki.gov.ru</button>
				</span>
			</div>
			<span class="help-block"></span>
		</div>
		<div class="form-group  form-group-sm">
			<label>Наименование объекта закупки</label>
			<textarea class="form-control"
					  name="buy_description"
					  rows="1"
					  placeholder=""
			><?php  echo set_value('buy_description'); ?></textarea>
		</div>
		<div class="form-group  form-group-sm">
			<label>Ссылка на процедуру</label>
			<div class="input-group">
				<input type="text" class="form-control"  name="buy_url" value="<?php  echo set_value('buy_url'); ?>"
				   placeholder="">
				<span class="input-group-addon" id="buy_url_external"><i class="fa fa-external-link"></i></span>
			</div>
		</div>
		<div class="form-group form-group-sm">
			<label>Площадка</label>
			<div class="row">
				<div class="col-sm-6">
					<textarea class="form-control"
							  placeholder=""
							  name="place"
							  rows="1"
					><?php  echo set_value('place'); ?></textarea>
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" placeholder="" name="place_url"
						   value="<?php  echo set_value('place_url'); ?>" >
				</div>
			</div>
		</div>
		<div class="form-group form-group-sm">
			<label>Заказчик</label>
			<textarea class="form-control"
					  name="customer"
					  rows="1"
					  placeholder=""
			><?php  echo set_value('customer'); ?></textarea>
		</div>
		<div class="form-group form-group-sm">
			<div class="row">
				<div class="col-sm-6">
					<label >Дата и время окончания рассмотрения заявки</label>
					<input type="text"
						   class="form-control datetime-picker"
						   placeholder=""
						   name="bid_date_end"
						   value="<?php  echo set_value('bid_date_end'); ?>" >
				</div>
				<div class="col-sm-6">
					<label >Дата и время проведения аукциона</label>
					<input type="text"
						   class="form-control datetime-picker"
						   placeholder=""
						   name="auction_date"
						   value="<?php  echo set_value('auction_date'); ?>" >
				</div>
			</div>
		</div>
		<div class="form-group form-group-sm">
			<label>Статус процедуры</label>
			<select name="buy_status" class="form-control">
			<?php foreach ($this->config->item('buy_status') as $status_id => $status): ?>
				<option value="<? echo $status_id ; ?>" <? echo set_select('buy_status', $status_id); ?> ><? echo $status ;?></option>
			<? endforeach; ?>
			</select>
		</div>
		<div class="form-group form-group-sm">
			<div class="row">
				<div class="col-sm-6">
					<label>Начальная цена контракта</label>
					<input type="text" class="form-control"
						   name="max_contract_price"
						   value="<?php  echo set_value('max_contract_price'); ?>"
						   placeholder="" >
				</div>
				<div class="col-sm-6">
					<label>Обеспечение заявки</label>
					<input type="text" class="form-control"
						   name="bid_price"
						   value="<?php  echo set_value('bid_price'); ?>"
						   placeholder="" >
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<input type="hidden" name="items" value="">
		<button type="submit" class="btn btn-info pull-right">Добавить</button>
	</div>
	</form>
</div>
