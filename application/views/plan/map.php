<style>
    #map {
        width: 100%; height: 800px; padding: 0; margin: 0;
    }
</style>
<div id="map"></div>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>
    <? if(isset($_GET['points'])):?>
    <?php $points =  $_GET['points']; ?>
    var points = <? echo $points ;?>;
    points.push(points[0]);
    function init_route () {
        /**
         * Создаем мультимаршрут.
         * Первым аргументом передаем модель либо объект описания модели.
         * Вторым аргументом передаем опции отображения мультимаршрута.
         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRoute.xml
         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRouteModel.xml
         */
        var multiRoute = new ymaps.multiRouter.MultiRoute({
            // Описание опорных точек мультимаршрута.
            referencePoints: points,
            // Параметры маршрутизации.
            params: {
                // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                results: 2
            }
        }, {
            // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
            boundsAutoApply: true
        });



        // Создаем карту с добавленными на нее кнопками.
        var myMap = new ymaps.Map('map', {
            center: points[0],
            zoom: 13,

        }, {
            buttonMaxWidth: 300
        });

        // Добавляем мультимаршрут на карту.
        myMap.geoObjects.add(multiRoute);
    }

    ymaps.ready(init_route);
    <? endif;?>

    <? if(isset($_GET['point'])): ?>
    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init(){
        myMap = new ymaps.Map("map", {
            center: <? echo $_GET['point'];?>,
            zoom: 17
        });

        myPlacemark = new ymaps.Placemark( <? echo $_GET['point'];?>, {
            hintContent: 'Метка',
            balloonContent: 'Метка'
        });

        myMap.geoObjects.add(myPlacemark);
    }
    <? endif;?>
</script>


