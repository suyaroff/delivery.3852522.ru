<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><? echo $title; ?></title>
	<style>
		body {
			margin: 0;
			padding: 5px;
			font-size: 1em%;
		}

		.table {
			width: 100%;
			margin: 0 auto;
			border-collapse: collapse;
		}

		.table td, .table th {
			border: 1px solid #666666;
			padding: 2px;
		}

		.table th {
			width: 7%;
		}

		h1 {
			margin: 0 0 10px 0;
		}
	</style>
</head>
<body>
<h1><? echo $title; ?></h1>

<table class="table  order-table">
	<thead>
	<tr>
		<th style="width:5%">Инициатор</th>
		<th style="width:5%;">Номер / статус</th>
		<th style="width: 3%;">Дата <br> доставки</th>
		<th>Экспедитор</th>
		<th style="width: 1%">Порядок</th>
		<th style="width: 2%">Тип операции</th>
		<th style="width: 3%">Накладные</th>
		<th>Контрагент</th>
		<th>Адрес</th>
		<th>Контакты</th>
		<th>Примечания</th>
		<th style="width: 3%;">Вес / Мест</th>
	</tr>
	</thead>
	<tbody>
	<? foreach ($plan as $forwarder_id => $p) {
		if ($_GET['print'] == 'all' || $forwarder_id == $_GET['print']) { ?>
			<tr>
				<td colspan="12">
					Маршрутный лист <? echo $p['name']; ?> <? echo $p['auto_name']; ?> <? echo $p['auto_number']; ?>
				</td>
			</tr>
			<? foreach ($p['orders'] as $order) { ?>

				<tr id="order_row_<? echo $order['id']; ?>">
					<td><? echo $order['author_first_name'] . ' ' . $order['author_last_name']; ?></td>
					<td><? echo $order['id']; ?> <br> <? echo $this->config->item($order['status'], 'orders_status'); ?></td>
					<td><? if ($order['delivery_date'] > 0) echo my_date($order['delivery_date']); ?></td>
					<td><? echo $order['forwarder_fn'] . ' ' . $order['forwarder_ln']; ?></td>
					<td style="text-align: center;"><? echo $order['ord']; ?></td>
					<td><? echo $this->config->item($order['operation'], 'orders_operation'); ?> </td>
					<td><? echo $order['invoice']; ?></td>
					<td><? echo $order['contractor_name']; ?></td>
					<td><? echo $order['contractor_address']; ?></td>
					<td><? echo $order['contractor_contact']; ?><? echo $order['contact_info']; ?></td>
					<td>
						<? echo $order['note']; ?>
						<? if ($order['note_stock']) { ?>
							<br><b>Примечение склада:</b> <? echo $order['note_stock']; ?>
						<? } ?>
					</td>
					<td><? echo $order['weight'] . ' кг. <br>' . $order['place']; ?> шт.</td>
				</tr>
			<? } ?>

		<? }
	} ?>
	</tbody>
</table>

<script>
	window.print();
</script>

</body>
</html>
