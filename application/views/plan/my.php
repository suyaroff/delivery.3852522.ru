<? foreach($orders as $order): ?>
    <div class="row" style="padding: 0 0 10px 0;">
        <div class="col-md-1">№: <? echo $order['id']; ?></div>
        <div class="col-md-1">Тип операции: <? echo $this->config->item($order['operation'], 'orders_operation'); ?></div>
        <div class="col-md-1">Контрагент: <? echo $order['contractor_name']; ?></div>
        <div class="col-md-1">Контакты: <? echo $order['contractor_contact']; ?></div>
        <div class="col-md-2">Адрес: <? echo $order['contractor_address']; ?></div>
        <div class="col-md-2">Примечания контакта: <? echo $order['contact_info'];?></div>
        <div class="col-md-2">Примечание: <? echo $order['note'];?></div>
        <div class="col-md-2"><a class="btn btn-primary" href="/plan/order/<? echo $order['id']; ?>">Подробнее</a></div>
    </div>
<? endforeach; ?>
