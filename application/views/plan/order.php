<p><b>№:</b> <? echo $order['id']; ?></p>
<p><b>Статус:</b> <? echo $this->config->item($order['status'], 'orders_status'); ?></p>
<p><b>Тип операции:</b> <? echo $this->config->item($order['operation'], 'orders_operation'); ?></p>
<p><b>Контрагент:</b> <? echo $order['contractor_name']; ?></p>
<p><b>Контакты:</b> <? echo $order['contractor_contact']; ?></p>
<p><b>Адрес:</b> <? echo $order['contractor_address']; ?></p>
<p><b>Примечания контакта:</b> <? echo $order['contact_info']; ?></p>
<p><b>Накладная:</b> <? echo $order['invoice']; ?></p>
<p><b>Примечание:</b> <? echo $order['note']; ?></p>
<p><b>Вес:</b> <? echo $order['weight']; ?></p>
<? if($order['contractor_latitude']):?>
<p>
    <a target="_blank" href="/plan/map?point=[<? echo $order['contractor_latitude'];?>,<? echo $order['contractor_longitude'];?>]">Карта </a>
</p>
<? endif;?>

<div class="row">
    <div class="col-sm-6 form-group"><a class="btn btn-success btn-block" href="/plan/order/<? echo $order['id']; ?>?status=4">Готово</a></div>
    <div class="col-sm-6 form-group"><a class="btn btn-danger btn-block" href="/plan/order/<? echo $order['id']; ?>?status=2">Возникла проблема</a></div>
</div>

