<div class="box">
    <div class="box-header  with-border">
        <h2 class="box-title">
            <button class="btn btn-primary" id="plan_datepicker_button">Выбрать дату</button>
            <a class="btn btn-info" href="<? echo  current_url(); ?>?print=all" target="_blank"><i class="fa fa-print"></i> печать</a>
        </h2>
    </div>
    <div class="box-body table-responsive">
        <table class="table order-table table-bordered table-hover ">
            <thead>
            <tr>
				<th>Инициатор</th>
				<th style="width: 65px">Номер</th>
				<th style="width:80px;">Дата <br> доставки</th>
                <th>Экспедитор</th>
                <th style="width: 80px">Порядок</th>
                <th>Тип операции</th>
                <th style="max-width: 200px;">Накладные</th>
                <th>Контрагент</th>
                <th>Контакты</th>
                <th>Адрес</th>
                <th>Примечания контакта</th>
                <th>Примечания</th>
                <th style="width: 100px;">Назначить после</th>
                <th>Статус заявки</th>
				<th>Статус склада</th>
                <th>Вес / Мест</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            <? foreach ($plan as $forwarder_id => $p): ?>
                <tr>
                    <td colspan="17" class="bg-gray">
                        Маршрутный лист
                        <img src="/upload/<? echo $p['user_photo']; ?>" alt="" style="height: 20px; vertical-align: top;"/>
                        <? echo $p['name']; ?>
                        <? echo $p['auto_name']; ?> <? echo $p['auto_number']; ?>
						<a  href="<? echo  current_url(); ?>?print=<? echo $forwarder_id; ?>" target="_blank"> <i class="fa fa-print"></i> печать</a>
                        <span id="forwarder_rout_link_<? echo $forwarder_id; ?>"></span>
                    </td>

                </tr>

                <? foreach ($p['orders'] as $order): ?>
                <tr id="order_row_<? echo $order['id']; ?>">
					<td><? echo $order['author_first_name'].' '.$order['author_last_name']; ?></td>
					<td><a href="/order/view/<? echo $order['id']; ?>"><? echo $order['id']; ?></a></td>
                    <td>
						<? if( $this->session->user->role == 1): ?>
                            <input type="text"
                                   class="changeOrder no-padding  order-field"
								   style="width: 75px;"
                                   name="delivery_date"
                                   data-date-format="dd-mm-yyyy"
                                   data-date-language="ru"
                                   data-date-autoclose="1"
                                   data-provide="datepicker"
                                   data-order="<? echo $order['id'];?>"
                                   value="<? if($order['delivery_date'] > 0) echo my_date($order['delivery_date']);?>"
                            >
                        <? else:?>
                            <? if($order['delivery_date'] > 0) echo my_date($order['delivery_date']);?>
                        <? endif;?>
                    </td>
                    <td>
                        <? if( $this->session->user->role == 1): ?>
                        <select class="order-field no-padding" name="forwarder_id" data-order="<? echo $order['id'];?>" style="width: 100%;">
                            <option></option>
                            <?php foreach ($forwarders as $f): ?>
                                <option value="<? echo $f['id'] ; ?>"  <? if($f['id'] == $order['forwarder_id']) echo 'selected'; ?>
                                ><? echo $f['first_name']. ' '.$f['last_name'] ; ?></option>
                            <? endforeach; ?>
                        </select>
                        <? elseif($order['forwarder_fn']):?>
                            <? echo $order['forwarder_fn'].' '.$order['forwarder_ln'];?>
                        <? endif;?>
                    </td>
                    <td>
                        <input class="no-padding order-field plan_ord_input ord_forwarder_<? echo $forwarder_id;?>"
							   name="ord"
							   value="<? echo $order['ord']; ?>"
                               data-order="<? echo $order['id'];?>"
                               onchange="<? if($order['next_order']):?>order_check_next(this, <? echo $order['id'];?>, <? echo $order['next_order'];?>);<? endif;?>
                                       order_check_pos('.ord_forwarder_<? echo $forwarder_id;?>');">
                    </td>
                    <td>
                        <? if( $this->session->user->role == 1): ?>
                        <select class="no-padding order-field" name="operation"
								style="width: 110px;"
								data-order="<? echo $order['id'];?>">
                            <?php foreach ($this->config->item('orders_operation') as $operation_id => $operation): ?>
                                <option value="<? echo $operation_id ; ?>" <? if($operation_id == $order['operation']) echo 'selected'; ?>
                                ><? echo $operation ;?></option>
                            <? endforeach; ?>
                        </select>
                        <? else:?>
                            <? echo $this->config->item($order['operation'], 'orders_operation'); ?>
                        <? endif;?>
                    </td>
                    <td style="word-break:break-all;">
                        <? echo $order['invoice']; ?>
                    </td>
                    <td>
                        <? echo $order['contractor_name']; ?>
                    </td>
                    <td>
                        <? echo $order['contractor_contact']; ?>
                    </td>
                    <td>
                        <? echo $order['contractor_address']; ?>
                    </td>

                    <td>
                        <? echo $order['contact_info'];?>
                        <? if($order['contact_map']):?>
                            <br><? echo $order['contact_map'];?>
                        <? endif;?>
                    </td>
                    <td>
                        <? if( $this->session->user->role == 1): ?>
                        <textarea rows="1" class="no-padding order-field"  name="note"
                                  data-order="<? echo $order['id'];?>"><? echo $order['note'];?></textarea>
                        <? else:?>
                            <? echo $order['note'];?>
                        <? endif;?>
                    </td>
                    <td>
                        <? if( $this->session->user->role == 1): ?>
                            <input type="text"
                                   class="no-padding order-field"
                                   name="next_order"
								   style="width: 30px; vertical-align: middle"
                                   id="next_order_<? echo $order['id'];?>"
                                   data-order="<? echo $order['id'];?>"
                                   value="<? echo $order['next_order'];?>">

                              <button type="button" class="btn btn-info btn-xs" title="найти"
                                      onclick="order_check(this, $('#next_order_<? echo $order['id'];?>') );"><i class="fa fa-search"></i></button>

                        </div>
                        <? else:?>
                            <? echo $order['next_order'];?>
                        <? endif;?>
                    </td>

                    <td>
                        <? if( $this->session->user->role == 1): ?>
                        <select name="status" class="no-padding order-field" data-order="<? echo $order['id'];?>">
                            <?php foreach ($this->config->item('orders_status') as $status_id => $status): ?>
                                <option value="<? echo $status_id; ?>" <? if ($status_id == $order['status']) echo 'selected'; ?> ><? echo $status; ?></option>
                            <? endforeach; ?>
                        </select>
                        <? else:?>
                            <? echo $this->config->item($order['status'], 'orders_status'); ?>
                        <? endif;?>
                    </td>
					<td>
						<? echo $this->config->item($order['stock_status'], 'stock_status'); ?>
					</td>
                    <td><? echo $order['weight'].' / '.$order['place']; ?></td>
                    <td>
                        <button class="btn btn-primary order-save-button"
                                id="order_save_button_<? echo $order['id'];?>"
                                onclick="plan_save(<? echo $order['id'];?>);" ><i class="fa fa-save"></i></button>
                    </td>
                </tr>
                <? endforeach; ?>
            <? endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<button type="button" class="btn btn-info" onclick="$('#planMap').toggle();"><i class="fa fa-map"></i> Карта</button>

<div id="planMap" style="height: 800px; margin: 20px 200px; display: none;"></div>
<script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>

<script type="text/javascript">
	var orders = [
		<? foreach ($map_points as $p) { $desc = $p['contractor_name'].', '.$p['contractor_contact'];
		if (!$p['coords']['lat']) continue; ?>
		{
			point: [<? echo $p['coords']['lat']?>, <? echo $p['coords']['long']?>],
			description: `<? echo nl2br($desc);?>`,
			address: `<? echo $p['contractor_address'];?>`,
		},
		<? } ?>

	];

	var map;

	DG.then(function () {
		map = DG.map('planMap', {
			center: orders[0].point,
			zoom: 13
		});
		for (let orderPoint of orders) {

			DG.marker(orderPoint.point).addTo(map).bindPopup(orderPoint.description).bindLabel(orderPoint.address,{static:true});
		}
	});

</script>

