<table class="table table-borders table-hover">
	<tr>
		<td><b>Инициатор:</b></td>
		<td><? echo $order->author_first_name . ' ' . $order->author_last_name; ?></td>
	</tr>
	<tr>
		<td><b>Экспедитор:</b></td>
		<td><? echo $order->forwarder_first_name . ' ' . $order->forwarder_last_name; ?></td>
	</tr>

	<tr>
		<td><b>Тип операции:</b></td>
		<td><? echo $this->config->item($order->operation, 'orders_operation'); ?></td>
	</tr>
	<tr>
		<td><b>Статус:</b></td>
		<td><? echo $this->config->item($order->status, 'orders_status'); ?></td>
	</tr>
	<tr>
		<td><b>Дата по плану:</b></td>
		<td><? echo my_date($order->delivery_date); ?></td>
	</tr>
	<tr>
		<td><b>Накладные</b></td>
		<td><? echo $order->invoice; ?></td>
	</tr>
	<tr>
		<td><b>Контрагент</b></td>
		<td><? echo $order->contractor_name; ?>, <? echo $order->contact_name; ?>, <? echo $order->contact_info; ?>, <? echo $order->address_name; ?> </td>
	</tr>
	<tr>
		<td><b>Примечание</b></td>
		<td><? echo nl2br($order->note); ?></td>
	</tr>
	<tr>
		<td><b>Назначить после</b></td>
		<td><? echo $order->next_order; ?></td>
	</tr>
	<tr>
		<td><b>Вес</b></td>
		<td><? echo $order->weight; ?> кг.</td>
	</tr>
	<tr>
		<td><b>Мест</b></td>
		<td><? echo $order->place; ?> шт.</td>
	</tr>
	<tr>
		<td><b>Объем</b></td>
		<td><? echo $order->volume; ?> м³</td>
	</tr>

	<tr>
		<td><b>Статус склада</b></td>
		<td><? echo $this->config->item($order->stock_status, 'stock_status'); ?></td>
	</tr>
	<tr>
		<td><b>Примечание склада</b></td>
		<td><? echo nl2br($order->note_stock); ?></td>
	</tr>
	<tr>
		<td><b>Дата создания:</b></td>
		<td><? echo my_date($order->date_created, 'd-m-Y H:i:s'); ?></td>
	</tr>
	<tr>
		<td><b>Дата обновления:</b></td>
		<td><? echo my_date($order->date_updated, "d-m-Y H:i:s"); ?></td>
	</tr>
	<tr>
		<td><b>Дата сборки:</b></td>
		<td><? echo my_date($order->date_stock_done, "d-m-Y H:i:s"); ?></td>
	</tr>
	<tr>
		<td><b>Файлы:</b></td>
		<td>
			<? foreach ($order->files as $f) { ?>
				<a target="_blank" href="http://delivery.3852522.ru/upload/orders/<? echo $f['order_id'] . '/' . $f['file_name']; ?>"><? echo $f['file_name']; ?> <i class="fa fa-external-link"></i></a> ,
			<? } ?>
		</td>
	</tr>

</table>
