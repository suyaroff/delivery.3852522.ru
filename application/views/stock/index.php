<div class="modal fade"  id="order_form" >
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
</div>
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Фильтр</h3>
	</div>
	<form class="form-horizontal" action="" method="get">
		<div class="box-body">
			<div class="form-group">
				<label class="col-sm-2 control-label">Статус</label>
				<div class="col-sm-10">
					<select name="status" class="form-control">
						<option value="">Выберите статус</option>
						<? foreach ($this->config->item('orders_status') as $s_id => $s_name) { ?>
							<option value="<? echo $s_id; ?>" <? if($status == $s_id) echo 'selected'; ?> ><? echo $s_name; ?></option>
						<? } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Статус склада</label>
				<div class="col-sm-10">
					<select name="stock_status" class="form-control">
						<option value="">Выберите статус склада</option>
						<? foreach ($this->config->item('stock_status') as $s_id => $s_name ) { ?>
							<option value="<? echo $s_id;?>" <? if(isset($stock_status) && $stock_status == $s_id) echo 'selected';?> ><? echo $s_name;?></option>
						<? } ?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Контрагент</label>
				<div class="col-sm-10">
					<select name="contractor" class="form-control select2">
						<option value="">Выберите из списка</option>
						<? foreach ($contractors as $c) { ?>
							<option value="<? echo $c['id'];?>" <? if(isset($contractor) && $contractor == $c['id']) echo 'selected';?> ><? echo $c['name'];?></option>
						<? } ?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Номер счета</label>
				<div class="col-sm-10">
					<input type="text" name="invoice_contractor" class="form-control " autocomplete="off"
						   value="<? if(isset($_GET['invoice_contractor']) && $_GET['invoice_contractor'] != '') echo $_GET['invoice_contractor'];?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Дата доставки</label>
				<div class="col-sm-10">
					<input type="text" name="date" value="<? if(isset($_GET['date']) && $_GET['date'] != '') echo $_GET['date'];?>" class="form-control datepicker-field" autocomplete="off"  >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-info">Фильтровать</button>
					<a href="/stock" class="btn btn-default pull-right"> Сбросить фильтр</a>
				</div>
			</div>
		</div>
	</form>
</div>

<? if(count($orders)) { if(isset($_GET['print'])) unset($_GET['print']);?>
	<p>
		<a href="/stock?print=all&<? echo http_build_query($_GET);?>" target="_blank" class="btn btn-info">Распечатать <i class="fa fa-print"></i></a>
	</p>
<? foreach ($orders as $order) { ?>
	<div class="box box-solid <? echo $this->config->item($order['stock_status'], 'stock_status_color');?>">
		<div class="box-header">
			<h3 class="box-title">
				<a href="/order/view/<? echo $order['id']; ?>">Сборка № <? echo $order['id']; ?> - <? if($order['stock_status'] >= 0) echo $this->config->item($order['stock_status'], 'stock_status');?></a>
			</h3>
			<div class="box-tools pull-right">
				<a class="btn btn-box-tool"
				   title="Редактровать"
				   data-toggle="modal"
				   data-target="#order_form"
				   href="/stock/edit/<? echo $order['id'];?>"><i class="fa fa-wrench"></i></a>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-sm-3">
					<b>Статус:</b> <? echo $this->config->item($order['status'], 'orders_status');?><br>
					<b>Дата создания:</b> <? echo my_date($order['date_created'], "d-m-Y H:i:s"); ?><br>
					<b>Дата доставки:</b> <? echo my_date($order['delivery_date']); ?><br>
					<b>Экспедитор:</b> <? echo $order['forwarder_fn'] . ' ' . $order['forwarder_ln']; ?><br>
				</div>
				<div class="col-sm-3">
					<b>Контрагент:</b> <? echo $order['contractor_name']; ?><br>
					<b>Накладные:</b> <? echo $order['invoice']; ?><br>
					<b>Номер счета:</b> <? echo $order['invoice_contractor'] ? $order['invoice_contractor']:'не указан'; ?><br>
				</div>
				<div class="col-sm-3">
					<b>Инициатор:</b> <? echo $order['author_first_name'].' '.$order['author_last_name']; ?><br>
					<b>Примечания:</b><? echo nl2br($order['note']);?><br>
					<?  if($order['files']) { ?><b>Файлы:</b> <? foreach ($order['files'] as $f) { ?>
						<a target="_blank" href="/upload/orders/<? echo $f['order_id'] . '/' . $f['file_name']; ?>"><? echo $f['file_name']; ?> <i class="fa fa-external-link"></i></a>
						<? }} ?>
				</div>
				<div class="col-sm-3">
					<b>Статус склада:</b> <? echo $this->config->item($order['stock_status'], 'stock_status');?><br>
					<b>Мест:</b> <? echo $order['place']; ?> шт.
					<b>Вес:</b> <? echo $order['weight']; ?> кг.
					<b>Объем:</b> <? echo $order['volume']; ?> м³<br>
					<? $date_stock_done = my_date($order['date_stock_done'], "d-m-Y H:i:s"); if($date_stock_done) { ?><b>Время сборки :</b> <? echo $date_stock_done; ?><br><? } ?>
					<? if($order['note_stock']) { ?><b>Примечания склада:</b> <? echo $order['note_stock']; ?><br><? } ?>
				</div>
			</div>
		</div>
	</div>
<? } ?>

<? } else { ?>
	<div class="alert alert-danger">Сборки не найдены</div>
<? } ?>
