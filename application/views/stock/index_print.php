<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><? echo $title; ?></title>
	<style>
		body{
			margin: 0;
			padding: 0;
			font-size: 14px;
		}
		.table {
			width: 100%;
			border-collapse: collapse;
		}
		.table td, .table th {
			border: 1px solid #666666;
		}
	</style>
</head>
<body>
<h1><? echo $title; ?></h1>

<table class="table table-bordered">
	<thead>
	<tr>
		<th style="width:80px;">Дата <br> доставки</th>
		<th style="width: 50px;">Номер</th>
		<th>Экспедитор</th>
		<th>Накладные</th>
		<th>Контрагент</th>
		<th>Примечания</th>
		<th style="width: 100px;">Мест</th>
		<th>Примечания склада</th>
		<th style="width: 145px;">Статус склада</th>
	</tr>
	</thead>
	<tbody>
	<? foreach ($orders as $order): ?>
		<tr id="order_row_<? echo $order['id']; ?>" class="<? echo $this->config->item($order['stock_status'], 'stock_status_color'); ?>">
			<td style="text-align: center;"><? if ($order['delivery_date'] > 0) echo my_date($order['delivery_date']); ?></td>
			<td style="text-align: center;"><? echo $order['id']; ?></td>
			<td><? echo $order['forwarder_fn'] . ' ' . $order['forwarder_ln']; ?></td>
			<td><? echo $order['invoice']; ?></td>
			<td><? echo $order['contractor_name']; ?></td>
			<td><? echo $order['note']; ?></td>
			<td style="text-align: center;"><? echo $order['place']; ?> шт.</td>
			<td><? echo nl2br($order['note_stock']); ?></td>
			<td style="text-align: center;"><? echo $this->config->item($order['stock_status'], 'stock_status'); ?></td>
		</tr>
	<? endforeach; ?>
	</tbody>
</table>

<b>Время:</b> <? echo date("d-m-Y H:i:s");?>

<script>
	window.print();
</script>

</body>
</html>
