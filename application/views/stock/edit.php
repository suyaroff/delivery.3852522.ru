<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Редактирование сборки № <? echo $order->id;?></h4>
</div>
<div class="modal-body">
    <?php echo form_open('', array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'stock_edit_form')); ?>
	<table class="table table-bordered">
		<tr>
			<td class="text-right col-sm-3" ><b>Экспедитор</b></td>
			<td> <? echo $order->forwarder_first_name.' '.$order->forwarder_last_name; ?></td>
		</tr>
		<tr>
			<td class="text-right"><b>Дата доставки</b></td>
			<td><? echo my_date($order->delivery_date);?></td>
		</tr>
		<tr>
			<td class="text-right"><b>Контрагент</b></td>
			<td>
				<? echo $order->contractor_name; ?>
				<? echo $order->contact_name; ?>
				<? echo $order->contact_info; ?>
			</td>
		</tr>
		<tr>
			<td class="text-right"><b>Накладные</b></td>
			<td><? echo $order->invoice;?></td>
		</tr>
		<? if($order->note){ ?>
		<tr>
			<td class="text-right"><b>Примечание</b></td>
			<td class="text-dangers"><? echo $order->note;?></td>
		</tr>
		<? } ?>
	</table>
	<div class="form-group">
		<label class="col-sm-3 control-label">Примечание склада</label>
		<div class="col-sm-9">
			<textarea name="note_stock" rows="3" class="form-control order-field" id="field_note_stock"><? echo $order->note_stock;?></textarea>
			<input type="hidden" name="old[note_stock]" id="field_old_note_stock" value="<? echo $order->note_stock;?>" class="order-field"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Статус сборки</label>
		<div class="col-sm-9">
			<select class="form-control order-field" name="stock_status" id="field_stock_status">
				<?php foreach ($this->config->item('stock_status') as $ss_id => $s_name): ?>
					<option value="<? echo $ss_id ; ?>" <? if($order->stock_status == $ss_id) {?>selected="selected" <? } ?> ><? echo $s_name ;?></option>
				<? endforeach; ?>
			</select>
			<input type="hidden" name="old[stock_status]" id="field_old_stock_status" value="<? echo $order->stock_status;?>" class="order-field"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label">Мест</label>
		<div class="col-md-9">
			<input name="place" type="text" class="form-control  order-field" value="<? echo $order->place;?>"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label">Вес</label>
		<div class="col-md-9">
			<input name="weight" type="text" class="form-control  order-field" value="<? echo $order->weight;?>"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Объем</label>
		<div class="col-sm-9">
			<input name="volume" type="text" class="form-control  order-field" value="<? echo $order->volume;?>"/>
		</div>
	</div>




    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <button type="button" class="btn btn-info button-save" onclick="stock_save(<? echo $order->id;?>)">Сохранить</button>
        </div>
    </div>
</form>
</div>
<script>
    $('.datepicker-field').datepicker({
        autoclose: true,
        format: "dd-mm-yyyy",
        language: 'ru',
        todayHighlight: true
    });
</script>
