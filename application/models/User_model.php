<?php

/**
 * Created by PhpStorm.
 * User: Jamal
 * Date: 05.03.2017
 * Time: 21:40
 */
class User_model extends CI_Model
{

    public function hash_password($password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function set_login($login) {

        $login = mb_strtolower($login);
        $login = str_replace(' ', '', $login);
        return $login;
    }

    function get($id) {
        $user = $this->db->get_where('users', array('id' => $id))->row();
        return $user;
    }
}
