<?php

class Buy_model extends CI_Model
{

	public $labels = array(
		'reg_number' => 'Регистрационный номер',
		'buy_url' => 'Ссылка на процедуру',
		'place' => 'Площадка',
		'place_url' => 'Адрес площадки',
		'customer' => 'Заказчик',
		'bid_date_end' => 'Дата окончания рассмотрения заявки',
		'auction_date' => 'Дата проведения аукциона',
		'bid_status' => 'Статус заявки',
		'buy_status' => 'Статус процедуры',
		'max_contract_price' => 'Начальная цена контракта',
		'bid_price' => 'Обеспечение заявки',
		'buy_description' => 'Наименование объекта закупки',
		'buy_price' => 'Цена закупки',
		'calculate_user' => 'Кто расчитал',
		'description_1' => 'Примечание 1',
		'description_2' => 'Примечание 2',
		'manager' => 'Ответственный менеждер',
		'down_sum' => 'Сумма снижения',
		'calculate_time' => 'Начало работ',
		'items' => 'Объекты закупки',
		'partner_id' => 'От кого участвуем',
		'winner' => 'Победитель'
	);

	/* добавление закупки */
	public function add_buy($data)
	{
		$data['date_created'] = date('Y-m-d H:i:s');
		$result = $this->db->insert('buys', $data);
		return $this->db->insert_id();
	}

	/* покупка */
	public function get_buy($id)
	{
		settype($id, 'int');
		$sql = "SELECT b.*, 
					u1.first_name as calc_user_first_name, 
					u1.last_name as calc_user_last_name,
					u2.first_name as manager_first_name, 
					u2.last_name as manager_last_name,
					p.name as partner_name,
					bp.name as winner_name,
					bp.offer as winner_offer
				FROM  buys b 
					LEFT OUTER JOIN users u1 ON b.calculate_user = u1.id
					LEFT OUTER JOIN users u2 ON b.manager = u2.id
					LEFT OUTER JOIN partners p ON b.partner_id = p.id
					LEFT OUTER JOIN buy_participant bp ON b.winner = bp.id
				WHERE b.id = $id";

		return $this->db->query($sql)->row();
	}

	/* сохранение покупки */
	public function save($data, $id)
	{
		$saved = $this->db->update('buys', $data, array('id' => $id));
		return $saved;
	}

	/* закупки на главной странице */
	public function get_index()
	{
		$now = date('Y-m-d H:i');
		$buys = array();
		$sql_base = "SELECT b.*, 
					u1.first_name AS calc_user_first_name, 
					u1.last_name AS calc_user_last_name,
					u2.first_name AS manager_first_name, 
					u2.last_name AS manager_last_name,
					bp.name AS winner_name,
					bp.offer AS winner_offer
				FROM  buys b 
					LEFT OUTER JOIN users u1 ON b.calculate_user = u1.id
					LEFT OUTER JOIN users u2 ON b.manager = u2.id
					LEFT OUTER JOIN buy_participant bp ON b.winner = bp.id
				WHERE b.trash = 0 ";

		/* первая вкладка  со статусом не указано, участвуем. С сортровкой по дате подачи*/
		$sql1 = $sql_base . " AND b.bid_status IN (0, 1) AND bid_date_end > '$now' ORDER BY bid_date_end LIMIT 100";
		$buys['tab1'] = $this->db->query($sql1)->result();
		/* вторая вкладка подана С сортировкой по дате аукциона-вверху*/
		$sql2 = $sql_base . " AND b.bid_status IN (3,4) AND auction_date > '$now' ORDER BY auction_date LIMIT 100";
		$buys['tab2'] = $this->db->query($sql2)->result();
		/* выиграные */
		$sql3 = $sql_base . " AND b.bid_status = 7 ORDER BY id DESC LIMIT 100";
		$buys['tab3'] = $this->db->query($sql3)->result();
		/* Просроченые */
		$sql4 = $sql_base . " AND b.bid_status IN(0, 1, 3, 4, 5) AND auction_date < '$now' ORDER BY auction_date DESC LIMIT 100";
		$buys['tab4'] = $this->db->query($sql4)->result();
		/* Проиграные */
		$sql5 = $sql_base . " AND b.bid_status = 6 ORDER BY auction_date DESC LIMIT 100";
		$buys['tab5'] = $this->db->query($sql5)->result();


		return $buys;
	}

	/* удаленные закупки */
	public function get_deleted()
	{
		$sql = "SELECT b.*, 
					u1.first_name AS calc_user_first_name, 
					u1.last_name AS calc_user_last_name,
					u2.first_name AS manager_first_name, 
					u2.last_name AS manager_last_name
				FROM  buys b 
					LEFT JOIN users u1 ON b.calculate_user = u1.id
					LEFT JOIN users u2 ON b.manager = u2.id
				WHERE b.trash = 1	
				ORDER BY b.trash_date DESC	
				";
		return $this->db->query($sql)->result();
	}


	/* список пользователей для этого проекта */
	public function users_list()
	{
		return $this->db->where('role IN (1, 5, 6, 7) ')->order_by('role, first_name')->get('users')->result();
	}

	public function get_log($buy_id)
	{
		$result = $this->db->select('bl.*, u.first_name, u.last_name')->from('buy_log bl')
			->join('users u', 'bl.user_id = u.id', 'LEFT')
			->order_by('bl.log_time', 'DESC')
			->where('bl.buy_id', $buy_id)->get()->result();

		return $result;
	}

	public function get_for_operators()
	{

		$sql = "SELECT b.*, 
					u1.first_name AS calc_user_first_name, 
					u1.last_name AS calc_user_last_name,
					u2.first_name AS manager_first_name, 
					u2.last_name AS manager_last_name
				FROM  buys b 
					LEFT JOIN users u1 ON b.calculate_user = u1.id
					LEFT JOIN users u2 ON b.manager = u2.id
				WHERE b.bid_status = 1	AND b.trash = 0
				";
		return $this->db->query($sql)->result();
	}


	/**
	 * @param $id int
	 * @param $input array
	 * @param $buy object
	 * Лог закупок
	 */
	public function log_buy($id, $input, $buy)
	{
		$log_data = array(
			'log_time' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->user->id,
			'buy_id' => $id,
			'dif' => ''
		);
		foreach ($input as $f => $v) {
			$old_v = $buy->$f;
			if ($f == 'buy_status') {
				$v = $this->config->item($v, 'buy_status');
				$old_v = $this->config->item($old_v, 'buy_status');
			}
			if ($f == 'bid_status') {
				$v = $this->config->item($v, 'bid_status');
				$old_v = $this->config->item($old_v, 'bid_status');
			}
			if ($v && ($f == 'calculate_user' || $f == 'manager')) {
				$user = $this->user_model->get($v);
				$v = $user->first_name . ' ' . $user->last_name;
				if ($old_v) {
					$user = $this->user_model->get($old_v);
					$old_v = $user->first_name . ' ' . $user->last_name;
				}
			}
			if ($v && $f == 'partner_id') {
				$partner = $this->buy_model->get_partner($v);
				$v = $partner->name;
				if ($old_v) {
					$old_v = $this->buy_model->get_partner($old_v);
				}
			}


			$log_data['dif'] .= '<b>' . $this->labels[$f] . '</b>: ' . $old_v . " &raquo; " . $v . '<br>';
		}
		$this->db->insert('buy_log', $log_data);
	}

	/**
	 * @param $id int
	 * Удаление закупки
	 */
	public function delete($id)
	{
		$now = date('Y-m-d H:i:s');
		$this->db->update('buys', array('trash' => 1, 'trash_date' => $now), array('id' => $id));
		$log_data = array(
			'log_time' => $now,
			'user_id' => $this->session->user->id,
			'buy_id' => $id,
			'dif' => '<b class="text-danger">Заявка удалена</b>'
		);
		$this->db->insert('buy_log', $log_data);
	}

	/**
	 * @return mixed
	 * Список партнеров
	 */
	public function get_partners()
	{
		return $this->db->select('*')->order_by('name')->get('partners')->result();
	}

	/**
	 * @param $id
	 * @return mixed
	 * Один партнер
	 */
	public function get_partner($id)
	{
		return $this->db->get_where('partners', array('id' => $id))->row();
	}

	/**
	 * @param $partner
	 * Добавление партнера
	 * @return mixed
	 */
	public function add_partner($partner)
	{
		return $this->db->insert('partners', $partner);
	}

	/**
	 * @param $id
	 * @return mixed
	 * Удаление партнера
	 */
	public function delete_partner($id)
	{
		return $this->db->delete('partners', array('id' => $id));
	}

	public function save_partner($data, $id)
	{
		return $this->db->update('partners', $data, array('id' => $id));
	}

	public function get_participants($buy_id)
	{
		return $this->db->get_where('buy_participant', array('buy_id' => $buy_id))->result();
	}

	public function get_statistic()
	{
		$year = date("Y");

		$sql = "SELECT COUNT(ID) AS num, bid_status, YEAR(date_created) AS y, MONTH(date_created) AS m
            FROM buys WHERE YEAR(date_created) = $year  GROUP BY MONTH(date_created), bid_status
            ORDER BY date_created DESC";
		$result = $this->db->query($sql);
		$data = array();

		for ($i = 1; $i <= 12; $i++) {
			$data[0][$i . '-' . $year] = 0;
			$data[1][$i . '-' . $year] = 0;
			$data[2][$i . '-' . $year] = 0;
			$data[3][$i . '-' . $year] = 0;
			$data[4][$i . '-' . $year] = 0;
			$data[5][$i . '-' . $year] = 0;
			$data[6][$i . '-' . $year] = 0;
			$data[7][$i . '-' . $year] = 0;
		}

		foreach ($result->result() as $s) {
			$data[$s->bid_status][$s->m . '-' . $s->y] = $s->num;
		}
		return $data;
	}

	public function find_buys($str)
	{
		$str = $this->db->escape('%'.$str.'%');
		$sql = "SELECT * FROM buys b WHERE b.reg_number LIKE $str OR b.customer LIKE $str OR b.buy_description LIKE $str";
		$data = $this->db->query($sql)->result_array();
		return $data;
	}

}
