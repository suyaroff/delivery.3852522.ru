<?php
/**
 * Created by PhpStorm.
 * User: suyar
 * Date: 01.03.2018
 * Time: 14:59
 */

class Order_model extends CI_Model
{
	public $delivery_date;
	public $status;
	public $forwarder_id;
	public $operation;
	public $invoice;
	public $contractor;
	public $contractor_contact;
	public $note;
	public $next_order;
	public $weight;
	public $place;
	public $note_stock;
	public $stock_status;
	public $ord;
	public $author_id;
	public $date_created;
	public $date_updated;
	public $date_stock_done;
	public $deleted;
	public $volume;
	public $invoice_contractor;

	public $labels = [
		'delivery_date' => 'Дата доставки/забора',
		'status' => 'Статус',
		'forwarder_id' => 'Экспедитор',
		'operation' => 'Операция',
		'invoice' => 'Накладные',
		'contractor' => 'Контрагент',
		'contractor_contact' => 'Контактное лицо',
		'note' => 'Примечание',
		'next_order' => 'Назначить после',
		'weight' => 'Вес',
		'place' => 'Мест',
		'note_stock' => 'Примечание склада',
		'stock_status' => 'Статус склада',
		'ord' => 'Порядок',
		'date_created' => 'Дата создания',
		'date_updated' => 'Дата редактирования',
		'date_stock_done' => 'Время сборки',
		'deleted' => 'Удалено',
		'volume' => 'Объем',
		'author_id' => 'Инициатор',
		'invoice_contractor' => 'Номер счета',
	];

	public function get($id)
	{
		settype($id, 'int');

		$sql = "SELECT
                  o.*,
                  u.first_name as forwarder_first_name,
                  u.last_name as forwarder_last_name,
                  u.phone as forwarder_phone,
                  u.auto_name as forwarder_auto_name,
                  u.auto_number as forwarder_auto_number,
       			  a.first_name as author_first_name,
       			  a.last_name as author_last_name,
                  c.name as contractor_name,
                  c.type as contractor_type,
                  cc.contact as contact_name, 
                  cc.contact_info ,
                  cc.contact_map,
                  ca.address_name
                FROM orders o
                LEFT OUTER JOIN users u ON o.forwarder_id = u.id
			  	LEFT OUTER JOIN users a ON o.author_id = a.id
                LEFT OUTER JOIN contractor c ON o.contractor = c.id
                LEFT OUTER JOIN contractor_contact cc ON o.contractor_contact = cc.id
                LEFT OUTER JOIN contractor_address ca ON cc.address_id = ca.address_id
                WHERE o.id = $id";
		$order = $this->db->query($sql)->row();
		$order->files = $this->db->get_where('order_files', ['order_id' => $id])->result_array();
		return $order;
	}

	public function get_index()
	{
		$where = ' 1 = 1 ';
		/* фильтр  по дате доставки */
		if ( isset($_GET['date_from']) && !empty($_GET['date_from'])) {
			$date_from = $_GET['date_from'];
			$date_mysql = mysql_date($date_from);
			$where .= " AND o.delivery_date >= '$date_mysql' ";
		}
		if ( isset($_GET['date_to']) && !empty($_GET['date_to'])) {
			$date_to = $_GET['date_from'];
			$date_to_mysql = mysql_date($date_to);
			$where .= " AND o.delivery_date <= '$date_to_mysql' ";
		}
		/* Дата создания */
		if ( isset($_GET['date_created_from']) && !empty($_GET['date_created_from'])) {
			$date_created_from = $_GET['date_created_from'];
			$date_created_from_mysql = mysql_date($date_created_from);
			$where .= " AND o.date_created >= '$date_created_from_mysql' ";
		}
		if ( isset($_GET['date_created_to']) && !empty($_GET['date_created_to'])) {
			$date_created_to = $_GET['date_created_to'];
			$date_created_to_mysql = mysql_date($date_created_to).' 23:59:59';
			$where .= " AND o.date_created <= '$date_created_to_mysql' ";
		}
		/* Дата сборки */
		if ( isset($_GET['date_stock_done_from']) && !empty($_GET['date_stock_done_from'])) {
			$date_stock_done_from = $_GET['date_stock_done_from'];
			$date_stock_done_from_mysql = mysql_date($date_stock_done_from);
			$where .= " AND o.date_stock_done >= '$date_stock_done_from_mysql' ";
		}
		if ( isset($_GET['date_stock_done_to']) && !empty($_GET['date_stock_done_to'])) {
			$date_stock_done_to = $_GET['date_stock_done_to'];
			$date_stock_done_to_mysql = mysql_date($date_stock_done_to).' 23:59:59';
			$where .= " AND o.date_created <= '$date_stock_done_to_mysql' ";
		}
		/* Статус*/
		if ( isset($_GET['status']) && $_GET['status'] !='') {
			settype($_GET['status'], 'int');
			if($_GET['status'] >= 0) {
				$status = $_GET['status'];
				$where .= " AND o.status = $status ";
			}
		}
		/* Контрагент */
		if ( isset($_GET['contractor']) && $_GET['contractor'] !='') {
			settype($_GET['contractor'], 'int');
			if($_GET['contractor'] >= 0) {
				$contractor = $_GET['contractor'];
				$where .= " AND o.contractor = $contractor ";
			}
		}
		/* Инициатор */
		if ( isset($_GET['author_id']) && $_GET['author_id'] !='') {
			settype($_GET['author_id'], 'int');
			if($_GET['author_id'] > 0) {
				$author_id = $_GET['author_id'];
				$where .= " AND o.author_id = $author_id ";
			}
		}
		/* Накладная */
		if ( isset($_GET['invoice']) && $_GET['invoice'] !='') {
			$invoice = trim($_GET['invoice']);
			if($invoice) {
				$where .= " AND o.invoice LIKE '%$invoice%' ";
			}
		}
		/* номер счета*/
		if ( isset($_GET['invoice_contractor']) && $_GET['invoice_contractor'] !='') {
			$invoice = trim($_GET['invoice_contractor']);
			if($invoice) {
				$where .= " AND o.invoice_contractor LIKE '%$invoice%' ";
			}
		}

		$data = [];

		$sql = "SELECT
                  o.*,
                  u.first_name as forwarder_first_name,
                  u.last_name as forwarder_last_name,
                  u.phone as forwarder_phone,
                  u.auto_name as forwarder_auto_name,
                  u.auto_number as forwarder_auto_number,
       			  a.first_name as author_first_name,
       			  a.last_name as author_last_name,
                  c.name as contractor_name,
                  c.type as contractor_type,
                  cc.contact as contact_name, 
                  cc.contact_info ,
                  cc.contact_map,
                  ca.address_name
                FROM orders o
                LEFT OUTER JOIN users u ON o.forwarder_id = u.id
			  	LEFT OUTER JOIN users a ON o.author_id = a.id
                LEFT OUTER JOIN contractor c ON o.contractor = c.id
                LEFT OUTER JOIN contractor_contact cc ON o.contractor_contact = cc.id
                LEFT OUTER JOIN contractor_address ca ON cc.address_id = ca.address_id
                WHERE $where
				ORDER BY o.status, o.delivery_date DESC LIMIT 50";
		$query = $this->db->query($sql);
		foreach ($query->result_array() as $r) {
			$r['files'] = $this->db->get_where('order_files', ['order_id' => $r['id']])->result_array();
			$data[$r['status']][] = $r;
		}
		return $data;
	}

	public function delete($id)
	{
		$this->db->delete('orders', array('id' => $id));
		//$update = $this->db->update('orders', ['deleted' => 1], array('id' => $id));
		$this->delete_files($id);
	}

	public function delete_files($id)
	{
		settype($id, 'int');
		$sql = "SELECT * FROM order_files WHERE order_id = $id";
		$query = $this->db->query($sql);
		foreach ($query->result() as $f) {
			$path = ORDER_UPLOAD . $id . '/' . $f->name;
			@unlink($path);
		}
	}

	public function update($data, $id)
	{
		$order = $this->get($id);

		/* Фиксируем дату выполнения склада */
		if (isset($data['stock_status']) && $order->stock_status != $data['stock_status']) {
			if ($data['stock_status'] == '2' && $order->stock_status != 2) {
				$data['date_stock_done'] = date('Y-m-d H:i:s');
			}
		}
		if (isset($data['delivery_date']) && !empty($data['delivery_date'])) {
			$data['delivery_date'] = mysql_date($data['delivery_date']);
		}

		/* меняем статус склада если снова поместили в сборку */
		if(isset($data['status']) && $data['status'] == 1 && $order->status > 2  ) {
			$data['stock_status'] = 0;
		}

		/* дата обновления */
		$data['date_updated'] = date('Y-m-d H:i:s');
		$this->log($data, $id);
		$update = $this->db->update('orders', $data, array('id' => $id));

		return $update;
	}

	public function add_file($file, $id)
	{
		/* добавляем фаил */
		if ($file['error'] == 0) {
			$dir = ORDER_UPLOAD . $id . '/';
			if (!is_dir($dir)) mkdir($dir, 0775);
			$file_name = $_FILES['doc']['name'];
			$file_ext = pathinfo($_FILES['doc']['name'], PATHINFO_EXTENSION);
			$path = $dir . $file_name;
			$upload = move_uploaded_file($_FILES['doc']['tmp_name'], $path);
			if ($upload) {
				$now = date("Y-m-d H:i:s");
				$order_files = ['order_id' => $id, 'file_name' => $file_name, 'file_ext' => $file_ext, 'created' => $now, 'author' => $this->session->user->id];
				$this->db->insert('order_files', $order_files);
				$this->log(['description' => 'Добавлен новый фаил:' . $file_name . ''], $id);
			}
		}
	}

	public function add($order)
	{
		$order['author_id'] = $this->session->user->id;
		$order['date_created'] = date("Y-m-d H:i:s");
		$order['date_updated'] = $order['date_created'];
		$this->db->insert('orders', $order);
		$id = $this->db->insert_id();
		$this->log($order, $id);
		return $id;
	}

	public function log($input, $id, $description = '')
	{

		$order = $this->get($id);
		$log_data = array(
			'log_time' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->user->id,
			'order_id' => $id,
			'description' => $description
		);
		if (!$description) {
			foreach ($input as $f => $v) {
				$old_v = $order->$f;
				if ($old_v == $v) continue;
				if ($f == 'status') {
					$v = $this->config->item($v, 'orders_status');
					$old_v = $this->config->item($old_v, 'orders_status');
				}
				if ($f == 'stock_status') {
					$v = $this->config->item($v, 'stock_status');
					$old_v = $this->config->item($old_v, 'stock_status');
				}
				// Экспедитор
				if ($v && $f == 'forwarder_id') {
					$forwarder = $this->db->query("SELECT * FROM users WHERE id = $v")->row();
					$v = $forwarder->first_name . ' ' . $forwarder->last_name;
					if ($old_v) {
						$forwarder_old = $this->db->query("SELECT * FROM users WHERE id = $old_v")->row();
						$old_v = $forwarder_old->first_name . ' ' . $forwarder_old->last_name;
					}
				}
				// контрагент
				if ($v && $f == 'contractor') {
					$contractor = $this->db->query("SELECT * FROM contractor WHERE id = $v")->row();
					$v = $contractor->name;
					if ($old_v) {
						$contractor_old = $this->db->query("SELECT * FROM contractor WHERE id = $old_v")->row();
						$old_v = $contractor_old->name;
					}
				}
				if ($v && $f == 'contractor_contact') {
					$contractor_contact = $this->db->query("SELECT * FROM contractor_contact WHERE id = $v")->row();
					$v = $contractor_contact->contact;
					if ($old_v) {
						$contractor_contact_old = $this->db->query("SELECT * FROM contractor_contact WHERE id = $old_v")->row();
						$old_v = $contractor_contact_old->contact;
					}
				}
				/* даты */
				if ($v && $f == 'delivery_date') { $v = my_date($v, 'd-m-Y H:i:s'); }
				if ($v && $f == 'date_updated') { $v = my_date($v, 'd-m-Y H:i:s'); }
				if ($v && $f == 'date_stock_done') { $v = my_date($v, 'd-m-Y H:i:s'); }

				$log_data['description'] .= '<b>' . $this->labels[$f] . '</b>: ' . $old_v . " &raquo; " . $v . '<br>';
			}
		}

		$this->db->insert('order_log', $log_data);
	}

	public function order_log_get($id)
	{
		$data = [];
		$sql = "SELECT
                  o.*, u.first_name as user_first_name, u.last_name as user_last_name
                FROM order_log o
			  	LEFT OUTER JOIN users u ON o.user_id = u.id
                WHERE o.order_id = $id
				ORDER BY o.log_time DESC";
		$query = $this->db->query($sql);
		foreach ($query->result_array() as $r) {
			$data[] = $r;
		}
		return $data;
	}
}
