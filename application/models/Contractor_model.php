<?php

/**
 * Created by PhpStorm.
 * User: suyar
 * Date: 08.02.2019
 * Time: 10:34
 */
class Contractor_model extends CI_Model
{

	public function get_list()
	{
		$data = [];
		$sql = "SELECT c.*, ca.address_id, ca.address_name, cc.contact, cc.contact_info, cc.contact_map 
                FROM contractor c
                LEFT JOIN contractor_address ca ON ca.contractor_id = c.id
                LEFT JOIN contractor_contact cc ON cc.address_id = ca.address_id
                GROUP BY c.id ORDER BY c.name";
		$query = $this->db->query($sql);
		foreach ($query->result() as $r) {
			$r->type_name = $this->config->item($r->type, 'contractor_type');
			$data[] = $r;
		}
		return $data;
	}

	public function get_full($id)
	{
		$data['contractor'] = $this->db->where('id', $id)->get('contractor')->row();
		$data['contractor_address'] = $this->db->where('contractor_id', $id)->get('contractor_address')->result();
		$sql = "SELECT cc.* FROM contractor_contact cc LEFT JOIN contractor_address ca ON cc.address_id = ca.address_id WHERE ca.contractor_id = $id";
		$data['contractor_contact'] = $this->db->query($sql)->result();
		return $data;
	}
}
