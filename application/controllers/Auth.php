<?php

/**
 * Авторизация
 */
class Auth extends CI_Controller
{


    //Вход в систему
    public function login()
    {
        $this->load->library('form_validation');
        $data = array();
        if ($this->form_validation->run() == TRUE) {
			redirect('/');
        }

        $this->load->view('login', $data);

    }

    public function user_check($password)
    {
        $this->form_validation->set_message('user_check', 'Не верный логин или пароль');
        if ($password) {
            $login = $this->user_model->set_login($this->input->post('login'));
            $user = $this->db->get_where('users', array('login' => $login))->row();
            if($user){
                $result = password_verify($password, $user->password);
                if ($result == true) {
                    $this->session->user = $user;
                    return true;
                } else {
                    return false;
                }
            }else{
                return false;
            }

        } else {
            return FALSE;
        }


    }


    //выход из системы
    public function logout()
    {
        $this->session->unset_userdata('user');
        redirect('/auth/login');
    }

}
