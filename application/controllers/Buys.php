<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buys extends CI_Controller
{
	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('buy_model');
	}

	/* Главная страница с фильтром и закупками */

	public function index()
	{
		if ($this->session->user->role == 5) redirect('/buys/operator');
		$this->data['h1'] = $this->data['title'] = 'Закупки';
		$data['buys'] = $this->buy_model->get_index();
		$this->data['content'] = $this->load->view('buys/index', $data, true);
		$this->load->view('layout', $this->data);
	}

	/* добавление закупки с автозаполнением */

	public function add()
	{
		$this->data['h1'] = $this->data['title'] = 'Добавление закупки';
		$this->load->library('form_validation');
		if ($this->form_validation->run() == TRUE) {
			$data_insert = array_map('trim', $this->input->post());
			$data_insert['items'] = base64_decode($data_insert['items']);
			$buy_id = $this->buy_model->add_buy($data_insert);
			if ($buy_id) {
				$this->session->set_flashdata('success', 'Закупка добавлена <a href="/buys/edit/' . $buy_id . '">Редактировать</a>');
				redirect('/buys/add');
			}
		}
		$data = array();
		$this->data['content'] = $this->load->view('buys/add', $data, true);

		$this->data['css'][] = 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css';
		$this->data['js'][] = 'moment/min/moment.min.js';
		$this->data['js'][] = 'moment/locale/ru.js';
		$this->data['js'][] = 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js';

		$this->data['javascript'] = "$(function () {
                $('.datetime-picker').datetimepicker({
                    locale: 'ru',
                    sideBySide:true,
                    format:'YYYY-MM-DD HH:mm'
                });
            });";


		$this->load->view('layout', $this->data);
	}

	/* редактирование закупки */
	function edit($id)
	{
		$buy = $this->buy_model->get_buy($id);
		$data['users'] = $this->buy_model->users_list();
		/* сохранение закупки */
		if ($this->input->post('save_buy')) {
			$this->load->library('form_validation');
			if ($this->form_validation->run() == TRUE) {
				$data_edit = array_map('trim', $this->input->post());
				unset($data_edit['save_buy']);
				$data_buy = array_diff_assoc($data_edit, get_object_vars($buy));
				if (count($data_buy)) {
					$saved = $this->buy_model->save($data_buy, $id);
					if ($saved) {
						/* напоминание в календарь */
						$this->add_event_to_calendar($buy->id);

						$this->session->set_flashdata('success', 'Закупка сохранена');
						/* пишем лог кто менял */
						$this->buy_model->log_buy($id, $data_buy, $buy);
						redirect('/buys/edit/' . $id);
					}
				} else {
					$data['warning'] = 'Даныне не менялись';
				}

			}
		}
		/* сохранение участников */
		if ($this->input->post('save_participants')) {
			unset($_POST['save_participants']);
			/* сохраняем то что есть */
			foreach ($_POST['name'] as $bp_id => $name) {
				$name = trim($name);
				if ($name) {
					$offer = isset($_POST['offer'][$bp_id]) ? $_POST['offer'][$bp_id] : 0;
					$data = array('name' => $name, 'offer' => $offer);
					$this->db->update('buy_participant', $data, array('id' => $bp_id));
				}
			}

			/* добавляем новых */
			if (isset($_POST['new_name'])) {
				foreach ($_POST['new_name'] as $i => $name) {
					$name = trim($name);
					if ($name) {
						$offer = isset($_POST['new_offer'][$i]) ? $_POST['new_offer'][$i] : 0;
						$data = array('name' => $name, 'offer' => $offer, 'buy_id' => $buy->id);
						$this->db->insert('buy_participant', $data);
					}
				}
			}

			/* Обновляем победителя */
			$winner = isset($_POST['winner']) ? $_POST['winner'] : 0;
			$data = array('winner' => $winner);
			$this->db->update('buys', $data, array('id' => $buy->id));

			redirect('/buys/edit/' . $buy->id . '#buy_participant');
		}

		/* Удаляем участника  */
		if (isset($_GET['delete_bp'])) {
			$bp_id = $_GET['delete_bp'];
			$this->db->delete('buy_participant', array('id' => $bp_id));
			if ($buy->winner == $bp_id) {
				$this->db->query("UPDATE buys SET winner = 0 WHERE id = $buy->id");
			}
			redirect('/buys/edit/' . $buy->id . '#buy_participant');
		}

		$data['partners'] = $this->buy_model->get_partners();
		$data['buy'] = $buy;
		$data['buy_log'] = $this->buy_model->get_log($buy->id);
		$data['participants'] = $this->buy_model->get_participants($buy->id);

		$this->data['h1'] = $this->data['title'] = 'Редактирование закупки #' . $buy->id;
		$this->data['content'] = $this->load->view('buys/edit', $data, true);

		$this->data['css'][] = 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css';
		$this->data['js'][] = 'moment/min/moment.min.js';
		$this->data['js'][] = 'moment/locale/ru.js';
		$this->data['js'][] = 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js';

		$this->data['javascript'] = "$(function () {
                $('.datetime-picker').datetimepicker({
                    locale: 'ru',
                    sideBySide:true,
                    format:'YYYY-MM-DD HH:mm'
                });
            });";

		$this->load->view('layout', $this->data);

	}

	public function delete($id, $confirm = 'no')
	{
		$buy = $buy = $this->buy_model->get_buy($id);
		if ($this->session->user->role != 1 || !$buy) redirect('/');
		if ($confirm == 'yes') {
			$this->buy_model->delete($id);
			delete_google_event($buy->event_bid, $buy->event_auc);
			redirect('/');
		}

		$data['buy'] = $buy;
		$this->data['h1'] = $this->data['title'] = 'Удалить закупку №' . $buy->id;
		$this->data['content'] = $this->load->view('buys/delete', $data, true);


		$this->load->view('layout', $this->data);
	}

	/* Удаленные закупки */
	public function deleted()
	{
		if ($this->session->user->role != 1) redirect('/');
		$this->data['h1'] = $this->data['title'] = 'Удаленные закупки';
		$data['buys'] = $this->buy_model->get_deleted();
		$this->data['content'] = $this->load->view('buys/deleted', $data, true);
		$this->load->view('layout', $this->data);
	}

	/* парсер закупок с госзакупок */
	public function find($reg_num)
	{

		$data = array('error' => '', 'success' => '');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36');
		/* ищем по номеру */
		$search_url = 'http://zakupki.gov.ru/epz/order/quicksearch/search.html?searchString=' . $reg_num;
		curl_setopt($ch, CURLOPT_URL, $search_url);
		$search_content = curl_exec($ch);
		$curl_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if ($curl_code == 200) {

			preg_match('#href="(.*?common-info.html\?regNumber=' . $reg_num . ')"#', $search_content, $match);

			if (isset($match[1])) {
				/* парсим инфу по закупке */
				$source_url = 'http://zakupki.gov.ru' . $match[1];
				curl_setopt($ch, CURLOPT_URL, $source_url);
				$content = curl_exec($ch);
				$curl_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

				if ($curl_code == 200) {

					preg_match('#Наименование объекта закупки.*?<td>(.*?)</td>#s', $content, $match);
					if (isset($match[1])) $data['buy_description'] = html_entity_decode(trim($match[1]));

					preg_match('#Наименование электронной площадки.*?<td>(.*?)</td>#s', $content, $match);
					if (isset($match[1])) $data['place'] = trim($match[1]);

					preg_match('#Адрес электронной площадки.*?<td>(.*?)</td>#s', $content, $match);
					if (isset($match[1])) $data['place_url'] = trim(strip_tags($match[1]));

					preg_match('#Размещение осуществляет.*?<td>(.*?)</td>#s', $content, $match);
					if (isset($match[1])) $data['customer'] = html_entity_decode(trim(str_replace('Заказчик', '', strip_tags($match[1]))));

					preg_match('#начала.*?подачи заявок.*?<td>(.*?)</td>#s', $content, $match);
					if (isset($match[1])) $data['bid_date_start'] = mysql_date(trim($match[1]), 'Y-m-d H:i');

					preg_match('#окончания.*?подачи заявок.*?<td>(.*?)</td>#s', $content, $match);
					if (isset($match[1])) $data['bid_date_end'] = mysql_date(trim($match[1]), 'Y-m-d H:i');

					preg_match('#Дата проведения аукциона.*?<td >(.*?)</td>#s', $content, $match);
					if (isset($match[1])) $data['auction_date'] = mysql_date(trim($match[1]), 'Y-m-d');

					preg_match('#Время проведения аукциона.*?<td >(.*?)</td>#s', $content, $match);
					if (isset($match[1])) $data['auction_time'] = trim($match[1]);
					if (isset($data['auction_time']) && strlen($data['auction_time']) == 5) {
						$data['auction_date'] .= ' ' . $data['auction_time'];
					}

					preg_match('#цена контракта.*?<td>(.*?)</td>#s', $content, $match);
					if (isset($match[1])) {
						$data['max_contract_price'] = trim($match[1]);
						$data['max_contract_price'] = str_replace(chr(194), '', $data['max_contract_price']);
						$data['max_contract_price'] = str_replace(chr(160), '', $data['max_contract_price']);
						$data['max_contract_price'] = floatval(str_replace(',', '.', $data['max_contract_price']));
					}

					preg_match('#Размер обеспечения заявок.*?<td>(.*?)</td>#s', $content, $match);
					if (isset($match[1])) {
						$data['bid_price'] = trim($match[1]);
						$data['bid_price'] = str_replace('&nbsp;Российский рубль', '', $data['bid_price']);
						$data['bid_price'] = str_replace(chr(194), '', $data['bid_price']);
						$data['bid_price'] = str_replace(chr(160), '', $data['bid_price']);
						$data['bid_price'] = floatval(str_replace(',', '.', $data['bid_price']));
					}

					preg_match('#Информация об объекте закупки.*?<table.*?(<table.*?Итого:.*?/table>)#s', $content, $match);
					if (isset($match[1])) {
						//$match[1] = preg_replace('/class=".*?"/', '', $match[1]);
						$match[1] = preg_replace('/<div.*?\/div>/', '', $match[1]);
						$data['items'] = base64_encode(trim($match[1]));
					}

					// закрытость аука
					preg_match('#Закрытый аукцион#', $content, $match);
					if (isset($match[0])) {
						$data['is_security'] = 'Закрытый акукцион';
					}


					if (isset($data['bid_date_end']) && isset($data['auction_date'])) {
						$data['buy_status'] = set_buy_status($data['bid_date_end'], $data['auction_date']);
					}

					$data['buy_url'] = $source_url;
					$data['reg_num'] = $reg_num;
					$data['success'] = 1;

				} else {
					$data['error'] = 'Ошибка запроса к ' . $source_url . ' код ' . $curl_code;
				}
			} else {
				$data['error'] = 'Закупка с номером ' . $reg_num . ' не найдена ';
			}

		} else {
			$data['error'] = 'Ошибка запроса к  ' . $search_url . ' код ' . $curl_code;
		}

		curl_close($ch);
		echo json_encode($data);

	}

	/* отображение календаря */
	public function calendar()
	{
		$this->data['h1'] = $this->data['title'] = 'Календарь';
		$this->data['content'] = $this->load->view('buys/calendar', array(), true);
		$this->load->view('layout', $this->data);
	}


	/* страница оператора со всеми закупками */
	public function operator()
	{
		$this->data['h1'] = $this->data['title'] = 'Расчет закупок';
		$data['buys'] = $this->buy_model->get_for_operators();
		$this->data['content'] = $this->load->view('buys/operator', $data, true);
		$this->load->view('layout', $this->data);
	}

	/* расчет стоимости закупки */
	public function calculate($buy_id)
	{

		$buy = $this->buy_model->get_buy($buy_id);
		if (!$buy->calculate_user) {
			$data = array(
				'calculate_user' => $this->session->user->id,
				'calculate_time' => date("Y-m-d H:i:s")
			);
			$this->buy_model->save($data, $buy_id);
			/* пишем лог кто менял */
			$this->buy_model->log_buy($buy_id, $data, $buy);
			$buy = $this->buy_model->get_buy($buy_id);
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('buy_price', 'Цена закупки', 'required');
		if ($this->form_validation->run() == TRUE) {
			$this->buy_model->save($this->input->post(), $buy_id);
			$this->session->set_flashdata('success', 'Закупка сохранена');
			/* пишем лог */
			$this->buy_model->log_buy($buy_id, $this->input->post(), $buy);
			$buy = $this->buy_model->get_buy($buy_id);
		}


		$this->data['h1'] = $this->data['title'] = 'Расчет закупки ID:' . $buy->id;
		$data['buy'] = $buy;
		$this->data['content'] = $this->load->view('buys/calculate', $data, true);
		$this->load->view('layout', $this->data);
	}


	/**
	 * @param $buy_id
	 * Добавление уведомления в календарь
	 */
	public function add_event_to_calendar($buy_id)
	{
		$buy = $this->buy_model->get_buy($buy_id);
		$now = date('Y-m-d H:i:s');
		/* напоминание о подачи заявки */
		if (!$buy->event_bid) {
			if ($buy->bid_date_end > $now && $buy->bid_status == 1) {
				$response = insert_google_event($buy, 1);
				if (isset($response->id)) {
					$this->db->update('buys', array('event_bid' => $response->id), array('id' => $buy->id));
				}
			}
		}

		/* напоминание о аукционе */
		if (!$buy->event_auc) {
			if ($buy->bid_status == 3) {
				$response2 = insert_google_event($buy, 2);
				delete_google_event($buy->event_bid, null);
			} elseif ($now < $buy->auction_date && ($buy->bid_status == 1 || $buy->bid_status == 4)) {
				$response2 = insert_google_event($buy, 2);
			}

			if (isset($response2) && isset($response2->id)) {
				$this->db->update('buys', array('event_auc' => $response2->id), array('id' => $buy->id));
			}
		}

		if ($buy->bid_status == 2) delete_google_event($buy->event_bid, $buy->event_auc);

	}

	/**
	 * партнеры
	 */
	public function partners()
	{
		/* добавление */
		if ($this->input->post('add')) {
			$partner = $this->input->post();
			unset($partner['add']);
			$this->buy_model->add_partner($partner);
		}
		/* удаление */
		if ($this->input->get('delete')) {
			$this->buy_model->delete_partner($this->input->get('delete'));
			redirect('/buys/partners');
		}
		/* редактирование */
		if ($this->input->get('edit')) {
			$partner_id = $this->input->get('edit');
			$data['partner'] = $this->buy_model->get_partner($partner_id);
			if ($this->input->post('save')) {
				$partner = $this->input->post();
				unset($partner['save']);
				$this->buy_model->save_partner($partner, $partner_id);
				redirect('/buys/partners');
			}
		}
		$this->data['h1'] = $this->data['title'] = 'От кого участвуем';
		$partners = $this->buy_model->get_partners();
		$data['partners'] = $partners;
		$this->data['content'] = $this->load->view('buys/partners', $data, true);
		$this->load->view('layout', $this->data);
	}

	/**
	 * Статистика
	 */
	public function statistic()
	{

		$this->data['h1'] = $this->data['title'] = 'Статистика по закупкам';

		$data['statistic'] = $this->buy_model->get_statistic();
		$this->data['content'] = $this->load->view('buys/statistic', $data, true);
		$this->data['js'][] = 'chartjs/Chart2.min.js';
		$this->data['js'][] = 'app/js/buy_statistic.js';
		$this->load->view('layout', $this->data);
	}

	public function search()
	{
		$str = isset($_GET['str']) ? trim($_GET['str']) : '';
		$this->data['h1'] = $this->data['title'] = 'Поиск';
		if (!empty($str)) {
			$this->data['h1'] = $this->data['title'] = 'Поиск  по ' . $str;
			$data['finds'] = $this->buy_model->find_buys($str);
		}

		$data['str'] = $str;
		$this->data['content'] = $this->load->view('buys/search', $data, true);
		$this->load->view('layout', $this->data);
	}


}
