<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * План
 */
class Plan extends CI_Controller
{

    public $data;

    public function __construct()
    {
        parent::__construct();
    }

    /* список заявок */
    public function index()
    {

        $selected_day = date("d-m-Y");
        $data = $this->_get_plan($selected_day);

        $this->data['h1'] = $this->data['title'] =  $data['title'] = 'План доставок ' . $selected_day;

        $data['selected_day'] = $selected_day;
        /* список доступных экспедиторов */
        $data['forwarders'] = $this->db->select('*')->from('users')->where('role', 3)->get()->result_array();

        /* печатная версия  */
        if(isset($_GET['print'])){
            $this->load->view('plan/index_print', $data);
        } else {
            $this->data['content'] = $this->load->view('plan/index', $data, true);
            $this->data['js'][] = 'app/js/map.js';
            $this->load->view('layout', $this->data);
        }

    }

    /* архив заявок */
    public function arhiv($selected_day)
    {
        $data = $this->_get_plan($selected_day);
        $this->data['h1'] = $this->data['title'] = $data['title']  ='План доставок на ' . $selected_day;

        $data['selected_day'] = $selected_day;

        /* список доступных экспедиторов */
        $data['forwarders'] = $this->db->select('*')->from('users')->where('role', 3)->get()->result_array();

        /* печатная версия  */
        if(isset($_GET['print'])){
            $this->load->view('plan/index_print', $data);

        } else {
            $this->data['content'] = $this->load->view('plan/index', $data, true);
            $this->data['js'][] = 'app/js/map.js';
            $this->load->view('layout', $this->data);
        }

    }

    /* маршрут на карте */
    public function map()
    {
        $this->data['h1'] = $this->data['title'] = 'На карте';
        $this->data['content'] = $this->load->view('plan/map', array(), true);
        $this->load->view('layout', $this->data);
    }

    /* план курьера */
    public function my($selected_day = '')
    {
        if(!$selected_day)  $selected_day = date("d-m-Y");
        $this->data['title'] = 'План доставок ' . $this->session->user->first_name . ' ' . $this->session->user->last_name;
        $this->data['h1'] = 'План доставок ' . $selected_day;
        $data['selected_day'] = $selected_day;
        $forwarder_id = $this->session->user->id;
        $date = mysql_date($selected_day);
        $sql = "SELECT
                  o.*,
                  c.name as contractor_name,
                  cc.contact as contractor_contact,
                  cc.contact_info,
                  cc.contact_map,
                  ca.address_name as contractor_address,
                  ca.latitude as contractor_latitude,
                  ca.longitude as contractor_longitude
                FROM orders o
                  LEFT JOIN contractor c ON o.contractor = c.id
                  LEFT JOIN contractor_contact cc ON o.contractor_contact = cc.id
                  LEFT JOIN contractor_address ca  ON cc.address_id = ca.address_id
                WHERE o.forwarder_id = $forwarder_id AND o.delivery_date = '$date' AND o.status = 3
                ORDER BY o.ord";

        //echo $sql;
        //var_dump($this->session->user);

        $query = $this->db->query($sql);
        $data['orders'] = array();
        foreach ($query->result_array() as $row) {
            $data['orders'][] = $row;
        }

        $this->data['content'] = $this->load->view('plan/my', $data, true);

        $this->load->view('layout', $this->data);
    }

    public function order($id) {

        settype($id, 'int');
        $forwarder_id = $this->session->user->id;


        $this->data['h1'] = $this->data['title'] = 'Заявка  № ' . $id;

        $sql = "SELECT
                  o.*,
                  c.name as contractor_name,
                  cc.contact as contractor_contact,
                  cc.contact_info,
                  cc.contact_map,
                  ca.address_name as contractor_address,
                  ca.latitude as contractor_latitude,
                  ca.longitude as contractor_longitude
                FROM orders o
                  LEFT JOIN contractor c ON o.contractor = c.id
                  LEFT JOIN contractor_contact cc ON o.contractor_contact = cc.id
                  LEFT JOIN contractor_address ca  ON cc.address_id = ca.address_id
                WHERE o.forwarder_id = $forwarder_id AND o.id = $id
                ORDER BY o.forwarder_id, o.ord";

        //echo $sql;

        $data['order'] = $this->db->query($sql)->row_array();
        if(!$data['order']) {
            redirect('/plan');
        }
        /* меняем статус */
        if(isset($_GET['status']) && ($_GET['status'] == 2 || $_GET['status'] == 4) ){
            $this->db->update('orders', array('status' => $_GET['status']), 'id = '.$id);
            redirect('/plan/order/'.$id);
        }

        $this->data['content'] = $this->load->view('plan/order', $data, true);
        $this->load->view('layout', $this->data);
    }


    public function _get_plan($date, $status = null)
    {
        $where = '';
        if($status){
            $where .= ' AND o.status =  '.$status;
        }
        $date = mysql_date($date);
        $sql = "SELECT
                  o.*,
                  c.name as contractor_name,
                  cc.contact as contractor_contact,
                  cc.contact_info,
                  cc.contact_map,
                  ca.address_name as contractor_address,
                  ca.latitude as contractor_latitude,
                  ca.longitude as contractor_longitude,
                  u.first_name as forwarder_fn,
                  u.last_name as forwarder_ln,
                  u.latitude as forwarder_latitude,
                  u.longitude as forwarder_longitude,
                  u.auto_name,
                  u.auto_number,
                  u.photo as user_photo,
				  a.first_name as author_first_name,
       			  a.last_name as author_last_name
                FROM orders o
                  LEFT JOIN contractor c ON o.contractor = c.id
                  LEFT JOIN contractor_contact cc ON o.contractor_contact = cc.id
                  LEFT JOIN contractor_address ca  ON cc.address_id = ca.address_id
                  LEFT OUTER JOIN users u ON o.forwarder_id = u.id
				  LEFT OUTER JOIN users a ON o.author_id = a.id
                WHERE o.forwarder_id > 0  AND o.delivery_date = '$date' $where
                ORDER BY o.forwarder_id, o.ord";

        //echo $sql;

        $query = $this->db->query($sql);
        $data['plan'] = array();
        foreach ($query->result_array() as $row) {
            $data['plan'][$row['forwarder_id']]['name'] = $row['forwarder_fn'] . ' ' . $row['forwarder_ln'];
            $data['plan'][$row['forwarder_id']]['auto_name'] = $row['auto_name'];
            $data['plan'][$row['forwarder_id']]['auto_number'] = $row['auto_number'];
            $data['plan'][$row['forwarder_id']]['user_photo'] = $row['user_photo'];
            $data['plan'][$row['forwarder_id']]['orders'][] = $row;

            $point = [
            	'coords' => ['lat' => $row['contractor_latitude'], 'long' => $row['contractor_longitude']],
				'contractor_address' => $row['contractor_address'],
				'contractor_name' => $row['contractor_name'],
				'contractor_contact' => $row['contractor_contact'],
			];

            $data['map_points'][] = $point;
        }

        return $data;
    }


}
