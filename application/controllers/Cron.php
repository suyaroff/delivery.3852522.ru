<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Cron extends CI_Controller
{

	public $data;

	public function __construct()
	{
		parent::__construct();
	}


	public function index()
	{

	}

	/**
	 * Проверка победителей аукциона
	 */
	public function auction_end()
	{
		$now = date("Y-m-d H:i");
		$sql = "SELECT id, reg_number, buy_url, buy_status FROM buys WHERE buy_status = 3 AND trash = 0 AND winner = 0 AND auction_date < '$now'";
		$query = $this->db->query($sql);
		foreach ($query->result() as $b) {


			$url = str_replace('common-info', 'supplier-results', $b->buy_url);
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36');
			$content = curl_exec($ch);
			$curl_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			// парсим
			if ($curl_code == 200) {
				preg_match_all('#<td.*?td>#s', $content, $match);
				$row_start = 0;
				$participant = array();
				foreach ($match[0] as $m) {
					$m = trim(strip_tags($m));
					if ($m == 'Информация о контракте') $row_start = 0;
					if (strstr($m, 'Победитель')) $row_start = 1;
					if ($row_start && strlen($m) > 30) {
						if (strstr($m, ')')) $name = $m;
						if (strstr($m, 'рубль')) $participant[$name] = str_replace(',', '.', trim(str_replace('Российский рубль', '', $m)));
					} else {
						continue;
					}
				}
				// добавляем в базу
				if (count($participant)) {
					foreach ($participant as $name => $offer) {
						$offer = str_replace(chr(194), '', $offer);
						$offer = str_replace(chr(160), '', $offer);
						$bp = $this->db->query("SELECT * FROM buy_participant WHERE buy_id = $b->id AND name = '$name'")->row();
						if (!$bp) {
							$data = array('name' => $name, 'buy_id' => $b->id, 'offer' => $offer);
							$this->db->insert('buy_participant', $data);
							if (strstr($name, 'Победитель')) {
								$winner = $this->db->insert_id();
								$this->db->query("UPDATE buys SET winner = $winner WHERE id = $b->id");
							}
						}
					}
				}

			}
		}
	}

	public function buys_check_date()
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36');
		$now = date("Y-m-d H:i");
		$sql = "SELECT id, bid_date_end, auction_date, bid_status, buy_url 
				FROM buys 
				WHERE bid_status IN (0, 1, 3) AND (bid_date_end > '$now' OR bid_date_end = '0000-00-00 00:00:00' OR auction_date = '0000-00-00 00:00:00')";
		$query = $this->db->query($sql);

		foreach ($query->result() as $b) {
			if(strpos($b->buy_url, '/223/')) continue ;
			curl_setopt($ch, CURLOPT_URL, $b->buy_url);
			$content = curl_exec($ch);
			$curl_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$data = array();
			if ($curl_code == 200) {
				preg_match('#окончания.*?подачи заявок.*?<td>(.*?)</td>#s', $content, $match);
				if (isset($match[1])) {
					$match[1] = strip_tags($match[1]);
					$data['bid_date_end'] = mysql_date(trim($match[1]), 'Y-m-d H:i');
					if (substr($data['bid_date_end'], 0, 4) == '1970') unset($data['bid_date_end']);
				}

				preg_match('#Дата проведения аукциона.*?<td >(.*?)</td>#s', $content, $match);
				if (isset($match[1])) $data['auction_date'] = mysql_date(trim($match[1]), 'Y-m-d');

				preg_match('#Время проведения аукциона.*?<td >(.*?)</td>#s', $content, $match);
				if (isset($match[1])) $data['auction_time'] = trim($match[1]);
				if (isset($data['auction_time']) && strlen($data['auction_time']) == 5) {
					$data['auction_date'] .= ' ' . $data['auction_time'];
				}

				$update['check_datetime'] = date("Y-m-d H:i");

				if (isset($data['bid_date_end'])) {
					$diff_bid = abs(strtotime($data['bid_date_end']) - strtotime($b->bid_date_end));
					if ($diff_bid) {
						$update['bid_date_change'] = 1;
						$update['bid_date_end'] = $data['bid_date_end'];
					}
				}

				if (isset($data['auction_date'])) {
					$diff_auc = abs(strtotime($data['auction_date']) - strtotime($b->auction_date));
					if ($diff_auc) {
						$update['auction_date_change'] = 1;
						$update['auction_date'] = $data['auction_date'];
					}
				}

				$this->db->update('buys', $update, array('id' => $b->id));
			}
		}
		curl_close($ch);
	}


}
