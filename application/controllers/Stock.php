<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Склад
 */
class Stock extends CI_Controller
{

	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('order_model');
	}

	/* список заявок */
	public function index()
	{

		$this->data['h1'] = $this->data['title'] = 'Склад - все сборки';
		$where = ' o.operation = 1 ';
		/* фильтр доставки */
		if (isset($_GET['date']) && !empty($_GET['date'])) {
			$data['current_date'] = $_GET['date'];
			$current_date_mysql = mysql_date($data['current_date']);
			$where .= " AND o.delivery_date = '$current_date_mysql' ";
			$this->data['h1'] = $this->data['title'] = 'Склад - сборки на ' . $data['current_date'];
		} else {
			$data['current_date'] = date("d-m-Y");
		}
		/* Статус доставки */
		$data['status'] = 1;
		if (isset($_GET['status']) && $_GET['status'] != '') {
			settype($_GET['status'], 'int');
			if ($_GET['status'] >= 0) {
				$data['status'] = $_GET['status'];
				$where .= " AND o.status = " . $data['status'] . " ";
			}
		} else {
			$where .= " AND o.status = " . $data['status'] . " ";
		}

		/* Статус склада */
		if (isset($_GET['stock_status']) && $_GET['stock_status'] != '') {
			settype($_GET['stock_status'], 'int');
			if ($_GET['stock_status'] >= 0) {
				$data['stock_status'] = $_GET['stock_status'];
				$where .= " AND o.stock_status = " . $data['stock_status'] . " ";
			}
		}

		/*Контрагент*/
		if (isset($_GET['contractor']) && $_GET['contractor'] != '') {
			settype($_GET['contractor'], 'int');
			if ($_GET['contractor'] >= 0) {
				$data['contractor'] = $_GET['contractor'];
				$where .= " AND o.contractor = " . $data['contractor'] . " ";
			}
		}

		if (isset($_GET['invoice_contractor']) && $_GET['invoice_contractor'] != '') {
			$invoice_contractor = $_GET['invoice_contractor'];
			$where .= " AND o.invoice_contractor LIKE '%$invoice_contractor%' ";
		}


		/* заявки */
		$data['orders'] = [];
		$sql = "SELECT
                  o.*,
                  c.name as contractor_name ,
                  u.first_name as forwarder_fn ,
                  u.last_name as forwarder_ln,
       			  a.first_name as author_first_name,
       			  a.last_name as author_last_name
                FROM orders o
                LEFT OUTER JOIN contractor c ON o.contractor = c.id
                LEFT OUTER JOIN users u ON o.forwarder_id = u.id
				LEFT OUTER JOIN users a ON o.author_id = a.id
                WHERE $where ORDER BY delivery_date, stock_status, o.id DESC LIMIT 100";
		$query = $this->db->query($sql);
		foreach ($query->result_array() as $r) {
			$r['files'] = $this->db->get_where('order_files', ['order_id' => $r['id']])->result_array();
			$data['orders'][] = $r;
		}

		$data['contractors'] = $this->db->order_by('name')->get('contractor')->result_array();

		if (isset($_GET['print'])) {
			$data['title'] = 'Склад - сборки на ' . $data['current_date'];
			$this->load->view('stock/index_print', $data);
		} else {
			$this->data['content'] = $this->load->view('stock/index', $data, true);
			$this->load->view('layout', $this->data);
		}
	}

	public function edit($id)
	{
		$this->data['order'] = $this->order_model->get($id);
		$this->load->view('stock/edit', $this->data);
	}


	public function update($id)
	{
		$this->load->library('email');

		$order = $this->order_model->get($id);

		$new_data['note_stock'] = $_POST['note_stock'];
		$new_data['stock_status'] = $_POST['stock_status'];
		$new_data['place'] = $_POST['place'];
		$new_data['weight'] = $_POST['weight'];
		$new_data['volume'] = $_POST['volume'];
		/* переводим в статус в плане если сборка готова */
		if ($order->status == 1 && $new_data['stock_status'] == 2 || $new_data['stock_status'] == 3) {
			$new_data['status'] = 3;
		}
		$update = $this->order_model->update($new_data, $id);

		$initiator = $order->author_first_name . ' ' . $order->author_last_name;
		$message = $this->load->view('stock/email', ['order' => $this->order_model->get($id)], true);

		/* отправялем письмо если статус недостача*/
		if (strstr($_SERVER['SERVER_NAME'], '.loc')) return;
		if ($_POST['stock_status'] == 3 && $_POST['stock_status'] != $_POST['old']['stock_status']) {
			$this->email->from('1@3852522.ru');
			$this->email->to('office@rus-office.net');
			//$this->email->bcc('suyaroff@gmail.com');
			$subject = "Уведомление из склада для ".$initiator . '. Недостача на складе. Сборка №' . $id;
			$this->email->subject($subject);
			$this->email->message("Недостача на складе. Сборка <a href='http://delivery.3852522.ru/order/view/$id'>№$id</a><br>" . $message);
			$this->email->send();
			//echo $this->email->print_debugger();
		}
		/* отправляем сообщение если готово */
		if ($_POST['stock_status'] == 2 && $_POST['stock_status'] != $_POST['old']['stock_status']) {
			$this->email->from('1@3852522.ru');
			$this->email->to('office@rus-office.net');
			//$this->email->bcc('suyaroff@gmail.com');
			$subject = "Уведомление из склада для ".$initiator." Сборка №$id готова";
			$this->email->subject($subject);
			$this->email->message("Сборка <a href='http://delivery.3852522.ru/order/view/$id'>№$id</a> готова<br>" . $message);
			$this->email->send();
			//echo $this->email->print_debugger();
		}

	}
}
