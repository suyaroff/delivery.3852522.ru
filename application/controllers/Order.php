<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Главная
 */
class Order extends CI_Controller
{

	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('order_model');
	}

	/* Главная страница список заявок */
	public function index()
	{
		// переадресация на закупки
		if (SYSTEM_NAME == 'buys') redirect('/buys');
		if ($this->session->user->role == 3) redirect('/plan/my');
		if ($this->session->user->role == 4) redirect('/stock');
		if ($this->session->user->role == 5) redirect('http://www.zakupki.3852522.ru/buys/operator');

		$this->data['h1'] = $this->data['title'] = 'Общий список  доставок';
		$data['contractors'] = $this->db->order_by('name')->get('contractor')->result_array();
		$data['users'] = $this->db->order_by('first_name')->get('users')->result_array();
		$data['orders'] = $this->order_model->get_index();

		$this->data['content'] = $this->load->view('order/index', $data, true);
		$this->load->view('layout', $this->data);
	}


	/* Добавление заявки */
	public function add()
	{
		$this->data['h1'] = $this->data['title'] = 'Добавить доставку';
		$data['success'] = '';

		/*Добавление*/
		$this->load->library('form_validation');
		$this->form_validation->set_rules('contractor', 'Контрагент', 'greater_than[0]', array('greater_than' => 'Выберите контрагента'));

		if ($this->form_validation->run() == TRUE) {
			$invoises = $_POST['invoice'];
			unset($_POST['invoice']);
			$order = $_POST;

			foreach ($invoises as $invoice) {
				$order['invoice'] = $invoice;
				$insert_id = $this->order_model->add($order);
			}
			redirect('/');
		}

		/* список доступных экспедиторов */
		$data['forwarders'] = $this->db->select('*')->from('users')->where('role', 3)->where('activity_status', 1)->get()->result_array();
		/* список контрагентов */
		$this->db->order_by('name');
		$data['contractors'] = $this->db->get('contractor')->result_array();
		/* список контактов */
		$data['contractor_contact'] = array();
		if (isset($_POST['contractor'])) {
			$contractor_id = (int)$_POST['contractor'];
			$sql = "SELECT * 
                FROM contractor_address ca 
                LEFT JOIN contractor_contact cc ON cc.address_id = ca.address_id 
                WHERE ca.contractor_id = $contractor_id";
			$data['contractor_contact'] = $this->db->query($sql)->result_array();
		}

		$this->data['content'] = $this->load->view('order/add', $data, true);
		$this->load->view('layout', $this->data);
	}

	/**
	 * Форма редактирования
	 * @param $id
	 */
	public function edit($id)
	{

		$this->data['order'] = $this->order_model->get($id);
		/* список доступных экспедиторов */
		$this->data['forwarders'] = $this->db->select('*')->from('users')->where('role', 3)->where('activity_status', 1)->get()->result_array();
		/* список контрагентов */
		$this->db->order_by('name');
		$this->data['contractors'] = $this->db->get('contractor')->result_array();
		/* список контактов  */
		$sql = "SELECT * 
                FROM contractor_address ca 
                LEFT JOIN contractor_contact cc ON cc.address_id = ca.address_id 
                WHERE ca.contractor_id = " . $this->data['order']->contractor;
		$this->data['contractor_contact'] = $this->db->query($sql)->result_array();

		$this->load->view('order/edit', $this->data);
	}

	/* Удаление заявки */
	public function delete($id, $confirm = 'no')
	{
		if ($confirm == 'no') {
			$this->load->view('order/delete', array('id' => $id));
		}
		if ($confirm == 'yes') {
			$this->order_model->delete($id);
		}

	}

	/**
	 * Сохранение заявки
	 * @param $id
	 */
	public function ajax_change($id)
	{

		$order = $_POST;
		$update = $this->order_model->update($order, $id);
		/* добавляем фаил */
		if (isset($_FILES['doc'])) {
			$this->order_model->add_file($_FILES['doc'], $id);
		}
	}

	/**
	 * Данные по заявке
	 * @param $id
	 */
	public function ajax_get($id)
	{
		$order = $this->db->get_where('orders', array('id' => $id))->row();
		$this->output->set_content_type('application/json')->set_output(json_encode($order));
	}

	/** Просмотр заявки
	 * @param $id
	 */
	public function view($id)
	{
		$order = $this->order_model->get($id);
		$this->data['h1'] = $this->data['title'] = 'Сборка №' . $order->id;
		$data['order'] = $order;
		$data['logs'] = $this->order_model->order_log_get($id);
		$this->data['content'] = $this->load->view('order/view', $data, true);
		$this->load->view('layout', $this->data);
	}

	/**
	 * История заявок
	 */
	public function history()
	{
		$this->data['h1'] = $this->data['title'] = 'История';
		$data['contractors'] = $this->db->order_by('name')->get('contractor')->result_array();
		$data['users'] = $this->db->order_by('first_name')->get('users')->result_array();

		/* заявки */
		$data['orders'] = [];
		if (isset($_GET['action']) && $_GET['action'] == 'filter') {

			$where = ' 1 = 1 ';
			/* фильтр  по дате доставки */
			if (isset($_GET['date_from']) && !empty($_GET['date_from'])) {
				$date_from = $_GET['date_from'];
				$date_mysql = mysql_date($date_from);
				$where .= " AND o.delivery_date >= '$date_mysql' ";
			}
			if (isset($_GET['date_to']) && !empty($_GET['date_to'])) {
				$date_to = $_GET['date_from'];
				$date_to_mysql = mysql_date($date_to);
				$where .= " AND o.delivery_date <= '$date_to_mysql' ";
			}
			/* Дата создания */
			if (isset($_GET['date_created_from']) && !empty($_GET['date_created_from'])) {
				$date_created_from = $_GET['date_created_from'];
				$date_created_from_mysql = mysql_date($date_created_from);
				$where .= " AND o.date_created >= '$date_created_from_mysql' ";
			}
			if (isset($_GET['date_created_to']) && !empty($_GET['date_created_to'])) {
				$date_created_to = $_GET['date_created_to'];
				$date_created_to_mysql = mysql_date($date_created_to) . ' 23:59:59';
				$where .= " AND o.date_created <= '$date_created_to_mysql' ";
			}
			/* Дата сборки */
			if (isset($_GET['date_stock_done_from']) && !empty($_GET['date_stock_done_from'])) {
				$date_stock_done_from = $_GET['date_stock_done_from'];
				$date_stock_done_from_mysql = mysql_date($date_stock_done_from);
				$where .= " AND o.date_stock_done >= '$date_stock_done_from_mysql' ";
			}
			if (isset($_GET['date_stock_done_to']) && !empty($_GET['date_stock_done_to'])) {
				$date_stock_done_to = $_GET['date_stock_done_to'];
				$date_stock_done_to_mysql = mysql_date($date_stock_done_to) . ' 23:59:59';
				$where .= " AND o.date_created <= '$date_stock_done_to_mysql' ";
			}
			/* Статус*/
			if (isset($_GET['status']) && $_GET['status'] != '') {
				settype($_GET['status'], 'int');
				if ($_GET['status'] >= 0) {
					$status = $_GET['status'];
					$where .= " AND o.status = $status ";
				}
			}
			/* Статус склада */
			if (isset($_GET['stock_status']) && $_GET['stock_status'] != '') {
				settype($_GET['stock_status'], 'int');
				if ($_GET['stock_status'] >= 0) {
					$stock_status = $_GET['stock_status'];
					$where .= " AND o.stock_status = $stock_status ";
				}
			}

			/* Контрагент */
			if (isset($_GET['contractor']) && $_GET['contractor'] != '') {
				settype($_GET['contractor'], 'int');
				if ($_GET['contractor'] >= 0) {
					$contractor = $_GET['contractor'];
					$where .= " AND o.contractor = $contractor ";
				}
			}
			/* Инициатор */
			if (isset($_GET['author_id']) && $_GET['author_id'] != '') {
				settype($_GET['author_id'], 'int');
				if ($_GET['author_id'] > 0) {
					$author_id = $_GET['author_id'];
					$where .= " AND o.author_id = $author_id ";
				}
			}
			/* Накладная */
			if (isset($_GET['invoice']) && $_GET['invoice'] != '') {
				$invoice = trim($_GET['invoice']);
				if ($invoice) {
					$where .= " AND o.invoice LIKE '%$invoice%' ";
				}
			}
			/* Номер счета */
			if (isset($_GET['invoice_contractor']) && $_GET['invoice_contractor'] != '') {
				$invoice_contractor = trim($_GET['invoice_contractor']);
				if ($invoice_contractor) {
					$where .= " AND o.invoice_contractor LIKE '%$invoice_contractor%' ";
				}
			}



			$sql = "SELECT
                  o.*,
                  u.first_name as forwarder_first_name,
                  u.last_name as forwarder_last_name,
                  u.phone as forwarder_phone,
                  u.auto_name as forwarder_auto_name,
                  u.auto_number as forwarder_auto_number,
       			  a.first_name as author_first_name,
       			  a.last_name as author_last_name,
                  c.name as contractor_name,
                  c.type as contractor_type,
                  cc.contact as contact_name, 
                  cc.contact_info ,
                  cc.contact_map,
                  ca.address_name
                FROM orders o
                LEFT OUTER JOIN users u ON o.forwarder_id = u.id
			  	LEFT OUTER JOIN users a ON o.author_id = a.id
                LEFT OUTER JOIN contractor c ON o.contractor = c.id
                LEFT OUTER JOIN contractor_contact cc ON o.contractor_contact = cc.id
                LEFT OUTER JOIN contractor_address ca ON cc.address_id = ca.address_id
                WHERE $where
				ORDER BY o.id desc LIMIT 0, 100";
			$query = $this->db->query($sql);
			foreach ($query->result_array() as $r) {
				$r['files'] = $this->db->get_where('order_files', ['order_id' => $r['id']])->result_array();
				$data['orders'][] = $r;
			}
		}

		$this->data['content'] = $this->load->view('order/history', $data, true);
		$this->load->view('layout', $this->data);

	}


}
