<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Пользователи
 */
class User extends CI_Controller
{
    public $data;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');

        $this->data['h1'] = 'Пользователи';
        $this->data['title'] = 'Пользователи';
        $this->data['content'] = '';
    }

    /**
     * Список пользователей
     */
    public function index()
    {
        $this->data['h1'] = $this->data['title'] = 'Список пользователей';
        $users = $this->db->where('id <>', $this->session->user->id)->order_by('first_name, last_name, role')->get('users')->result();
        $this->data['content'] = $this->load->view('user/index', array('users' => $users), true);
        $this->load->view('layout', $this->data);
    }

    /**
     * Добавляем пользователя
     */
    public function add()
    {

        $data['success'] = '';
        if ($this->form_validation->run() == TRUE) {

            $user_data['login'] = $this->user_model->set_login($this->input->post('login'));
            $user_data['first_name'] = $this->input->post('first_name');
            $user_data['last_name'] = $this->input->post('last_name');
            $user_data['email'] = $this->input->post('email');
            $user_data['phone'] = $this->input->post('phone');
            $user_data['role'] = $this->input->post('role');
            $user_data['registration_date'] = date('Y-m-d');

            if ($this->input->post('password')) {
                $user_data['password'] = $this->user_model->hash_password($this->input->post('password'));
            }

            /* Координаты */
            $user_data['latitude'] = str_replace(',','.', $this->input->post('latitude'));
            $user_data['longitude'] = str_replace(',','.', $this->input->post('longitude'));

            $insert = $this->db->insert('users', $user_data);
            if ($insert) {
                $data['success'] = 'Пользователь добавлен';
            }
        }

        $this->data['h1'] = $this->data['title'] = 'Добавление пользователя';
        $this->data['content'] = $this->load->view('user/add', $data, true);
        $this->load->view('layout', $this->data);
    }

    /**
     * Редактируем пользователя
     */
    public function edit($id)
    {
    	$data['success'] = '';
        $this->config->load('form_validation');
		$user = $this->db->get_where('users', array('id' => $id))->row();
        if ($this->form_validation->run() == TRUE) {
            $new_login = $this->user_model->set_login($this->input->post('login'));
			if($user->login != $new_login){
				$user_data['login'] = $new_login;
			}
            $user_data['first_name'] = $this->input->post('first_name');
            $user_data['last_name'] = $this->input->post('last_name');
            $user_data['email'] = $this->input->post('email');
            $user_data['phone'] = $this->input->post('phone');
            $user_data['role'] = $this->input->post('role');

            if ($this->input->post('password')) {
                $user_data['password'] = $this->user_model->hash_password($this->input->post('password'));
            }

            /* Координаты */
            $user_data['latitude'] = str_replace(',','.', $this->input->post('latitude'));
            $user_data['longitude'] = str_replace(',','.', $this->input->post('longitude'));


            $update = $this->db->update('users', $user_data, array('id' => $id));
            if ($update) {
                $data['success'] = 'Пользователь сохранен';
            }
        }

        $user = $this->db->get_where('users', array('id' => $id))->row();
        if(!$user) redirect('/user');
        $data['user'] = $user;
        $this->data['h1'] = $this->data['title'] = 'Редактрование пользователя';
        $this->data['content'] = $this->load->view('user/edit', $data, true);
        $this->load->view('layout', $this->data);
    }

    public function delete($id)
    {
        if($this->session->user->role == 1 AND $id != $this->session->user->id) {
            $this->db->delete('users', array('id' => $id));
            redirect('/user');
        }
    }

    /**
     * Редактируем свой профиль
     */
    public function profile()
    {

        $this->data['h1'] = $this->data['title'] = 'Мои настройки';

        if ($this->form_validation->run() == TRUE) {

            $user_data['login'] = $this->input->post('login');
            $user_data['login'] = $this->user_model->set_login($user_data['login']);

            $user_data['first_name'] = $this->input->post('first_name');
            $user_data['last_name'] = $this->input->post('last_name');
            $user_data['email'] = $this->input->post('email');
            $user_data['phone'] = $this->input->post('phone');
            if ($this->input->post('password')) {
                $user_data['password'] = $this->user_model->hash_password($this->input->post('password'));
            }



            $update = $this->db->update('users', $user_data, array('id' => $this->session->user->id));

            /* Загрузка авы*/
            if ($_FILES['photo']['size'] > 0 && $_FILES['photo']['error'] == 0) {
                $photo = 'ava_' . $this->session->user->id . '.jpg';
                $upload = upload_photo($_FILES['photo']['tmp_name'], $photo, $this->config->item('upload_dir'), 200, 200);
                if ($upload) {
                    $this->db->update('users', array('photo' => $photo), array('id' => $this->session->user->id));
                }
            }


            /*Обновляем в сессии*/
            $this->session->user = $this->db->get_where('users', array('id' => $this->session->user->id))->row();

        }

        $profile = $this->db->get_where('users', array('id' => $this->session->user->id))->row_array();

        $this->data['content'] = $this->load->view('user/profile', $profile, true);

        $this->load->view('layout', $this->data);
    }


}
