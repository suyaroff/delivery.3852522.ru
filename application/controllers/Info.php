<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Справочники
 */
class Info extends CI_Controller
{
	public $data;

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');

		$this->data['h1'] = 'Справочники';
		$this->data['title'] = 'Справочники';

		$this->data['content'] = '';
	}


	public function index()
	{

	}

	public function config()
	{
		$conf['contractor_type'] = $this->config->item('contractor_type');
		$conf['orders_status'] = $this->config->item('orders_status');
		echo json_encode($conf);
	}


	/* список контрагентов */
	public function contractor_list($api = '')
	{
		$this->load->model('contractor_model');
		$data = [];
		if ($api) {
			$data['contractors'] = $this->contractor_model->get_list();
			$this->output->set_content_type('application/json')->set_output(json_encode($data));

		} else {
			$this->data['h1'] = $this->data['title'] = 'Контрагенты';
			$this->data['content'] = $this->load->view('info/contractor_list', $data, true);
			$this->load->view('layout', $this->data);
		}
	}

	public function ajax_contractor_contact($contractor_id)
	{
		settype($contractor_id, 'int');
		$sql = "SELECT * 
                FROM contractor_address ca 
                LEFT JOIN contractor_contact cc ON cc.address_id = ca.address_id 
                WHERE ca.contractor_id = $contractor_id";
		$result = $this->db->query($sql)->result();
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	 * Добавление контрагента
	 */
	public function contractor_add()
	{

		$data['success'] = '';
		$this->form_validation->set_rules('name', 'Название', 'required');
		$this->form_validation->set_rules('address', 'Адрес', 'required');
		$this->form_validation->set_rules('contact', 'Контактное лицо', 'required');
		if ($this->form_validation->run() == TRUE) {
			$this->db->insert('contractor', array(
				'name' => $this->input->post('name'),
				'type' => $this->input->post('type')
			));
			$contractor_id = $this->db->insert_id();
			if ($contractor_id) {
				$this->db->insert('contractor_address', array(
					'address_name' => $this->input->post('address'),
					'latitude' => $this->input->post('latitude'),
					'longitude' => $this->input->post('longitude'),
					'contractor_id' => $contractor_id
				));
				$address_id = $this->db->insert_id();
				if ($address_id) {
					$this->db->insert('contractor_contact', array(
						'contact' => $this->input->post('contact'),
						'address_id' => $address_id,
						'contact_info' => $this->input->post('contact_info'),
						'contact_map' => $this->input->post('contact_map')
					));
				}
				redirect('/info/contractor_edit/' . $contractor_id);
				$data['success'] = 'Контрагент добавлен';
			}
		}
		$this->data['h1'] = $this->data['title'] = 'Добавление контрагента';
		$this->data['content'] = $this->load->view('info/contractor_add', $data, true);
		$this->load->view('layout', $this->data);
	}


	/**
	 * Редактирование контрагента
	 * @param $id
	 */
	public function contractor_edit($id)
	{
		$data['success'] = '';
		$data['id'] = $id;
		$this->data['h1'] = $this->data['title'] = 'Редактирование контрагента';
		$this->data['content'] = $this->load->view('info/contractor_edit', $data, true);
		$this->load->view('layout', $this->data);
	}

	/**
	 * ИНформация по контрагенту
	 * @param $id
	 */
	public function contractor($id)
	{
		$this->load->model('contractor_model');
		$contractor = $this->contractor_model->get_full($id);
		echo json_encode($contractor);
	}

	/**
	 * Сохранение контрагента
	 */
	public function contractor_save()
	{
		$data = ['error' => '', 'success' => ''];
		if (isset($_POST['id'])) {
			if (isset($_POST['name'])) $this->db->set('name', $_POST['name']);
			if (isset($_POST['type'])) $this->db->set('type', $_POST['type']);
			$this->db->where('id', $_POST['id']);
			$result = $this->db->update('contractor');
			if ($result) {
				$data['success'] = 'updated';
			} else {
				$data['error'] = 'not updated';
			}
		}
		echo json_encode($data);
	}

	/**
	 * Сохранение адреса
	 */
	public function address_save()
	{
		$data = ['error' => '', 'success' => ''];
		if (isset($_POST['address_id'])) {
			if (isset($_POST['address_name'])) $this->db->set('address_name', $_POST['address_name']);
			if (isset($_POST['latitude'])) $this->db->set('latitude', $_POST['latitude']);
			if (isset($_POST['longitude'])) $this->db->set('longitude', $_POST['longitude']);
			$this->db->where('address_id', $_POST['address_id']);
			$result = $this->db->update('contractor_address');
			if ($result) {
				$data['success'] = 'updated';
			} else {
				$data['error'] = 'not updated';
			}
		}
		echo json_encode($data);
	}

	/**
	 * Добавление адреса
	 */
	public function address_add()
	{
		$data = ['error' => '', 'success' => ''];
		if (isset($_POST['name']) && isset($_POST['contractor_id']) && isset($_POST['latitude'])  && isset($_POST['longitude']) ) {
			$this->db->set('address_name', $_POST['name']);
			$this->db->set('contractor_id', $_POST['contractor_id']);
			$this->db->set('latitude', $_POST['latitude']);
			$this->db->set('longitude', $_POST['longitude']);
			$result = $this->db->insert('contractor_address');
			if ($result) {
				$data['success'] = 'inserted';
			} else {
				$data['error'] = 'not updated';
			}
		}
		echo json_encode($data);
	}

	/**
	 * Удаление адреса
	 */
	public function address_delete() {
		$id = $_POST['id'];
		$data = ['error' => '', 'success' => ''];
		$deleted = $this->db->where('address_id', $id)->delete('contractor_address');
		if($deleted){
			$this->db->where('address_id', $id)->delete('contractor_contact');
			$data['success'] = 'deleted';
		} else {
			$data['error'] = 'not deleted';
		}
		echo json_encode($data);
	}

	public function contractor_contact_save() {
		$id = $_POST['id'];
		$data = ['error' => '', 'success' => ''];

		$this->db->set('contact', $_POST['contact']);
		$this->db->set('address_id', $_POST['address_id']);
		$this->db->set('contact_info', $_POST['contact_info']);
		$this->db->set('contact_map', $_POST['contact_map']);
		$this->db->where('id', $id);

		$result = $this->db->update('contractor_contact');

		if ($result) {
			$data['success'] = 'updated';
		} else {
			$data['error'] = 'not updated';
		}
		echo json_encode($data);
	}

	public function contractor_contact_add() {

		$data = ['error' => '', 'success' => ''];

		$this->db->set('contact', $_POST['contact']);
		$this->db->set('address_id', $_POST['address_id']);
		$this->db->set('contact_info', $_POST['contact_info']);
		$this->db->set('contact_map', $_POST['contact_map']);

		$result = $this->db->insert('contractor_contact');

		if ($result) {
			$data['success'] = 'inserted';
		} else {
			$data['error'] = 'not inserted';
		}
		echo json_encode($data);
	}
	public function contractor_contact_delete() {
		$id = $_POST['id'];
		$data = ['error' => '', 'success' => ''];
		$deleted = $this->db->where('id', $id)->delete('contractor_contact');
		if($deleted){
			$data['success'] = 'deleted';
		} else {
			$data['error'] = 'not deleted';
		}

		echo json_encode($data);
	}

	/**
	 * Список экспедиторов
	 */
	public function forwarder_list()
	{
		$this->data['h1'] = $this->data['title'] = 'Список экспедиторов';
		$users = $this->db->where('role ', 3)->get('users')->result();
		$this->data['content'] = $this->load->view('info/forwarder_list', array('users' => $users), true);
		$this->load->view('layout', $this->data);
	}

	/* Добавление экспедитора */
	public function forwarder_add()
	{
		$data['success'] = '';

		$this->form_validation->set_rules('first_name', 'Имя', 'required');
		$this->form_validation->set_rules('last_name', 'Фамилия', 'required');
		$this->form_validation->set_rules('password', 'Пароль', 'required');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email');
		$this->form_validation->set_rules('login', 'Логин', 'trim|required|is_unique[users.login]');

		if ($this->form_validation->run() == TRUE) {
			$user['login'] = $this->user_model->set_login($_POST['first_name'] . '' . $_POST['last_name']);
			$user['password'] = $this->user_model->hash_password($_POST['password']);
			$user['role'] = 3;
			$user['registration_date'] = date('Y-m-d');
			$user['activity_status'] = 0;
			$user['first_name'] = $_POST['first_name'];
			$user['last_name'] = $_POST['last_name'];
			$user['phone'] = $_POST['phone'];
			$user['email'] = $_POST['email'];
			$user['auto_name'] = $_POST['auto_name'];
			$user['auto_number'] = $_POST['auto_number'];
			$user['note'] = $_POST['note'];
			/* Координаты */
			$user['latitude'] = str_replace(',', '.', $this->input->post('latitude'));
			$user['longitude'] = str_replace(',', '.', $this->input->post('longitude'));
			$insert = $this->db->insert('users', $user);
			if ($insert) {
				$data['success'] = 'Экспедитор добавлен';
			}

		}

		$this->data['h1'] = $this->data['title'] = 'Добавление экспедитора';
		$this->data['content'] = $this->load->view('info/forwarder_add', $data, true);
		$this->load->view('layout', $this->data);
	}


	public function forwarder_edit($id)
	{
		$data['success'] = '';

		$this->form_validation->set_rules('first_name', 'Имя', 'required');
		$this->form_validation->set_rules('last_name', 'Фамилия', 'required');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email');

		if ($this->form_validation->run() == TRUE) {
			$user['first_name'] = $_POST['first_name'];
			$user['last_name'] = $_POST['last_name'];
			$user['phone'] = $_POST['phone'];
			$user['email'] = $_POST['email'];
			$user['auto_name'] = $_POST['auto_name'];
			$user['auto_number'] = $_POST['auto_number'];
			$user['note'] = $_POST['note'];
			$user['activity_status'] = $_POST['activity_status'];
			/* Координаты */
			$user['latitude'] = str_replace(',', '.', $this->input->post('latitude'));
			$user['longitude'] = str_replace(',', '.', $this->input->post('longitude'));

			if ($user['activity_status'] == 2) {
				$user['activity_date'] = mysql_date($_POST['activity_date']);
			} else {
				$user['activity_date'] = NULL;
			}
			$update = $this->db->update('users', $user, array('id' => $id));
			if ($update) {
				$data['success'] = 'Экспедитор сохранен';
			}
		}


		$data['forwarder'] = $this->db->get_where('users', "id = $id")->row_array();

		$this->data['h1'] = $this->data['title'] = 'Редактирование экспедитора';
		$this->data['content'] = $this->load->view('info/forwarder_edit', $data, true);
		$this->load->view('layout', $this->data);
	}

	public function forwarder_delete($id)
	{
		if ($this->session->user->role == 1 AND $id != $this->session->user->id) {
			$this->db->delete('users', array('id' => $id));
			redirect('/info/forwarder_list');
		}
	}


}
