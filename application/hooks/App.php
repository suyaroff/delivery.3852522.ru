<?php

/**
 * Хуки выполняются на каждой странице
 */
class App
{
	private $CI;

	public function __construct()
	{
		$this->CI = &get_instance();
	}

	function all()
	{
		/* всех кто не авторизован */
		if (!$this->CI->session->user && ($this->CI->router->class != 'auth' && $this->CI->router->class != 'cron' ) ) {
			redirect('auth/login');
		}

		/* для всех кто авторизован */
		if ($this->CI->session->user) {
			$this->CI->db->query('UPDATE users SET last_activity = NOW() WHERE id =' . $this->CI->session->user->id);

			/* доступ к страницам по ролям */
			if ($this->CI->router->class == 'user' &&
				($this->CI->router->method == 'add' && $this->CI->router->method == 'edit') &&
				$this->CI->session->user->role != 1) {
				redirect('/');
			}

			/* для закупок */
			if (SYSTEM_NAME == 'buys') {
				/* автоматический статус */
				$now = date('Y-m-d H:i:s');
				$this->CI->db->query("UPDATE buys SET buy_status = 2 WHERE buy_status = 1 AND bid_date_end < '$now' AND auction_date > '$now'");
				$this->CI->db->query("UPDATE buys SET buy_status = 3 WHERE buy_status = 2 AND auction_date < '$now'");
			}
		}
	}

}
