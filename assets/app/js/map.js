/**
 * Created by Jamal on 28.03.2017.
 */

if (typeof all_points !== 'undefined') {


    $.each(all_points, function (i, v) {
        $('#forwarder_rout_link_' + v.forwarder).html('<a target="_blank" href="/plan/map?points=' + JSON.stringify(v.points) + '">маршурт на карте</a>');
    });

    ymaps.ready(init_way);

    function init_way() {

        $.each(all_points, function (i, v) {
            var forwarder_point = $('.forwarder_point_' + v.forwarder);
            var km = 0;
            var km_total = 0;
            $.each(v.points, function (ii, p) {
                if (ii == 0) {
                    v.points.push(p);
                } else {
                    km = ymaps.coordSystem.geo.getDistance(p, v.points[ii + 1]);
                    km_total += km;
                    forwarder_point[ii].innerHTML = ymaps.formatter.distance(km);
                }
            });
            forwarder_point[0].innerHTML = ymaps.formatter.distance(km_total);
        });
    }
}

