function check_buy() {
  var btn = $('#checkBuyButton');
  var regNumber = $('#reg_number').val();
  var regNumberBox = $('#regNumberBox');
  var regNumberBoxChildren = regNumberBox.children('.help-block');
  if (regNumber) {
    regNumberBox.removeClass('has-error');
    regNumberBoxChildren.html('');
    $.ajax({
      url: '/buys/find/' + regNumber,
      method: 'GET',
      beforeSend: function () {
        regNumberBoxChildren.html('Обработка запроса <i class="fa fa-refresh fa-spin"></i>');
        document.getElementById('buyAddForm').reset();
        $('#reg_number').val(regNumber);
      },
      success: function (res) {
        regNumberBoxChildren.html('');
        var data = JSON.parse(res);
        if (data.error) {
          regNumberBox.addClass('has-error');
          regNumberBoxChildren.html(data.error);
          document.getElementById('buyAddForm').reset();
        }
        if (data.success) {
          //console.log(data);
          $('input[name="buy_url"]').val(data.buy_url);
          $('textarea[name="customer"]').val(data.customer);
          $('input[name="bid_date_end"]').val(data.bid_date_end);
          $('input[name="auction_date"]').val(data.auction_date);
          $('input[name="max_contract_price"]').val(data.max_contract_price);
          $('input[name="bid_price"]').val(data.bid_price);
          $('input[name="items"]').val(data.items);
          $('textarea[name="buy_description"]').val(data.buy_description);
          $('select[name="buy_status"]').val(data.buy_status);
          if (data.place) {
            $('textarea[name="place"]').val(data.place);
          }
          if (data.place_url) {
            $('input[name="place_url"]').val(data.place_url);
          }
          $('#buy_url_external').html('<a target="_blank" href="' + data.buy_url + '"><i class="fa fa-external-link"></i></a>');
          //закрытость аука
          if(data.is_security){
            $("#buyAddForm").prepend('<div id="isSecurity" class="alert alert-warning">Этот аукцион закрытый</div>')
          } else {
            $("#isSecurity").remove();
          }
        }
      },
      error: function (error) {
        //console.log(error);
        regNumberBox.addClass('has-error');
        regNumberBoxChildren.html(error.status + '(' + error.statusText + ')');
      }

    });
  } else {
    regNumberBox.addClass('has-error');
    regNumberBoxChildren.html('Введите номер');
  }

}

function add_buy_participant_row() {
  var row = '<tr><td><textarea name="new_name[]" class="form-control input-sm" rows="1"></textarea></td><td><input type="text" name="new_offer[]" class="form-control input-sm" value=""></td><td></td></tr>';
  $('#buy_participant .table tbody').append(row);
}
