/* список контактов контрагента */
function contractor_contacts(contractor_id, selector) {

	$(selector).prop("disabled", true);
	$.getJSON("/info/ajax_contractor_contact/" + contractor_id, function (data) {
		var options = '';
		$.each(data, function (key, val) {
			options += '<option value="' + val.id + '">' + val.contact + ' (' + val.address_name + ')</option>';
		});
		$(selector).html(options).prop("disabled", false);
	});
}


/* удаление заявки */
function order_delete(id) {
	$('#boxOrder_' + id).fadeOut("slow");
	$.get('/order/delete/' + id + '/yes');
	$('#modalOrderDelete').html('Заявка удалена');
}

/* изменение заявки  */
function order_save(id) {

	var fmd = new FormData;
	var forwarder_id = $('#order_modal_edit_form select[name=forwarder_id]').val();
	fmd.append('forwarder_id', forwarder_id);
	var operation = $('#order_modal_edit_form select[name=operation]').val();
	fmd.append('operation', operation);
	var status = $('#order_modal_edit_form select[name=status]').val();
	fmd.append('status', status);
	var contractor = $('#order_modal_edit_form select[name=contractor]').val();
	fmd.append('contractor', contractor);
	var contractor_contact = $('#order_modal_edit_form select[name=contractor_contact]').val();
	fmd.append('contractor_contact', contractor_contact);

	var delivery_date = $('#order_modal_edit_form input[name=delivery_date]').val();
	fmd.append('delivery_date', delivery_date);
	var invoice = $('#order_modal_edit_form textarea[name=invoice]').val();
	fmd.append('invoice', invoice);
	var note = $('#order_modal_edit_form textarea[name=note]').val();
	fmd.append('note', note);
	var next_order = $('#order_modal_edit_form input[name=next_order]').val();
	fmd.append('next_order', next_order);
	var weight = $('#order_modal_edit_form input[name=weight]').val();
	fmd.append('weight', weight);
	var invoice_contractor = $('#order_modal_edit_form input[name=invoice_contractor]').val();
	fmd.append('invoice_contractor', invoice_contractor);

	var doc = $('#order_modal_edit_form input[name=doc]');

	if(doc.prop('files').length){
		fmd.append('doc', doc.prop('files')[0]);
	}

	if (status == 1 && operation == 2) {
		alert('Тип операции: забор товара. Невозжно отправить на сборку');
		return false;
	}
	if (status == 2 && status === 0) {
		if (delivery_date == '') {
			alert('Выберите дату доставки');
			return false;
		}

		if (forwarder_id == '') {
			alert('Выберите експедитора');
			return false;
		}
	}

	$.ajax({
		data: fmd,
		url: '/order/ajax_change/' + id,
		type: 'POST',
		cache: false,
		contentType: false,
		processData: false,
		success: function (res) {

		},
	});
}


/* изменение склада  */
function stock_save(id) {
	var data = $('#stock_edit_form .order-field');
	var note_stock = data.filter('[name=note_stock]').val();
	var old_note_stock = $('#field_old_note_stock').val();
	var stock_status = data.filter('[name=stock_status]').val();
	var old_stock_status = $('#field_old_stock_status').val();

	/* недостача */
	if(stock_status == 3 && stock_status !=old_stock_status){
		if(!note_stock || (old_note_stock == note_stock)){
			alert('Введите примечание склада');
			return;
		}
	}

	$.ajax({
		data: data,
		url: '/stock/update/' + id,
		method: 'POST',
		success: function (res) {
			$('#order_form').modal('hide');
		},

	});
}


/* изменение плана  */
function plan_save(id) {
	var data = $('#order_row_' + id + ' .order-field');
	var btn = $('#order_save_button_' + id);
	$.ajax({
		data: data,
		url: '/order/ajax_change/' + id,
		method: 'POST',
		beforeSend: function () {
			btn.html('<i class="fa fa-refresh fa-spin"></i>');
		},
		success: function (res) {
			btn.removeClass('btn-success');
			btn.html('<i class="fa fa-check"></i>');
			setTimeout(function () {
				btn.html('<i class="fa fa-save"></i>');
			}, 500);
		},

	});
}


function order_check(button, input) {

	$.ajax({
		url: '/order/ajax_get/' + input.val(),
		method: 'GET',
		dataType: 'json',
		beforeSend: function () {
			$(button).html('<i class="fa fa-refresh fa-spin"></i>');
		},
		success: function (order) {
			if (order) {
				$(button).html('<i class="fa fa-check"></i>').removeClass('btn-info').addClass('btn-success');
			} else {
				$(button).html('<i class="fa fa-search"></i>').removeClass('btn-success').addClass('btn-info');
				input.val('');
			}
		},

	});
}

function order_check_next(field, current_order, next_order) {
	var n = $('#order_row_' + next_order);
	if (n.length) {
		var pos = parseInt($(n).find('input[name=ord]').val());
		var pos_current = parseInt($(field).val());
		var need_pos = pos + 1;

		if (need_pos != pos_current) {
			alert('Проверьте поле "назначить после" у заявки ' + current_order);
		}
	}

}


function order_check_pos(selector) {
	var positions = {};
	var p = '';
	$(selector).each(function (k, v) {
		p = $(v).val();
		if (positions[p]) {
			alert('Порядковые номера должны быть разными');
		}
		positions[p] = 1;
	});
}

function add_invoice_field() {
	var input = '<div class="input-group">\n' +
		'<input type="text" class="form-control" name="invoice[]">\n' +
		'<div class="input-group-btn">\n' +
		'<button type="button" class="btn btn-danger" onclick="remove_invoice_field(this)">Удалить</button>\n' +
		'</div>\n' +
		'</div>';
	$('#invoice_box').append(input);

}

function remove_invoice_field(el) {
	$(el).parents('.input-group').remove();
}


function parseCoordinates(el){
	let coordinates = el.value.split(',');
	if(coordinates[1]) {
		el.value = coordinates[0];
		let lon_name = el.id.replace('lat', 'long');

		$('#'+lon_name).val(coordinates[1]);
	}
}

$(function () {
	/* календарь для страницы плана */
	$('#plan_datepicker_button').datepicker({
		language: 'ru',
		format: 'dd-mm-yyyy',
		todayHighlight: true
	}).on('changeDate', function (e) {
		var selected_date = e.format('dd-mm-yyyy');
		document.location = '/plan/arhiv/' + selected_date;
	});

	/* индикатор изменения заявки */
	$('.order-field').each(function (i, v) {
		$(v).on('change', function (f) {
			var id = f.target.dataset.order;
			$('#order_save_button_' + id).addClass('btn-success');
		})
	});

	/* авто размер */
	autosize($('.order-table textarea'));

	/* календарь */
	$('.datepicker-field').datepicker({
		autoclose: true,
		format: "dd-mm-yyyy",
		language: 'ru',
		todayHighlight: true
	});

	$('.modal').on('hidden.bs.modal', function () {
		$(this).removeData('bs.modal');
	});
	$('.select2').select2();

});

