Vue.component('contractor-list', {
	template: `<table id="contractor_list" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Тип</th>
                <th>Контрагент</th>
                <th>Адрес</th>
                <th>Контакты</th>
                <th>Примечание</th>
                <th>На карте</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="contractor of contractors">
                <td>{{contractor.id}}</td>
                <td>{{contractor.type_name}}</td>
                <td>{{contractor.name}}</td>
                <td>{{contractor.address_name}}</td>
                <td>{{contractor.contact}}</td>
                <td>{{contractor.contact_info}}</td>
                <td>{{contractor.contact_map}}</td>
                <td><a :href="'/info/contractor_edit/'+contractor.id" class="btn btn-primary btn-sm">Редактировать</a></td>
            </tr>
            </tbody>
        </table>`,
	data: function () {
		return {
			contractors: []
		}
	},

	created: function () {
		axios.get('/info/contractor_list/api').then(r => {
			this.contractors = r.data.contractors;
		})
	}
});

Vue.component('contractor-edit', {
	template: `<form class="form-horizontal">
<div class="box box-info">
<div class="box-header with-border"><h3 class="box-title">Контрагент</h3></div>
<div class="box-body">
	<div class="form-group">
		<label for="selectType" class="col-sm-2 control-label">Тип</label>
		<div class="col-sm-10">
			<select id="selectType" class="form-control" name="type" v-model="contractor.type" @change="contractorSave">
				<option v-for="name, id of this.$store.state.config.contractor_type" :value="id" >{{name}}</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="input0" class="col-sm-2 control-label">Название</label>
		<div class="col-sm-10">
			<input type="text" name="name" v-model="contractor.name" class="form-control" placeholder="Название" @change="contractorSave">
		</div>
	</div>
</div>
</div>
<div class="box box-success">
	<div class="box-header with-border"><h3 class="box-title">Адреса</h3></div>
	<div class="box-body">
		<div class="form-group" v-for="ca of contractor_address">
			<div class="col-sm-5">
				<input type="text" class="form-control" v-model="ca.address_name" @change="addressSave(ca)">
			</div>
			<div class="col-sm-3">
				<input type="text" placeholder="Широта" class="form-control" v-model="ca.latitude" @change="addressSave(ca)">
			</div>
			<div class="col-sm-3">
				<input type="text" placeholder="Долгота" class="form-control" v-model="ca.longitude" @change="addressSave(ca)">
			</div>
			<div class="col-sm-1">
				<button type="button" class="btn btn-sm btn-warning btn-block" @click="addressDelete(ca)"><i class="fa fa-trash" ></i> удалить</button>
			</div>
		</div>
	</div>
	<div class="box-footer">
	<div class="form-group">
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="Введите новый адрес"  v-model="addressNew.name" >
			</div>
			<div class="col-sm-3">
				<input type="text" placeholder="Широта" class="form-control" v-model="addressNew.latitude">
			</div>
			<div class="col-sm-3">
				<input type="text" placeholder="Долгота" class="form-control" v-model="addressNew.longitude">
			</div>
			<div class="col-sm-1">
				<button type="button" class="btn btn-sm btn-success btn-block" @click="addressAdd"><i class="fa fa-plus"></i> Добавить</button>
			</div>
		</div>
	</div>
</div>
<div class="box box-warning">
	<div class="box-header with-border"><h3 class="box-title">Контактные лица</h3></div>
	<div class="box-body">
		<div class="form-group" v-for="cc of contractor_contact">
			<div class="col-sm-3">
				<input type="text" v-model="cc.contact" class="form-control" @change="contactSave(cc)">
			</div>
			<div class="col-sm-3">
				<select v-model="cc.address_id" class="form-control" @change="contactSave(cc)">
					<option v-for="ca of contractor_address" :value="ca.address_id">{{ca.address_name}}</option>
				</select>
			</div>
			<div class="col-sm-3">
				<input type="text" v-model="cc.contact_info" class="form-control"  placeholder="Примечание" @change="contactSave(cc)">
			</div>
			<div class="col-sm-2">
				<input type="text" v-model="cc.contact_map"  class="form-control" placeholder="На карте" @change="contactSave(cc)">
			</div>
			<div class="col-sm-1">
				<button type="button" class="btn btn-sm btn-warning btn-block" @click="contactDelete(cc)"><i class="fa fa-trash" ></i> удалить</button>
			</div>
		</div>
	</div>	
	<div class="box-footer">
		<div class="form-group">
			<div class="col-sm-3">
				<input type="text" v-model="contactNew.contact" placeholder="Введите Имя что бы добавить" class="form-control">
			</div>
			<div class="col-sm-3">
				<select v-model="contactNew.address_id" class="form-control">
					<option v-for="ca of contractor_address" :value="ca.address_id">{{ca.address_name}}</option>
				</select>
			</div>
			<div class="col-sm-3">
				<input type="text" v-model="contactNew.contact_info" placeholder="Примечание" class="form-control">
			</div>
			<div class="col-sm-2">
				<input type="text" v-model="contactNew.contact_map" placeholder="На карте" class="form-control">
			</div>
			<div class="col-sm-1">
				<button type="button" class="btn btn-sm btn-success btn-block" @click="contactAdd"><i class="fa fa-plus"></i> Добавить</button>
			</div>
		</div>
	</div>
</div>
</form>`,
	props: ['id'],
	data: function () {
		return {
			contractor: {},
			contractor_address: {},
			contractor_contact: {},
			addressNew: {name: '', latitude: '', longitude: ''},
			contactNew: {contact: '', address_id: '', contact_info: '', contact_map: ''}
		}
	},
	methods: {
		contractorGet: function () {
			axios.get('/info/contractor/' + this.id).then(r => {
				this.contractor = r.data.contractor;
				this.contractor_address = r.data.contractor_address;
				this.contractor_contact = r.data.contractor_contact;
			}).catch(e => {
				toastr.error('Ошибка с запросом контрагента' + e)
			})
		},
		contractorSave: function () {
			var data = new FormData();
			data.append('id', this.contractor.id);
			data.append('name', this.contractor.name);
			data.append('type', this.contractor.type);
			axios.post('/info/contractor_save', data).then(r => {
				if (r.data.success) {
					toastr.success('Контрагент сохранен');
				}
				if (r.data.error) {
					toastr.error('Ошибка сохранения контрагента');
				}

			}).catch(e => {
				toastr.error('Ошибка сохранения контрагента ' + e)
			})
		},
		addressSave: function (address) {
			if(address.address_name){
				var data = new FormData();
				data.append('address_id', address.address_id);
				data.append('address_name', address.address_name);
				data.append('latitude', address.latitude);
				data.append('longitude', address.longitude);
				axios.post('/info/address_save', data).then(r => {
					if (r.data.success) {
						toastr.success('Адрес сохранен');
						this.contractorGet();
					}
					if (r.data.error) {
						toastr.error('Ошибка сохранения адреса');
					}

				}).catch(e => {
					toastr.error('Ошибка выполнения запроса ' + e)
				})
			} else {
				toastr.warning('Введите название адреса');
			}

		},
		addressAdd: function () {
			if (this.addressNew.name) {
				var data = new FormData();
				data.append('name', this.addressNew.name);
				data.append('latitude', this.addressNew.latitude);
				data.append('longitude', this.addressNew.longitude);
				data.append('contractor_id', this.id);
				axios.post('/info/address_add', data).then(r => {
					if (r.data.success) {
						toastr.success('Адрес добавлен');
						this.addressNew = {name: '', latitude: '', longitude: ''};
						this.contractorGet();
					}
					if (r.data.error) {
						toastr.error('Ошибка добавления адреса');
					}
				}).catch(e => {
					toastr.error('Ошибка выполнения запроса ' + e)
				})
			} else {
				toastr.warning('Введите название адреса');
			}

		},
		addressDelete: function (address) {
			if (confirm('Удалить адрес "' + address.address_name + '" ?')) {
				var data = new FormData();
				data.append('id', address.address_id);
				axios.post('/info/address_delete', data).then(r => {
					if (r.data.success) {
						toastr.success('Адрес удаден');
						this.contractorGet();
					}
					if (r.data.error) {
						toastr.error('Ошибка удаления адреса');
					}
				}).catch(e => {
					toastr.error('Ошибка выполнения запроса ' + e)
				})
			}
		},
		contactSave:function (c) {
			if(c.contact){
				var d = new FormData();
				d.append('id', c.id);
				d.append('contact', c.contact);
				d.append('address_id', c.address_id);
				d.append('contact_info', c.contact_info);
				d.append('contact_map', c.contact_map);
				axios.post('/info/contractor_contact_save', d).then(r => {
					if (r.data.success) {
						toastr.success('Контакт сохранен');
					}
					if (r.data.error) {
						toastr.warning('Контакт не сохранен');
					}
				}).catch(e => {
					toastr.error('Ошибка выполнения запроса ' + e)
				})
			} else {
				toastr.warning('Введите название контакта');
			}

		},
		contactAdd:function () {
			var e = '';
			if(!this.contactNew.contact) e += ' Введите имя ';
			if(!this.contactNew.address_id) e += ' Выберите адрес ';
			if(e){
				toastr.warning(e);
			} else {
				var d = new FormData();
				d.append('contact', this.contactNew.contact);
				d.append('address_id', this.contactNew.address_id);
				d.append('contact_info', this.contactNew.contact_info);
				d.append('contact_map', this.contactNew.contact_map);
				axios.post('/info/contractor_contact_add', d).then(r => {
					if (r.data.success) {
						toastr.success('Контакт добавлен');
						this.contractorGet();
						this.contactNew = {contact: '', address_id: '', contact_info: '', contact_map: ''};
					}
					if (r.data.error) {
						toastr.warning('Контакт не добавлен');
					}
				}).catch(e => {
					toastr.error('Ошибка выполнения запроса ' + e)
				})
			}
		},
		contactDelete:function (c) {
			if (confirm('Удалить контакт "' + c.contact + '" ?')) {
				var data = new FormData();
				data.append('id', c.id);
				axios.post('/info/contractor_contact_delete', data).then(r => {
					if (r.data.success) {
						toastr.success('Контакт удаден');
						this.contractorGet();
					}
					if (r.data.error) {
						toastr.error('Ошибка удаления контакта');
					}
				}).catch(e => {
					toastr.error('Ошибка выполнения запроса ' + e)
				})
			}
		}
	},
	created: function () {
		this.contractorGet();
	}
});
