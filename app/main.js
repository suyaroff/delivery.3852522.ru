const store = new Vuex.Store({
	state: {
		config: {}
	},
	mutations: {
		setConf(state, d) {
			state.config = d
		}
	},
	actions: {
		/* config */
		getConf({commit}) {
			axios.get('/info/config').then(r => {
				commit('setConf', r.data);
			}).catch(error => {
				toastr.error(error);
			});
		},
	}
});

var app = new Vue({
	el: "#root",
	store,
	created: function () {
		store.dispatch('getConf');
	}
});
